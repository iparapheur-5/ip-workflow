Change Log
==========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.6.4] - 2022
[1.6.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.4

### Added

- Docker healthcheck
- Trivy monitoring


## [1.6.3] - 2022
[1.6.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.3


## [1.6.2] - 2022
[1.6.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.2


## [1.6.1] - 2022
[1.6.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.1


## [1.6.0] - 2022
[1.6.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.6.0


## [1.5.7] - 2022
[1.5.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.7


## [1.5.6] - 2022
[1.5.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.6


## [1.5.5] - 2022-07-04
[1.5.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.5

### Fixed

- External actor in history


## [1.5.4] - 2022-06-28
[1.5.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.4

### Added

- Start and final tasks wrapped as regular actions

### Fixed

- Undo
- Delegation in history


## [1.5.3] - 2022-06-20
[1.5.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.3

### Fixed

- Date filter


## [1.5.2] - 2022-06-13
[1.5.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.2

### Fixed

- Read state


## [1.5.1] - 2022-06-08
[1.5.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.1

### Fixed

- Visibility parse


## [1.5.0] - 2022-06-01
[1.5.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.5.0

### Added

- Second opinion's ASK dedicated action


## [1.4.2] - 2022-05-27
[1.4.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.2

### Fixed

- Minor crashes


## [1.4.1] - 2022-05-25
[1.4.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.1

### Added

- Read state in BPMN
- Prometheus monitoring entrypoint


## [1.4.0] - 2022-04-13
[1.4.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.4.0

### Fixed

- States
- Creation date


## [1.3.1] - 2022-04-01
[1.3.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.3.1

### Fixed

- Spring4Shell vulnerability


## [1.3.0] - 2022-03-30
[1.3.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.3.0

### Added

- Secure mail action


## [1.2.0] - 2022-03-26
[1.2.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.2.0

### Fixed

- Metadata
- Requests' sort-by


## [1.1.7] - 2022-03-09
[1.1.7]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.7

### Fixed

- End desk definition
- Parallel tasks retrieving


## [1.1.6] - 2022-03-07
[1.1.6]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.6

### Fixed

- Folder search
- Annotations in sub-tasks


## [1.1.5] - 2022-02-22
[1.1.5]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.5

### Fixed

- Info on get task


## [1.1.4] - 2022-02-11
[1.1.4]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.4

### Fixed

- Limit date


## [1.1.3] - 2022-02-04
[1.1.3]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.3

### Fixed

- Wrong parameter for delegations


## [1.1.2] - 2022-01-28
[1.1.2]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.2

### Fixed

- Sentry reporting


## [1.1.1] - 2022-01-20
[1.1.1]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.1

### Fixed

- Log4j2 patch


## [1.1.0] - 2022-01-10
[1.1.0]: https://gitlab.libriciel.fr/outils/workflow/tags/1.1.0

### Changed

- Swagger 2 to OAS3


## [0.2.0] - 2020-03-03
[0.2.0]: https://gitlab.libriciel.fr/outils/workflow/tags/0.2.0

### Added

- DueDate on instances


## [0.1.0] - 2020-02-12
[0.1.0]: https://gitlab.libriciel.fr/outils/workflow/tags/0.1.0

### Added

- iparapheur's folder
- iparapheur's visa
- iparapheur's sign
- iparapheur's second_opinion
- Variables
- JUnit tests
- Swagger
- CRUD on definitions
- CRUD on tasks
- CRUD on instances

