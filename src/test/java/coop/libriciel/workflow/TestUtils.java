/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow;

import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.StepDefinition;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.Task.Action;
import coop.libriciel.workflow.models.WorkflowDefinition;
import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.TaskInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;

import static coop.libriciel.workflow.models.StepDefinition.ParallelType.OR;
import static coop.libriciel.workflow.models.Task.Action.*;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.*;

public class TestUtils {


    public static final String EXAMPLE_TENANT_ID = "tenant01";
    public static final String EXAMPLE_TENANT_ID_02 = "tenant02";

    public static final String EXAMPLE_WORKFLOW_DEFINITION_ID = "generated_workflow_01";
    public static final String EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY = "generated_workflow_conflictKey";
    public static final String EXAMPLE_WORKFLOW_TYPE_ID = "type_01";
    public static final String EXAMPLE_WORKFLOW_SUBTYPE_ID = "subtype_01";
    public static final String EXAMPLE_WORKFLOW_DEFINITION_NAME = "Generated workflow";
    public static final Date EXAMPLE_WORKFLOW_DUE_DATE = new Date(9999L);

    public static final String EXAMPLE_ORIGIN_DESK = "desk00";
    public static final String EXAMPLE_ORIGIN_USER = "user00";
    public static final String EXAMPLE_FINAL_DESK = "desk99";
    public static final String EXAMPLE_FINAL_USER = "user99";

    public static final Action EXAMPLE_STEP1_ACTION = VISA;
    public static final String EXAMPLE_STEP1_NAME = "visa user01";
    public static final String EXAMPLE_STEP1_DESK = "desk01";
    public static final String EXAMPLE_STEP1_USER = "user01";

    public static final Action EXAMPLE_STEP2_ACTION = VISA;
    public static final String EXAMPLE_STEP2_NAME = "visa user02";
    public static final String EXAMPLE_STEP2_DESK = "desk02";
    public static final String EXAMPLE_STEP2_USER = "user02";

    public static final Action EXAMPLE_STEP3_ACTION = SIGNATURE;
    public static final String EXAMPLE_STEP3_NAME = "signature user03";
    public static final String EXAMPLE_STEP3_DESK = "desk03";
    public static final String EXAMPLE_STEP3_USER = "user03";

    public static final Action EXAMPLE_STEP4_ACTION = EXTERNAL_SIGNATURE;
    public static final String EXAMPLE_STEP4_NAME = "external signature user04";
    public static final String EXAMPLE_STEP4_DESK = "desk04";
    public static final String EXAMPLE_STEP4_USER = "user04";

    public static final Action EXAMPLE_STEP5_ACTION = SECURE_MAIL;
    public static final String EXAMPLE_STEP5_NAME = "secure mail user05";
    public static final String EXAMPLE_STEP5_DESK = "desk05";
    public static final String EXAMPLE_STEP5_USER = "user05";

    public static final WorkflowDefinition EXAMPLE_WORKFLOW_DEFINITION = new WorkflowDefinition() {{
        setId(EXAMPLE_WORKFLOW_DEFINITION_ID);
        setName(EXAMPLE_WORKFLOW_DEFINITION_NAME);
        setSteps(asList(
                new StepDefinition(EXAMPLE_STEP1_ACTION, "visa_user01", EXAMPLE_STEP1_NAME, singletonList(EXAMPLE_STEP1_DESK), emptyList(), emptyList(), singletonList("metadata_01"), OR),
                new StepDefinition(EXAMPLE_STEP2_ACTION, "visa_user02", EXAMPLE_STEP2_NAME, singletonList(EXAMPLE_STEP2_DESK), singletonList("notified_01"), emptyList(), emptyList(), OR),
                new StepDefinition(EXAMPLE_STEP3_ACTION, "signature_user02", EXAMPLE_STEP3_NAME, singletonList(EXAMPLE_STEP3_DESK), emptyList(), emptyList(), emptyList(), OR),
                new StepDefinition(EXAMPLE_STEP4_ACTION, "external_signature_user02", EXAMPLE_STEP4_NAME, singletonList(EXAMPLE_STEP4_DESK), emptyList(), emptyList(), emptyList(), OR),
                new StepDefinition(EXAMPLE_STEP5_ACTION, "secure_mail_user02", EXAMPLE_STEP5_NAME, singletonList(EXAMPLE_STEP5_DESK), emptyList(), emptyList(), emptyList(), OR)
        ));
        setFinalDeskId(EXAMPLE_FINAL_DESK);
    }};

    public static final WorkflowDefinition EXAMPLE_CONFLICT_WORKFLOW_DEFINITION_1 = new WorkflowDefinition() {{
        setId(EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY);
        setName(EXAMPLE_WORKFLOW_DEFINITION_NAME);
        setSteps(asList(
                new StepDefinition(EXAMPLE_STEP1_ACTION, "visa_user01", EXAMPLE_STEP1_NAME, singletonList(EXAMPLE_STEP1_DESK), emptyList(), singletonList("metadata_01"), emptyList(), OR),
                new StepDefinition(EXAMPLE_STEP2_ACTION, "visa_user02", EXAMPLE_STEP2_NAME, singletonList(EXAMPLE_STEP2_DESK), singletonList("notified_01"), emptyList(), emptyList(), OR)
        ));
        setFinalDeskId(EXAMPLE_FINAL_DESK);
    }};

    public static final WorkflowDefinition EXAMPLE_CONFLICT_WORKFLOW_DEFINITION_2 = new WorkflowDefinition() {{
        setId(EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY);
        setName(EXAMPLE_WORKFLOW_DEFINITION_NAME);
        setSteps(asList(
                new StepDefinition(EXAMPLE_STEP1_ACTION, "visa_user01", EXAMPLE_STEP1_NAME, singletonList(EXAMPLE_STEP1_DESK), emptyList(), singletonList("metadata_01"), emptyList(), OR),
                new StepDefinition(EXAMPLE_STEP3_ACTION, "signature_user02", EXAMPLE_STEP3_NAME, singletonList(EXAMPLE_STEP3_DESK), emptyList(), emptyList(), emptyList(), OR)
        ));
        setFinalDeskId(EXAMPLE_FINAL_DESK);
    }};

    public static final Supplier<Instance> EXAMPLE_INSTANCE_TO_START_SUPPLIER = () -> new Instance() {{
        setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
        setTenantId(EXAMPLE_TENANT_ID);
        setOriginGroup(EXAMPLE_ORIGIN_DESK);
        setFinalGroup(EXAMPLE_FINAL_DESK);
        setDueDate(EXAMPLE_WORKFLOW_DUE_DATE);
        setBusinessKey(UUID.randomUUID().toString());
        setVariables(emptyMap());
    }};

    public static final Instance EXAMPLE_INSTANCE_WITH_DEFINITION_CONFLICT = new Instance() {{
        setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY);
        setTenantId(EXAMPLE_TENANT_ID);
        setOriginGroup(EXAMPLE_ORIGIN_DESK);
        setFinalGroup(EXAMPLE_FINAL_DESK);
        setDueDate(EXAMPLE_WORKFLOW_DUE_DATE);
        setVariables(emptyMap());
    }};


    public static void cleanup(TaskService taskService, RuntimeService runtimeService, RepositoryService repositoryService) {

        taskService
                .createNativeTaskQuery()
                .list().stream()
                .map(TaskInfo::getId)
                .forEach(taskService::deleteTask);

        runtimeService
                .createProcessInstanceQuery()
                .list().stream()
                .map(ProcessInstance::getId)
                .forEach(i -> {
                    try {
                        runtimeService.deleteProcessInstance(i, "");
                    } catch (FlowableObjectNotFoundException f) {
                        /* If a parent process has been deleted, children will be unavailable. */
                    }
                });

        repositoryService
                .createDeploymentQuery()
                .list()
                .stream()
                .map(Deployment::getParentDeploymentId)
                .forEach(repositoryService::deleteDeployment);

        assertEquals(0, taskService.createTaskQuery().list().size());
        assertEquals(0, taskService.createNativeTaskQuery().list().size());
        assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
    }


    public static Task assertReadExistenceAndGetMainTask(@Nullable List<Task> tasks, @NotNull Action action, @NotNull String candidateGroupId) {

        // Naive tests

        assertNotNull(tasks);
        assertTrue(tasks.stream().anyMatch(t -> t.getExpectedAction() == action));
        assertTrue(tasks.stream().anyMatch(t -> t.getExpectedAction() == READ));

        // Deep tests

        List<Task> filteredTasks = tasks.stream().filter(t -> t.getCandidateGroups().contains(candidateGroupId)).toList();

        filteredTasks.stream()
                .filter(t -> t.getExpectedAction() == READ)
                .filter(t -> t.getPerformedAction() == null)
                .findFirst()
                .orElseGet(Assertions::fail);

        return filteredTasks.stream()
                .filter(t -> t.getExpectedAction() == action)
                .filter(t -> t.getPerformedAction() == null)
                .findFirst()
                .orElseGet(Assertions::fail);
    }


    public static org.flowable.task.api.Task assertReadExistenceAndGetMainTask(@Nullable List<org.flowable.task.api.Task> tasks, @NotNull String actionName) {

        assertNotNull(tasks);
        assertEquals(2, tasks.size());
        assertEquals(Set.of("read", actionName), tasks.stream().map(TaskInfo::getName).collect(toSet()));

        return tasks.stream()
                .filter(t -> StringUtils.equals(actionName, t.getName()))
                .findFirst()
                .orElseGet(Assertions::fail);
    }


    public static org.flowable.task.api.Task assertAndGetSingleUndoTask(@Nullable List<org.flowable.task.api.Task> tasks) {

        assertNotNull(tasks);
        assertEquals(1, tasks.size());

        org.flowable.task.api.Task undoTask = tasks.get(0);
        assertEquals("undo", undoTask.getName());

        return undoTask;
    }


    /**
     * Filters out UNDO and READ tasks, to ease tests.
     * We don't want to have those everytime, as they aren't always generated in the same order.
     */
    public static boolean isMainAction(@NotNull Task task) {
        Set<Action> filterableActions = Set.of(UNDO, READ);
        return !filterableActions.contains(task.getExpectedAction());
    }


}
