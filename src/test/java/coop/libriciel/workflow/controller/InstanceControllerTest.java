/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.TestUtils;
import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.WorkflowDefinition;
import coop.libriciel.workflow.models.requests.PerformTaskRequest;
import coop.libriciel.workflow.utils.PaginatedList;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.TaskInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.IntStream;

import static coop.libriciel.workflow.TestUtils.*;
import static coop.libriciel.workflow.models.Instance.FOLDER_DEPLOYMENT_KEY;
import static coop.libriciel.workflow.models.SortBy.INSTANCE_ID;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.WorkflowDefinition.SortBy.NAME;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class InstanceControllerTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private InstanceController instanceController;
    @Autowired private TaskController taskController;

    @Autowired private TaskService taskService;
    @Autowired private RuntimeService runtimeService;
    @Autowired private HistoryService historyService;


    @BeforeEach void setup() {

        cleanup();

        assertEquals(0, taskService.createNativeTaskQuery().list().size());
        assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricTaskInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricProcessInstanceQuery().list().size());

        definitionController.setupIParapheurDefinitions();

        WorkflowDefinition workflowDefinition = definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);
        assertNotNull(workflowDefinition);
        assertNotNull(workflowDefinition.getDeploymentId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_ID, workflowDefinition.getId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_NAME, workflowDefinition.getName());
    }


    @AfterEach void cleanup() {

        taskService.createNativeTaskQuery()
                .list().stream()
                .map(TaskInfo::getId)
                .forEach(i -> taskService.deleteTask(i));

        runtimeService.createProcessInstanceQuery()
                .list().stream()
                .map(ProcessInstance::getId)
                .forEach(i -> {
                    try {
                        runtimeService.deleteProcessInstance(i, "");
                    } catch (FlowableObjectNotFoundException f) {
                        /* If a parent process has been deleted, children will be unavailable. */
                    }
                });

        historyService.createHistoricTaskInstanceQuery()
                .list().stream()
                .map(TaskInfo::getId)
                .forEach(i -> historyService.deleteHistoricTaskInstance(i));

        historyService.createHistoricProcessInstanceQuery()
                .list().stream()
                .map(HistoricProcessInstance::getId)
                .forEach(i -> {
                    try {
                        historyService.deleteHistoricProcessInstance(i);
                    } catch (FlowableObjectNotFoundException e) {
                        /* If a parent process has been deleted, children will be unavailable. */
                    }
                });

        assertEquals(
                0,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null,
                                null, null, null, null
                        )
                        .getTotal()
        );

        definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null)
                .getData()
                .stream()
                .map(WorkflowDefinition::getDeploymentId)
                .forEach(definitionController::deleteWorkflowDefinition);

        assertEquals(0, definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());

        assertEquals(0, taskService.createNativeTaskQuery().list().size());
        assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricTaskInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricProcessInstanceQuery().list().size());
    }


    @Test void startInstance() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());
        assertNotNull(instance);
        assertNotNull(instance.getId());
        assertNotNull(instance.getStartTime());
        assertEquals(FOLDER_DEPLOYMENT_KEY, instance.getDeploymentKey());
        assertEquals(EXAMPLE_ORIGIN_DESK, instance.getOriginGroup());
        assertEquals(EXAMPLE_FINAL_DESK, instance.getFinalGroup());
        assertNull(instance.getCreationWorkflowId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_ID, instance.getValidationWorkflowId());
    }


    @Test void startInstance_error() {

        Instance badInstance = new Instance() {{
            setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
            setOriginGroup(EXAMPLE_ORIGIN_DESK);
            setFinalGroup(EXAMPLE_FINAL_DESK);
            setVariables(Map.of(WORKFLOW_INTERNAL + "test", "test"));
        }};

        try {
            instanceController.createInstance(badInstance);
            fail();
        } catch (ResponseStatusException e) {
            assertEquals(BAD_REQUEST, e.getStatus());
        }
    }


    @Test void getInstances() {
        IntStream.range(0, 15).forEach(i -> instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get()));

        // Pagination

        PaginatedList<Task> result = instanceController
                .getInstances(
                        EXAMPLE_TENANT_ID, 0, 1, INSTANCE_ID, true,
                        null, null, null, null,
                        null, null, null, null
                );

        assertEquals(15, result.getTotal());
        assertEquals(1, result.getData().size());

        result = instanceController
                .getInstances(
                        EXAMPLE_TENANT_ID, 1, 10, INSTANCE_ID, true,
                        null, null, null, null,
                        null, null, null, null
                );

        assertEquals(15, result.getTotal());
        assertEquals(5, result.getData().size());

        result = instanceController
                .getInstances(
                        EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                        null, null, null, null,
                        null, null, null, null
                );

        assertEquals(15, result.getTotal());
        assertEquals(15, result.getData().size());
    }


    @Test void getInstance() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertNotNull(instance);
        assertNotNull(instance.getId());
        assertNotNull(instance.getStartTime());
        assertEquals(FOLDER_DEPLOYMENT_KEY, instance.getDeploymentKey());
        assertEquals(EXAMPLE_ORIGIN_DESK, instance.getOriginGroup());
        assertEquals(EXAMPLE_FINAL_DESK, instance.getFinalGroup());
        assertNull(instance.getCreationWorkflowId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_ID, instance.getValidationWorkflowId());

        assertEquals(asList(EXAMPLE_ORIGIN_DESK, EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK, EXAMPLE_STEP3_DESK, EXAMPLE_STEP4_DESK, EXAMPLE_STEP5_DESK, EXAMPLE_FINAL_DESK),
                instance.getTaskList().stream().map(Task::getCandidateGroups).flatMap(Collection::stream).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList());

        assertTrue(instance.getTaskList().stream().map(Task::getDate).allMatch(Objects::isNull));
        assertTrue(instance.getTaskList().stream().map(Task::getAssignee).allMatch(Objects::isNull));
    }


    @Test void getInstance_historyAfterVisa() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null, null),
                instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).map(Task::getAssignee).toList()
        );
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).skip(2).map(Task::getDate).allMatch(Objects::isNull));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertEquals(
                asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, UNDO, READ, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList()
        );
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList());

        // TODO : Test the real dates, for some reason, they are different in the CI.
        //  Probably a locale issue
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).map(Task::getDueDate).limit(3).noneMatch(Objects::isNull));
        // assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).map(Task::getDueDate).skip(3).allMatch(Objects::isNull));

        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).skip(2).map(Task::getDate).allMatch(Objects::isNull));

        taskController.performTask(instance.getTaskList().get(2).getId(), new PerformTaskRequest(EXAMPLE_STEP2_ACTION, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, UNDO, READ, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList());
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).limit(3).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().filter(t -> t.getExpectedAction() != READ).skip(3).map(Task::getDate).allMatch(Objects::isNull));
    }


    @Test void getInstance_historyAfterDelegatedVisa() {

        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task startTask = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(startTask.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task step1Task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(
                step1Task.getId(),
                new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP3_USER, EXAMPLE_STEP3_DESK, EXAMPLE_STEP1_DESK, emptyMap())
        );

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        Task historicStep1Task = instance.getTaskList().stream().filter(task -> task.getExpectedAction() != READ).toList().get(1);
        assertEquals(EXAMPLE_STEP3_USER, historicStep1Task.getAssignee());
        assertEquals(List.of(EXAMPLE_STEP3_DESK), historicStep1Task.getCandidateGroups());
    }


    @Test void getInstance_historyAfterUndo() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        assertEquals(
                asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, UNDO, READ, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList()
        );
        assertEquals(
                asList(EXAMPLE_ORIGIN_DESK, EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK, EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK, EXAMPLE_STEP3_DESK, EXAMPLE_STEP4_DESK, EXAMPLE_STEP5_DESK, EXAMPLE_FINAL_DESK),
                instance.getTaskList().stream().map(Task::getCandidateGroups).flatMap(Collection::stream).toList()
        );
        assertEquals(
                asList(START, EXAMPLE_STEP1_ACTION, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getPerformedAction).toList()
        );
        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList()
        );
        assertTrue(instance.getTaskList().stream().limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().skip(2).map(Task::getDate).allMatch(Objects::isNull));

        Task undoTask = instance.getTaskList().get(3);
        assertEquals(UNDO, undoTask.getExpectedAction());
        taskController.performTask(undoTask.getId(), new PerformTaskRequest(UNDO, EXAMPLE_STEP1_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertTrue(instance.getTaskList().stream().limit(1).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().skip(1).map(Task::getDate).allMatch(Objects::isNull));

        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, READ, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList());
    }


    @Test void getInstance_historyAfterRecycle() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task startTask = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(startTask.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task step1Task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(step1Task.getId(), new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task step2Task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP2_ACTION, EXAMPLE_STEP2_DESK);
        taskController.performTask(step2Task.getId(), new PerformTaskRequest(EXAMPLE_STEP2_ACTION, EXAMPLE_STEP2_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task step3Task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP3_ACTION, EXAMPLE_STEP3_DESK);
        taskController.performTask(step3Task.getId(), new PerformTaskRequest(REJECT, EXAMPLE_STEP3_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task endTask = assertReadExistenceAndGetMainTask(instance.getTaskList(), DELETE, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(endTask.getId(), new PerformTaskRequest(RECYCLE, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        startTask = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(startTask.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        step1Task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(step1Task.getId(), new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(asList(START, VISA, VISA, SIGNATURE, EXTERNAL_SIGNATURE, SECURE_MAIL, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, VISA, null, null, null, null, null),
                taskList.stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());
        assertTrue(taskList.stream().limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(taskList.stream().skip(2).map(Task::getDate).allMatch(Objects::isNull));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, null, null, null, null, null),
                taskList.stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());

        // TODO : Test the real dates, for some reason, they are different in the CI.
        //  Probably a locale issue
        // assertTrue(instance.getTaskList().stream().map(Task::getDueDate).skip(3).allMatch(Objects::isNull));
        assertTrue(instance.getTaskList().stream().map(Task::getDueDate).limit(3).noneMatch(Objects::isNull));

        assertTrue(instance.getTaskList().stream().limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().skip(2).map(Task::getDate).allMatch(Objects::isNull));

        taskController.performTask(instance.getTaskList().get(2).getId(), new PerformTaskRequest(EXAMPLE_STEP2_ACTION, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, null, null, null, null),
                taskList.stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());
        assertTrue(taskList.stream().limit(3).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(taskList.stream().skip(3).map(Task::getDate).allMatch(Objects::isNull));
    }


    @Test void getInstance_historyAfterChain() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(EXAMPLE_STEP1_ACTION, EXAMPLE_STEP1_USER));
        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.size());
        assertTrue(taskList.stream().limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(taskList.stream().skip(2).map(Task::getDate).allMatch(Objects::isNull));
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.size());
        assertTrue(taskList.stream().limit(2).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(taskList.stream().skip(2).map(Task::getDate).allMatch(Objects::isNull));
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, null, null, null, null, null),
                taskList.stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, null, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());

        // TODO : Test the real dates, for some reason, they are different in the CI.
        //  Probably a locale issue
        // assertTrue(instance.getTaskList().stream().map(Task::getDueDate).skip(3).allMatch(Objects::isNull));
        assertTrue(taskList.stream().map(Task::getDueDate).limit(3).noneMatch(Objects::isNull));

        taskController.performTask(instance.getTaskList().get(2).getId(), new PerformTaskRequest(EXAMPLE_STEP2_ACTION, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.size());
        assertTrue(taskList.stream().limit(3).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(taskList.stream().skip(3).map(Task::getDate).allMatch(Objects::isNull));
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, null, null, null, null),
                taskList.stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, null, null, null, null),
                taskList.stream().map(Task::getAssignee).toList());
    }


    /**
     * Related to a specific bug, occurring when there are two identical workflow definition keys in two separate tenants.
     * Previous to the fix, the instances created from those workflow definitions could not be retrieved.
     * The previous behaviour (an exception thrown) can be observed by passing `null` as tenantId to the getInstance method.
     */
    @Test void getInstance_conflictOnWorkflowDefinitionKey() {

        WorkflowDefinition workflowDefinition1 = definitionController.createWorkflowDefinition(EXAMPLE_CONFLICT_WORKFLOW_DEFINITION_1, EXAMPLE_TENANT_ID);
        assertNotNull(workflowDefinition1);
        assertNotNull(workflowDefinition1.getDeploymentId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY, workflowDefinition1.getId());

        WorkflowDefinition workflowDefinition2 = definitionController.createWorkflowDefinition(EXAMPLE_CONFLICT_WORKFLOW_DEFINITION_2, EXAMPLE_TENANT_ID_02);
        assertNotNull(workflowDefinition2);
        assertNotNull(workflowDefinition2.getDeploymentId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_CONFLICT_KEY, workflowDefinition2.getId());

        EXAMPLE_INSTANCE_WITH_DEFINITION_CONFLICT.setBusinessKey(UUID.randomUUID().toString());

        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_WITH_DEFINITION_CONFLICT);

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertNotNull(instance);
        assertNotNull(instance.getId());
        assertNotNull(instance.getStartTime());

    }


    @Test void deleteInstance() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        assertEquals(
                1,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null,
                                null, null, null, null
                        )
                        .getData()
                        .size()
        );

        instanceController.deleteInstance(instance.getId());

        assertEquals(
                0,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null,
                                null, null, null, null
                        )
                        .getData()
                        .size()
        );
    }


}
