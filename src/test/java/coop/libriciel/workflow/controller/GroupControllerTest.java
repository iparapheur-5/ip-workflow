/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.TestUtils;
import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.WorkflowDefinition;
import coop.libriciel.workflow.models.requests.*;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.TaskInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static coop.libriciel.workflow.TestUtils.*;
import static coop.libriciel.workflow.models.SortBy.INSTANCE_ID;
import static coop.libriciel.workflow.models.State.DRAFT;
import static coop.libriciel.workflow.models.State.PENDING;
import static coop.libriciel.workflow.models.Task.Action.START;
import static coop.libriciel.workflow.models.WorkflowDefinition.SortBy.NAME;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class GroupControllerTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private InstanceController instanceController;
    @Autowired private GroupController groupController;
    @Autowired private TaskController taskController;

    @Autowired private TaskService taskService;
    @Autowired private RuntimeService runtimeService;
    @Autowired private HistoryService historyService;


    @BeforeEach void setup() {

        cleanup();

        definitionController.setupIParapheurDefinitions();

        WorkflowDefinition workflowDefinition = definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);
        assertNotNull(workflowDefinition);
        assertNotNull(workflowDefinition.getDeploymentId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_ID, workflowDefinition.getId());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_NAME, workflowDefinition.getName());

        // Creating test instances

        IntStream.range(0, 2)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_ORIGIN_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(new HashMap<>());
                    setBusinessKey(String.format("test2_%d", i));
                }})
                .forEach(instanceController::createInstance);

        IntStream.range(0, 3)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_ORIGIN_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(Map.of("Metadata01", "value 01"));
                    setBusinessKey(String.format("test3_M1_v1_%d", i));
                }})
                .forEach(instanceController::createInstance);

        assertEquals(5, groupController.countWorkflows(EXAMPLE_ORIGIN_DESK, DRAFT).getResult().intValue());

        IntStream.range(0, 6)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_STEP1_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(Map.of("Metadata01", "value 02"));
                    setBusinessKey(String.format("test6_M1_v2_%d", i));
                }})
                .forEach(instanceController::createInstance);

        assertEquals(6, groupController.countWorkflows(EXAMPLE_STEP1_DESK, DRAFT).getResult().intValue());

        IntStream.range(0, 7)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_STEP1_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(new HashMap<>());
                    setBusinessKey(String.format("test7_%d", i));
                }})
                .map(instanceController::createInstance)
                .map(i -> instanceController.getInstance(i.getId(), false, EXAMPLE_TENANT_ID))
                .map(i -> TestUtils.assertReadExistenceAndGetMainTask(i.getTaskList(), START, EXAMPLE_STEP1_DESK))
                .forEach(task -> taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_STEP1_USER)));

        IntStream.range(0, 11)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_STEP1_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(Map.of("Metadata01", "value 01"));
                    setBusinessKey(String.format("test11_M1V1_%d", i));
                }})
                .map(instanceController::createInstance)
                .map(i -> instanceController.getInstance(i.getId(), false, EXAMPLE_TENANT_ID))
                .map(i -> TestUtils.assertReadExistenceAndGetMainTask(i.getTaskList(), START, EXAMPLE_STEP1_DESK))
                .forEach(task -> taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_STEP1_USER)));

        IntStream.range(0, 1)
                .mapToObj(i -> new Instance() {{
                    setValidationWorkflowId(EXAMPLE_WORKFLOW_DEFINITION_ID);
                    setTenantId(EXAMPLE_TENANT_ID);
                    setOriginGroup(EXAMPLE_STEP1_DESK);
                    setFinalGroup(EXAMPLE_FINAL_DESK);
                    setVariables(Map.of("Metadata01", "value 02"));
                    setBusinessKey(String.format("test1_M1_v2_%d", i));
                }})
                .map(instanceController::createInstance)
                .map(i -> instanceController.getInstance(i.getId(), false, EXAMPLE_TENANT_ID))
                .map(i -> TestUtils.assertReadExistenceAndGetMainTask(i.getTaskList(), START, EXAMPLE_STEP1_DESK))
                .forEach(task -> taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_STEP1_USER)));

        assertEquals(19, groupController.countWorkflows(EXAMPLE_STEP1_DESK, PENDING).getResult().intValue());
    }


    @AfterEach void cleanup() {

        taskService.createNativeTaskQuery()
                .list().stream()
                .map(TaskInfo::getId)
                .forEach(i -> taskService.deleteTask(i));

        runtimeService.createProcessInstanceQuery()
                .list().stream()
                .map(ProcessInstance::getId)
                .forEach(i -> {
                    try {
                        runtimeService.deleteProcessInstance(i, "");
                    } catch (FlowableObjectNotFoundException f) {
                        /* If a parent process has been deleted, children will be unavailable. */
                    }
                });

        historyService.createHistoricTaskInstanceQuery()
                .list().stream()
                .map(TaskInfo::getId)
                .forEach(i -> historyService.deleteHistoricTaskInstance(i));

        historyService.createHistoricProcessInstanceQuery()
                .list().stream()
                .map(HistoricProcessInstance::getId)
                .forEach(i -> {
                    try {
                        historyService.deleteHistoricProcessInstance(i);
                    } catch (FlowableObjectNotFoundException e) {
                        /* If a parent process has been deleted, children will be unavailable. */
                    }
                });

        assertEquals(
                0,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null, null,
                                null, null, null
                        )
                        .getTotal());

        definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null)
                .getData()
                .stream()
                .map(WorkflowDefinition::getDeploymentId)
                .forEach(definitionController::deleteWorkflowDefinition);

        assertEquals(0, definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());

        assertEquals(0, taskService.createNativeTaskQuery().list().size());
        assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricTaskInstanceQuery().list().size());
        assertEquals(0, historyService.createHistoricProcessInstanceQuery().list().size());
    }


    @Test void countWorkflows() {

        assertEquals(5, groupController.countWorkflows(EXAMPLE_ORIGIN_DESK, DRAFT).getResult().intValue());
        assertEquals(6, groupController.countWorkflows(EXAMPLE_STEP1_DESK, DRAFT).getResult().intValue());

        assertEquals(0, groupController.countWorkflows(EXAMPLE_ORIGIN_DESK, PENDING).getResult().intValue());
        assertEquals(19, groupController.countWorkflows(EXAMPLE_STEP1_DESK, PENDING).getResult().intValue());
    }


    @Test void requestCountInstances_singleGroup() {

        List<FilteringParameterCountResult> request = groupController
                .requestCounts(new FilteredRequest(singletonList(FilteringParameter.builder().groupId(EXAMPLE_STEP1_DESK).build()), new FolderFilter()));

        assertEquals(2, request.size());

        FilteringParameterCountResult groupCount0 = request.get(0);
        assertEquals(Integer.valueOf(6), groupCount0.getCount());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount0.getGroupId());
        assertEquals(DRAFT, groupCount0.getState());
        assertNull(groupCount0.getFilterMetadataKey());
        assertNull(groupCount0.getFilterMetadataValue());

        FilteringParameterCountResult groupCount1 = request.get(1);
        assertEquals(Integer.valueOf(19), groupCount1.getCount());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount1.getGroupId());
        assertEquals(PENDING, groupCount1.getState());
        assertNull(groupCount1.getFilterMetadataKey());
        assertNull(groupCount1.getFilterMetadataValue());
    }


    @Test void requestCountInstances_multipleGroup() {

        List<FilteringParameter> request = asList(
                FilteringParameter.builder().groupId(EXAMPLE_ORIGIN_DESK).build(),
                FilteringParameter.builder().groupId(EXAMPLE_STEP1_DESK).build()
        );

        List<FilteringParameterCountResult> result = groupController.requestCounts(new FilteredRequest(request, new FolderFilter()));
        assertEquals(3, result.size());

        FilteringParameterCountResult groupCount0 = result.get(0);
        assertEquals(5, groupCount0.getCount().intValue());
        assertEquals(EXAMPLE_ORIGIN_DESK, groupCount0.getGroupId());
        assertEquals(DRAFT, groupCount0.getState());
        assertNull(groupCount0.getFilterMetadataKey());
        assertNull(groupCount0.getFilterMetadataValue());

        FilteringParameterCountResult groupCount1 = result.get(1);
        assertEquals(6, groupCount1.getCount().intValue());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount1.getGroupId());
        assertEquals(DRAFT, groupCount1.getState());
        assertNull(groupCount1.getFilterMetadataKey());
        assertNull(groupCount1.getFilterMetadataValue());

        FilteringParameterCountResult groupCount2 = result.get(2);
        assertEquals(19, groupCount2.getCount().intValue());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount2.getGroupId());
        assertEquals(PENDING, groupCount2.getState());
        assertNull(groupCount2.getFilterMetadataKey());
        assertNull(groupCount2.getFilterMetadataValue());
    }


    @Test void requestCountInstances_metadataFilter() {

        List<FilteringParameter> request = singletonList(
                FilteringParameter.builder().groupId(EXAMPLE_STEP1_DESK).state(PENDING).filterMetadataKey("Metadata01").filterMetadataValue("value 01").build()
        );

        List<FilteringParameterCountResult> result = groupController.requestCounts(new FilteredRequest(request, new FolderFilter()));
        assertEquals(1, result.size());

        FilteringParameterCountResult groupCount = result.get(0);
        assertEquals(11, groupCount.getCount().intValue());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount.getGroupId());
        assertEquals(PENDING, groupCount.getState());
        assertEquals("Metadata01", groupCount.getFilterMetadataKey());
        assertEquals("value 01", groupCount.getFilterMetadataValue());
    }


    @Test void requestCountInstances_mixedFilter() {

        List<FilteringParameter> request = asList(
                FilteringParameter.builder().groupId(EXAMPLE_ORIGIN_DESK).build(),
                FilteringParameter.builder().groupId(EXAMPLE_STEP1_DESK).state(PENDING).filterMetadataKey("Metadata01").filterMetadataValue("value 02").build()
        );

        List<FilteringParameterCountResult> result = groupController.requestCounts(new FilteredRequest(request, new FolderFilter()));
        assertEquals(2, result.size());

        FilteringParameterCountResult groupCount0 = result.get(0);
        assertEquals(5, groupCount0.getCount().intValue());
        assertEquals(EXAMPLE_ORIGIN_DESK, groupCount0.getGroupId());
        assertEquals(DRAFT, groupCount0.getState());
        assertNull(groupCount0.getFilterMetadataKey());
        assertNull(groupCount0.getFilterMetadataValue());

        FilteringParameterCountResult groupCount1 = result.get(1);
        assertEquals(1, groupCount1.getCount().intValue());
        assertEquals(EXAMPLE_STEP1_DESK, groupCount1.getGroupId());
        assertEquals(PENDING, groupCount1.getState());
        assertEquals("Metadata01", groupCount1.getFilterMetadataKey());
        assertEquals("value 02", groupCount1.getFilterMetadataValue());
    }


}
