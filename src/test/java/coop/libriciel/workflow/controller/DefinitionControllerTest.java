/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.StepDefinition;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.WorkflowDefinition;
import coop.libriciel.workflow.models.requests.FilteredRequest;
import coop.libriciel.workflow.models.requests.PerformTaskRequest;
import coop.libriciel.workflow.utils.XmlUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.BpmnModel;
import org.jooq.tools.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.transform.TransformerException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.IntStream;

import static coop.libriciel.workflow.TestUtils.*;
import static coop.libriciel.workflow.controller.DefinitionController.VERSION_NUMBER_COMPARATOR;
import static coop.libriciel.workflow.models.SortBy.INSTANCE_ID;
import static coop.libriciel.workflow.models.State.PENDING;
import static coop.libriciel.workflow.models.StepDefinition.ParallelType.AND;
import static coop.libriciel.workflow.models.StepDefinition.ParallelType.OR;
import static coop.libriciel.workflow.models.Task.Action.START;
import static coop.libriciel.workflow.models.Task.Action.VISA;
import static coop.libriciel.workflow.models.WorkflowDefinition.SortBy.NAME;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.Comparator.naturalOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class DefinitionControllerTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private InstanceController instanceController;
    @Autowired private GroupController groupController;
    @Autowired private TaskController taskController;


    @BeforeEach void setup() {
        this.cleanup();
        definitionController.setupIParapheurDefinitions();
    }


    @AfterEach void cleanup() {

        int maxVersion = definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null)
                .getData()
                .stream()
                .map(WorkflowDefinition::getVersion)
                .max(naturalOrder())
                .orElse(1);

        // If we have versions > 1, we have to delete the current version,
        // then re-list, and trigger delete with the "n-1" version's id fetched, and so on.

        IntStream.range(0, maxVersion)
                .mapToObj(i -> definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getData())
                .flatMap(Collection::stream)
                .forEach(d -> definitionController.deleteWorkflowDefinition(d.getDeploymentId()));

        assertEquals(0, definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());
    }


    @SuppressWarnings("EqualsWithItself")
    @Test void versionNumberComparator() {
        assertEquals(0, VERSION_NUMBER_COMPARATOR.compare(Triple.of(2, 5, 7), Triple.of(2, 5, 7)));
        assertEquals(1, VERSION_NUMBER_COMPARATOR.compare(Triple.of(3, 0, 0), Triple.of(2, 99, 99)));
        assertEquals(-1, VERSION_NUMBER_COMPARATOR.compare(Triple.of(2, 0, 99), Triple.of(2, 1, 0)));
    }


    @Test void getVersionFromDefinition() {
        assertEquals(Triple.of(2, 5, 7), DefinitionController.getVersionFromDefinition("Test\nVersion 2.5.7\nTest"));
        assertEquals(Triple.of(2, 5, 0), DefinitionController.getVersionFromDefinition("Test\nVersion 2.5\nTest"));
        assertEquals(Triple.of(2, 0, 0), DefinitionController.getVersionFromDefinition("Test\nVersion 2\nTest"));
        assertEquals(Triple.of(1, 0, 0), DefinitionController.getVersionFromDefinition("Test\nTest Version 99\nTest"));
        assertEquals(Triple.of(1, 0, 0), DefinitionController.getVersionFromDefinition("Test\nVersion 99 test\nTest"));
        assertEquals(Triple.of(123, 456, 789), DefinitionController.getVersionFromDefinition("Version 123.456.789"));
    }


    @Test void definitionToBpmn() throws TransformerException {

        BpmnXMLConverter converter = new BpmnXMLConverter();
        BpmnModel definition = DefinitionController.definitionToBpmn(EXAMPLE_WORKFLOW_DEFINITION);
        String definitionString = new String(converter.convertToXML(definition), UTF_8);

        assertNotNull(definitionString);
        definitionString = XmlUtils.canonicalize(definitionString.getBytes());

        String expected = XmlUtils.canonicalize(
                """
                <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
                             xmlns:flowable="http://flowable.org/bpmn" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
                             xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" expressionLanguage="http://www.w3.org/1999/XPath"
                             targetNamespace="http://www.flowable.org/test" typeLanguage="http://www.w3.org/2001/XMLSchema">
                  <process id="generated_workflow_01" isExecutable="true" name="Generated workflow">
                    <documentation>Straightforward visa-signature workflow</documentation>
                    <startEvent id="startEvent1"></startEvent>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_user01" name="visa user01" flowable:fallbackToDefaultTenant="true"
                                  flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk01" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_start" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="0" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in sourceExpression="metadata_01" target="workflow_internal_mandatory_rejection_metadata"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-startEvent1-visa_user01" sourceRef="startEvent1" targetRef="visa_user01"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_user02" name="visa user02" flowable:fallbackToDefaultTenant="true"
                                  flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk02" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk01" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_visa" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="1" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in sourceExpression="notified_01" target="workflow_internal_notified_groups"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_user01-visa_user02" sourceRef="visa_user01" targetRef="visa_user02"></sequenceFlow>
                    <boundaryEvent attachedToRef="visa_user02" id="visa_user02_undo_catch">
                      <errorEventDefinition errorRef="undo"></errorEventDefinition>
                    </boundaryEvent>
                    <sequenceFlow id="sid-visa_user02_undo_catch-visa_user01" sourceRef="visa_user02_undo_catch" targetRef="visa_user01"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_signature" id="signature_user02" name="signature user03"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk03" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk02" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_visa" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="2" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_user02-signature_user02" sourceRef="visa_user02" targetRef="signature_user02"></sequenceFlow>
                    <boundaryEvent attachedToRef="signature_user02" id="signature_user02_undo_catch">
                      <errorEventDefinition errorRef="undo"></errorEventDefinition>
                    </boundaryEvent>
                    <sequenceFlow id="sid-signature_user02_undo_catch-visa_user02" sourceRef="signature_user02_undo_catch" targetRef="visa_user02"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_external_signature" id="external_signature_user02" name="external signature user04"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk04" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk03" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_signature" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="3" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-signature_user02-external_signature_user02" sourceRef="signature_user02" targetRef="external_signature_user02"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_secure_mail" id="secure_mail_user02" name="secure mail user05"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk05" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk04" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_external_signature" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="4" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-external_signature_user02-secure_mail_user02" sourceRef="external_signature_user02" targetRef="secure_mail_user02"></sequenceFlow>
                    <endEvent id="endEvent1"></endEvent>
                    <sequenceFlow id="sid-secure_mail_user02-endEvent1" sourceRef="secure_mail_user02" targetRef="endEvent1"></sequenceFlow>
                    <dataObject id="i_Parapheur_internal_final_desk_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_desk_attr">
                      <extensionElements>
                        <flowable:value>desk99</flowable:value>
                      </extensionElements>
                    </dataObject>
                    <dataObject id="i_Parapheur_internal_final_notified_desks_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_notified_desks_attr">
                      <extensionElements>
                        <flowable:value></flowable:value>
                      </extensionElements>
                    </dataObject>
                  </process>
                  <bpmndi:BPMNDiagram id="BPMNDiagram_generated_workflow_01">
                    <bpmndi:BPMNPlane bpmnElement="generated_workflow_01" id="BPMNPlane_generated_workflow_01"></bpmndi:BPMNPlane>
                  </bpmndi:BPMNDiagram>
                </definitions>
                """.getBytes()
        );

        assertEquals(expected, definitionString);
    }


    @Test void definitionToBpmn_orStep() throws TransformerException {

        WorkflowDefinition orDefinition = new WorkflowDefinition();
        orDefinition.setId("id_or");
        orDefinition.setName("Name or");
        orDefinition.setKey("id_or");
        orDefinition.setSteps(asList(
                new StepDefinition(
                        EXAMPLE_STEP1_ACTION,
                        "visa_desk01_or_desk02",
                        "visa desk01 or desk02",
                        asList(EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK),
                        emptyList(),
                        emptyList(),
                        emptyList(),
                        OR
                ),
                new StepDefinition(
                        EXAMPLE_STEP2_ACTION,
                        "visa_desk03",
                        "visa desk03",
                        singletonList(EXAMPLE_STEP3_DESK),
                        emptyList(),
                        emptyList(),
                        emptyList(),
                        OR
                )
        ));
        orDefinition.setFinalDeskId(EXAMPLE_FINAL_DESK);
        orDefinition.setFinalNotifiedDeskIds(List.of(EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK));

        BpmnXMLConverter converter = new BpmnXMLConverter();
        BpmnModel definition = DefinitionController.definitionToBpmn(orDefinition);
        String definitionString = new String(converter.convertToXML(definition), UTF_8);

        assertNotNull(definitionString);
        definitionString = XmlUtils.canonicalize(definitionString.getBytes());

        String expected = XmlUtils.canonicalize(
                """
                <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
                             xmlns:flowable="http://flowable.org/bpmn" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
                             xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" expressionLanguage="http://www.w3.org/1999/XPath"
                             targetNamespace="http://www.flowable.org/test" typeLanguage="http://www.w3.org/2001/XMLSchema">
                  <process id="id_or" isExecutable="true" name="Name or">
                    <documentation>Straightforward visa-signature workflow</documentation>
                    <startEvent id="startEvent1"></startEvent>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_desk01_or_desk02" name="visa desk01 or desk02"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk01,desk02" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_start" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="0" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-startEvent1-visa_desk01_or_desk02" sourceRef="startEvent1" targetRef="visa_desk01_or_desk02"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_desk03" name="visa desk03" flowable:fallbackToDefaultTenant="true"
                                  flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk03" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk01,desk02" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_visa" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="1" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_desk01_or_desk02-visa_desk03" sourceRef="visa_desk01_or_desk02" targetRef="visa_desk03"></sequenceFlow>
                    <endEvent id="endEvent1"></endEvent>
                    <sequenceFlow id="sid-visa_desk03-endEvent1" sourceRef="visa_desk03" targetRef="endEvent1"></sequenceFlow>
                    <dataObject id="i_Parapheur_internal_final_desk_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_desk_attr">
                      <extensionElements>
                        <flowable:value>desk99</flowable:value>
                      </extensionElements>
                    </dataObject>
                    <dataObject id="i_Parapheur_internal_final_notified_desks_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_notified_desks_attr">
                      <extensionElements>
                        <flowable:value>desk01,desk02</flowable:value>
                      </extensionElements>
                    </dataObject>
                  </process>
                  <bpmndi:BPMNDiagram id="BPMNDiagram_id_or">
                    <bpmndi:BPMNPlane bpmnElement="id_or" id="BPMNPlane_id_or"></bpmndi:BPMNPlane>
                  </bpmndi:BPMNDiagram>
                </definitions>
                """.getBytes());

        assertEquals(expected, definitionString);
    }


    @Test void definitionToBpmn_andStep() throws TransformerException {

        WorkflowDefinition orDefinition = new WorkflowDefinition();
        orDefinition.setId("id_and");
        orDefinition.setName("Name and");
        orDefinition.setKey("id_and");
        orDefinition.setSteps(asList(
                new StepDefinition(EXAMPLE_STEP1_ACTION, "visa_desk01_and_desk02", "visa desk01 and desk02", asList(EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK), emptyList(), emptyList(), emptyList(), AND),
                new StepDefinition(EXAMPLE_STEP2_ACTION, "visa_desk03", "visa desk03", singletonList(EXAMPLE_STEP3_DESK), emptyList(), emptyList(), emptyList(), OR)
        ));
        orDefinition.setFinalDeskId(EXAMPLE_FINAL_DESK);

        BpmnXMLConverter converter = new BpmnXMLConverter();
        BpmnModel definition = DefinitionController.definitionToBpmn(orDefinition);
        String definitionString = new String(converter.convertToXML(definition), UTF_8);

        assertNotNull(definitionString);
        definitionString = XmlUtils.canonicalize(definitionString.getBytes());

        String expected = XmlUtils.canonicalize(
                """
                <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
                             xmlns:flowable="http://flowable.org/bpmn" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
                             xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" expressionLanguage="http://www.w3.org/1999/XPath"
                             targetNamespace="http://www.flowable.org/test" typeLanguage="http://www.w3.org/2001/XMLSchema">
                  <process id="id_and" isExecutable="true" name="Name and">
                    <documentation>Straightforward visa-signature workflow</documentation>
                    <startEvent id="startEvent1"></startEvent>
                    <parallelGateway id="visa_desk01_and_desk02_start_parallel_gateway"></parallelGateway>
                    <parallelGateway id="visa_desk01_and_desk02"></parallelGateway>
                    <sequenceFlow id="sid-startEvent1-visa_desk01_and_desk02_start_parallel_gateway" sourceRef="startEvent1"
                                  targetRef="visa_desk01_and_desk02_start_parallel_gateway"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_desk01_and_desk02_validator_0" name="visa desk01 and desk02 validator_1"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk01" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_start" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="0" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_desk01_and_desk02_start_parallel_gateway-visa_desk01_and_desk02_validator_0"
                                  sourceRef="visa_desk01_and_desk02_start_parallel_gateway" targetRef="visa_desk01_and_desk02_validator_0"></sequenceFlow>
                    <sequenceFlow id="sid-visa_desk01_and_desk02_validator_0-visa_desk01_and_desk02" sourceRef="visa_desk01_and_desk02_validator_0"
                                  targetRef="visa_desk01_and_desk02"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_desk01_and_desk02_validator_1" name="visa desk01 and desk02 validator_2"
                                  flowable:fallbackToDefaultTenant="true" flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk02" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_start" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="0" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_desk01_and_desk02_start_parallel_gateway-visa_desk01_and_desk02_validator_1"
                                  sourceRef="visa_desk01_and_desk02_start_parallel_gateway" targetRef="visa_desk01_and_desk02_validator_1"></sequenceFlow>
                    <sequenceFlow id="sid-visa_desk01_and_desk02_validator_1-visa_desk01_and_desk02" sourceRef="visa_desk01_and_desk02_validator_1"
                                  targetRef="visa_desk01_and_desk02"></sequenceFlow>
                    <callActivity calledElement="i_Parapheur_internal_visa" id="visa_desk03" name="visa desk03" flowable:fallbackToDefaultTenant="true"
                                  flowable:inheritBusinessKey="true" flowable:inheritVariables="true">
                      <extensionElements>
                        <flowable:in sourceExpression="desk03" target="workflow_internal_current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="desk01,desk02" target="workflow_internal_previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="i_Parapheur_internal_visa" target="workflow_internal_previous_action"></flowable:in>
                        <flowable:in sourceExpression="1" target="workflow_internal_step_index"></flowable:in>
                        <flowable:in source="i_Parapheur_internal_due_date" target="i_Parapheur_internal_due_date"></flowable:in>
                        <flowable:in source="workflow_internal_workflow_index" target="workflow_internal_workflow_index"></flowable:in>
                      </extensionElements>
                    </callActivity>
                    <sequenceFlow id="sid-visa_desk01_and_desk02-visa_desk03" sourceRef="visa_desk01_and_desk02" targetRef="visa_desk03"></sequenceFlow>
                    <endEvent id="endEvent1"></endEvent>
                    <sequenceFlow id="sid-visa_desk03-endEvent1" sourceRef="visa_desk03" targetRef="endEvent1"></sequenceFlow>
                    <dataObject id="i_Parapheur_internal_final_desk_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_desk_attr">
                      <extensionElements>
                        <flowable:value>desk99</flowable:value>
                      </extensionElements>
                    </dataObject>
                    <dataObject id="i_Parapheur_internal_final_notified_desks_attr" itemSubjectRef="xsd:string" name="i_Parapheur_internal_final_notified_desks_attr">
                      <extensionElements>
                        <flowable:value></flowable:value>
                      </extensionElements>
                    </dataObject>
                  </process>
                  <bpmndi:BPMNDiagram id="BPMNDiagram_id_and">
                    <bpmndi:BPMNPlane bpmnElement="id_and" id="BPMNPlane_id_and"></bpmndi:BPMNPlane>
                  </bpmndi:BPMNDiagram>
                </definitions>
                """.getBytes());

        assertEquals(expected, definitionString);
    }


    @SuppressWarnings("SpellCheckingInspection")
    @Test void patchBpmn_bpmnIo() throws Exception {

        InputStream simpleWorkflowInputStream = getClass()
                .getClassLoader()
                .getResourceAsStream("bpmn/http.bpmn.io_visaDRH_visaDGS_signPresident.bpmn20.xml");

        String result = DefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
        String expected =
                """
                <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn" \
                exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="3.2.1" id="Definitions_1osu5pz" targetNamespace="http://bpmn.io/schema/bpmn">
                  <bpmn:process id="Process_1lzk2p2" isExecutable="true" name="Process_1lzk2p2">
                    <bpmn:startEvent id="StartEvent_0gowkhg">
                      <bpmn:outgoing>SequenceFlow_0jia701</bpmn:outgoing>
                    </bpmn:startEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0jia701" sourceRef="StartEvent_0gowkhg" targetRef="Task_0tu55t0"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0889tye" sourceRef="Task_0tu55t0" targetRef="Task_0ii92vr"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0sgwjie" sourceRef="Task_0ii92vr" targetRef="Task_08nvn6g"></bpmn:sequenceFlow>
                    <bpmn:endEvent id="EndEvent_0cokryq">
                      <bpmn:incoming>SequenceFlow_0iuof41</bpmn:incoming>
                    </bpmn:endEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0iuof41" sourceRef="Task_08nvn6g" targetRef="EndEvent_0cokryq"></bpmn:sequenceFlow>
                    <bpmn:subProcess id="Task_0tu55t0" name="visa DRH">
                      <bpmn:incoming>SequenceFlow_0jia701</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0889tye</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_0ii92vr" name="visa DGS">
                      <bpmn:incoming>SequenceFlow_0889tye</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0sgwjie</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_08nvn6g" name="signature Président">
                      <bpmn:incoming>SequenceFlow_0sgwjie</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0iuof41</bpmn:outgoing>
                    </bpmn:subProcess>
                  </bpmn:process>
                </bpmn:definitions>""";

        assertEquals(expected, result);
    }


    //    @SuppressWarnings("SpellCheckingInspection")
//    @Test void patchBpmn_flowableModeler() throws Exception {
//
//        InputStream simpleWorkflowInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/flowableModeler_simple_workflow.bpmn20.xml");
//        String result = DefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
//        String expected = "" +
//                          "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:flowable=\"http://flowable.org/bpmn\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://www.flowable.org/processdef\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\">\n" +
//                          "  <process xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"simple_workflow\" isExecutable=\"true\" name=\"Simple workflow\">\n" +
//                          "    <documentation>Straightforward visa-signature workflow</documentation>\n" +
//                          "    <startEvent id=\"startEvent1\"></startEvent>\n" +
//                          "    <callActivity calledElement=\"i_Parapheur_internal_visa\" id=\"visa_DRH\" name=\"visa DRH\" flowable:inheritBusinessKey=\"true\" flowable:inheritVariables=\"false\">\n" +
//                          "      <bpmn:extensionElements>\n" +
//                          "        <flowable:in sourceExpression=\"DRH\" target=\"current_candidate_groups\"></flowable:in>\n" +
//                          "        <flowable:in sourceExpression=\"admins\" target=\"previous_candidate_groups\"></flowable:in>\n" +
//                          "        <flowable:in sourceExpression=\"i_Parapheur_internal_unknown\" target=\"previous_action\"></flowable:in>\n" +
//                          "        <flowable:in source=\"i_Parapheur_internal_due_date\" target=\"i_Parapheur_internal_due_date\"></flowable:in>\n" +
//                          "      </bpmn:extensionElements>\n" +
//                          "    </callActivity>\n" +
//                          "    <callActivity calledElement=\"i_Parapheur_internal_signature\" id=\"signature_President\" name=\"signature Président\" flowable:inheritBusinessKey=\"true\" flowable:inheritVariables=\"false\">\n" +
//                          "      <bpmn:extensionElements>\n" +
//                          "        <flowable:in sourceExpression=\"Président\" target=\"current_candidate_groups\"></flowable:in>\n" +
//                          "        <flowable:in sourceExpression=\"DRH\" target=\"previous_candidate_groups\"></flowable:in>\n" +
//                          "        <flowable:in sourceExpression=\"i_Parapheur_internal_visa\" target=\"previous_action\"></flowable:in>\n" +
//                          "        <flowable:in source=\"i_Parapheur_internal_due_date\" target=\"i_Parapheur_internal_due_date\"></flowable:in>\n" +
//                          "      </bpmn:extensionElements>\n" +
//                          "    </callActivity>\n" +
//                          "    <bpmn:boundaryEvent attachedToRef=\"signature_President\" id=\"signature_President_undo_catch\">\n" +
//                          "      <bpmn:errorEventDefinition errorRef=\"undo\"></bpmn:errorEventDefinition>\n" +
//                          "    </bpmn:boundaryEvent>\n" +
//                          "    <bpmn:sequenceFlow id=\"signature_President_undo_catch_back_sequenceflow\" sourceRef=\"signature_President_undo_catch\" targetRef=\"visa_DRH\"></bpmn:sequenceFlow>\n" +
//                          "    <endEvent id=\"sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7\"></endEvent>\n" +
//                          "    <sequenceFlow id=\"sid-24850A4C-275C-4A43-9608-CF356255317F\" sourceRef=\"signature_President\" targetRef=\"sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7\"></sequenceFlow>\n" +
//                          "    <sequenceFlow id=\"sid-E81E772C-1D9E-433F-8AAE-7F5F3B5C5925\" sourceRef=\"startEvent1\" targetRef=\"visa_DRH\"></sequenceFlow>\n" +
//                          "    <sequenceFlow id=\"sid-D05D909D-956E-4A7F-BA5D-6008F4DC6541\" sourceRef=\"visa_DRH\" targetRef=\"signature_President\"></sequenceFlow>\n" +
//                          "  </process>\n" +
//                          "</bpmn:definitions>";
//
//        Assert.assertEquals(expected, result);
//    }


    @Test void createWorkflowDefinition() {

        WorkflowDefinition definition = definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);

        assertNotNull(definition);
        assertNotNull(definition.getId());
        assertEquals(1, definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());
    }


    @Test void createWorkflowDefinition_updateVersion() {

        IntStream.range(0, 50)
                .forEach(i -> definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID));

        assertEquals(1, definitionController.getWorkflowDefinitions(0, 1, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());
        assertEquals(1, definitionController.getWorkflowDefinitions(0, 1, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());
        assertEquals(1, definitionController.getWorkflowDefinitions(0, 25, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());
        assertEquals(1, definitionController.getWorkflowDefinitions(0, 25, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());
        assertEquals(50, definitionController.getWorkflowDefinitions(0, 25, EXAMPLE_TENANT_ID, NAME, true, null).getData().get(0).getVersion());
    }


    @Test void createWorkflowDefinition_orSteps() {

        WorkflowDefinition orDefinition = new WorkflowDefinition();
        orDefinition.setId("id_or");
        orDefinition.setName("Name or");
        orDefinition.setKey("id_or");
        orDefinition.setFinalDeskId(EXAMPLE_FINAL_DESK);
        orDefinition.setSteps(asList(
                new StepDefinition(VISA, "visa_user01_or_user02", EXAMPLE_STEP1_NAME, asList(EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK), emptyList(), emptyList(), emptyList(), OR),
                new StepDefinition(VISA, "visa_user03", EXAMPLE_STEP2_NAME, singletonList(EXAMPLE_STEP3_DESK), emptyList(), emptyList(), emptyList(), OR)
        ));

        definitionController.createWorkflowDefinition(orDefinition, EXAMPLE_TENANT_ID);

        Instance orInstance = instanceController.createInstance(new Instance() {{
            setValidationWorkflowId(orDefinition.getId());
            setTenantId(EXAMPLE_TENANT_ID);
            setOriginGroup(EXAMPLE_ORIGIN_DESK);
            setBusinessKey(UUID.randomUUID().toString());
            setVariables(emptyMap());
        }});

        orInstance = instanceController.getInstance(orInstance.getId(), false, EXAMPLE_TENANT_ID);
        Task startTask = assertReadExistenceAndGetMainTask(orInstance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(startTask.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        // Test parallel events

        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP3_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());

        Task user02Task = groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getData().get(0);
        taskController.performTask(user02Task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));

        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP3_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());

        // Cleanup

        instanceController.deleteInstance(orInstance.getId());
    }


    @Test void createWorkflowDefinition_andSteps() {

        WorkflowDefinition orDefinition = new WorkflowDefinition();
        orDefinition.setId("id_and");
        orDefinition.setName("Name and");
        orDefinition.setKey("id_and");
        orDefinition.setSteps(asList(
                new StepDefinition(VISA, "visa_user01_and_user02", EXAMPLE_STEP1_NAME, asList(EXAMPLE_STEP1_DESK, EXAMPLE_STEP2_DESK), emptyList(), emptyList(), emptyList(), AND),
                new StepDefinition(VISA, "visa_user03", EXAMPLE_STEP2_NAME, singletonList(EXAMPLE_STEP3_DESK), emptyList(), emptyList(), emptyList(), OR)
        ));
        orDefinition.setFinalDeskId(EXAMPLE_FINAL_DESK);

        definitionController.createWorkflowDefinition(orDefinition, EXAMPLE_TENANT_ID);

        Instance orInstance = instanceController.createInstance(new Instance() {{
            setValidationWorkflowId(orDefinition.getId());
            setTenantId(EXAMPLE_TENANT_ID);
            setOriginGroup(EXAMPLE_ORIGIN_DESK);
            setBusinessKey(UUID.randomUUID().toString());
            setVariables(emptyMap());
        }});

        orInstance = instanceController.getInstance(orInstance.getId(), true, EXAMPLE_TENANT_ID);
        assertNotNull(orInstance);
        // FIXME : Task startTask = assertReadExistenceAndGetMainTask(orInstance.getTaskList(), START, EXAMPLE_ORIGIN_USER);
        Task startTask = orInstance.getTaskList().stream().filter(t -> t.getExpectedAction() == START).findFirst().orElseGet(Assertions::fail);

        assertNotNull(startTask);
        taskController.performTask(startTask.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        // Test parallel events

        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP3_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());

        Task user02Task = groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getData().get(0);
        taskController.performTask(user02Task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));

        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP3_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());

        Task user01Task = groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getData().get(0);
        taskController.performTask(user01Task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));

        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP1_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(0, groupController.requestTasks(EXAMPLE_STEP2_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());
        assertEquals(1, groupController.requestTasks(EXAMPLE_STEP3_DESK, PENDING, new FilteredRequest(), INSTANCE_ID, true, 0, MAX_VALUE).getTotal());

        // Cleanup

        instanceController.deleteInstance(orInstance.getId());
    }


    //    @Test void createWorkflowDefinition_badBpmn() {
    //        try {
    //            definitionController.createWorkflowDefinition(
    //                    new CreateWorkflowDefinitionRequest(
    //                            "<<<< badXml !! <<<",
    //                            "plop",
    //                            true)
    //            );
    //        } catch (ResponseStatusException e) {
    //            Assert.assertEquals(400, e.getStatus().value());
    //        }
    //    }


    @Test void listWorkflowDefinition_pagination() {

        definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);

        assertEquals(1, definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());
        assertEquals(1, definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());

        assertEquals(0, definitionController.getWorkflowDefinitions(0, 50, "bad_tenant", NAME, true, null).getData().size());
        assertEquals(0, definitionController.getWorkflowDefinitions(0, 50, "bad_tenant", NAME, true, null).getTotal());

        assertEquals(0, definitionController.getWorkflowDefinitions(999, 50, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());
        assertEquals(1, definitionController.getWorkflowDefinitions(999, 50, EXAMPLE_TENANT_ID, NAME, true, null).getTotal());
    }


    @Test void listWorkflowDefinition_search() {

        definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);

        String validSearchTerm = EXAMPLE_WORKFLOW_DEFINITION_NAME.substring(3, 6);
        var successfulSearch = definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, validSearchTerm);
        assertEquals(1, successfulSearch.getData().size());
        assertEquals(1, successfulSearch.getTotal());
        assertEquals(EXAMPLE_WORKFLOW_DEFINITION_NAME, successfulSearch.getData().get(0).getName());

        var unsuccessfulSearch = definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, "bad_search");
        assertEquals(0, unsuccessfulSearch.getData().size());
        assertEquals(0, unsuccessfulSearch.getTotal());
    }


    @Test void listWorkflowDefinition_sort() {

        WorkflowDefinition example = new WorkflowDefinition();
        example.setSteps(emptyList());
        example.setFinalDeskId(EXAMPLE_FINAL_DESK);
        example.setId("id01");
        example.setName("Name 01");
        definitionController.createWorkflowDefinition(example, EXAMPLE_TENANT_ID);
        example.setId("id02");
        example.setName("Name 02");
        definitionController.createWorkflowDefinition(example, EXAMPLE_TENANT_ID);
        example.setId("id03");
        example.setName("Name 03");
        definitionController.createWorkflowDefinition(example, EXAMPLE_TENANT_ID);

        // Natural order

        var naturalOrderList = definitionController
                .getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null);

        assertEquals(3, naturalOrderList.getData().size());
        assertEquals(3, naturalOrderList.getTotal());
        assertEquals("Name 01", naturalOrderList.getData().get(0).getName());
        assertEquals("Name 02", naturalOrderList.getData().get(1).getName());
        assertEquals("Name 03", naturalOrderList.getData().get(2).getName());

        // Reverse order

        var reverseOrderList = definitionController
                .getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, false, null);

        assertEquals(3, reverseOrderList.getData().size());
        assertEquals(3, reverseOrderList.getTotal());
        assertEquals("Name 03", reverseOrderList.getData().get(0).getName());
        assertEquals("Name 02", reverseOrderList.getData().get(1).getName());
        assertEquals("Name 01", reverseOrderList.getData().get(2).getName());
    }


    @Test void listWorkflowDefinition_count() {

        definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);
        List<WorkflowDefinition> definitions = definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getData();
        Optional<WorkflowDefinition> def = definitions.stream().filter(d -> StringUtils.equals(d.getKey(), EXAMPLE_WORKFLOW_DEFINITION_ID)).findFirst();
        assertTrue(def.isPresent());
        assertEquals(0, def.get().getUsageCount());

        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());
        definitions = definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getData();
        def = definitions.stream().filter(d -> StringUtils.equals(d.getKey(), EXAMPLE_WORKFLOW_DEFINITION_ID)).findFirst();
        assertTrue(def.isPresent());
        assertEquals(1, def.get().getUsageCount());

        instanceController.deleteInstance(instance.getId());
        definitions = definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null).getData();
        def = definitions.stream().filter(d -> StringUtils.equals(d.getKey(), EXAMPLE_WORKFLOW_DEFINITION_ID)).findFirst();
        assertTrue(def.isPresent());
        assertEquals(0, def.get().getUsageCount());
    }


    @Test void deleteWorkflowDefinition() {

        definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);
        assertEquals(1, definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());

        definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null)
                .getData()
                .stream()
                .findAny()
                .ifPresent(def -> definitionController.deleteWorkflowDefinition(def.getDeploymentId()));

        assertEquals(0, definitionController.getWorkflowDefinitions(0, 50, EXAMPLE_TENANT_ID, NAME, true, null).getData().size());
    }


    @Test void deleteWorkflowDefinition_unknownId() {
        try {
            definitionController.deleteWorkflowDefinition("unknown id !!");
        } catch (ResponseStatusException e) {
            assertEquals(NOT_FOUND, e.getStatus());
        }
    }


}
