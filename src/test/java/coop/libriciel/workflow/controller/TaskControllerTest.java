/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.TestUtils;
import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.WorkflowDefinition;
import coop.libriciel.workflow.models.requests.PerformTaskRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static coop.libriciel.workflow.TestUtils.*;
import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.SortBy.INSTANCE_ID;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.WorkflowDefinition.SortBy.NAME;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TaskControllerTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private InstanceController instanceController;
    @Autowired private ArchiveController archiveController;
    @Autowired private TaskController taskController;


    @BeforeEach void setup() {
        cleanup();
        definitionController.setupIParapheurDefinitions();
        definitionController.createWorkflowDefinition(EXAMPLE_WORKFLOW_DEFINITION, EXAMPLE_TENANT_ID);
    }


    @AfterEach void cleanup() {

        instanceController
                .getInstances(
                        EXAMPLE_TENANT_ID, 0, MAX_VALUE, INSTANCE_ID, true,
                        null, null, null, null,
                        null, null, null, null
                )
                .getData()
                .stream()
                .map(Task::getInstanceId)
                .forEach(instanceController::deleteInstance);

        definitionController.getWorkflowDefinitions(0, MAX_VALUE, EXAMPLE_TENANT_ID, NAME, true, null)
                .getData()
                .stream()
                .map(WorkflowDefinition::getDeploymentId)
                .forEach(definitionController::deleteWorkflowDefinition);

        archiveController.getHistoricInstances(EXAMPLE_TENANT_ID, 0, MAX_VALUE, INSTANCE_ID, true, null)
                .getData()
                .stream()
                .map(Instance::getId)
                .forEach(archiveController::deleteHistoricInstance);

        assertEquals(
                0,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null,
                                null, null, null, null
                        )
                        .getTotal()
        );

        assertEquals(
                0,
                instanceController
                        .getInstances(
                                EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true,
                                null, null, null, null,
                                null, null, null, null
                        )
                        .getTotal()
        );

        assertEquals(0, archiveController.getHistoricInstances(EXAMPLE_TENANT_ID, 0, 50, INSTANCE_ID, true, null).getTotal());
    }


    @Test void performTask_visa() {

        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        assertEquals(asList(START, VISA, READ, VISA, SIGNATURE, EXTERNAL_SIGNATURE, SECURE_MAIL, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList());
        assertTrue(instance.getTaskList().stream().skip(1).map(Task::getDate).allMatch(Objects::isNull));
        assertTrue(instance.getTaskList().stream().limit(1).map(Task::getDate).allMatch(Objects::nonNull));

        taskController.performTask(
                instance.getTaskList().get(1).getId(),
                new PerformTaskRequest(
                        VISA,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(
                                "key1", "value1",
                                "key2", "value2"
                        )
                )
        );

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        assertNotNull(instance.getTaskList().get(0).getDate());
        assertTrue(2 <= instance.getTaskList().get(1).getVariables().size());
        assertEquals("value1", instance.getTaskList().get(1).getVariables().get("key1"));
        assertEquals("value2", instance.getTaskList().get(1).getVariables().get("key2"));
    }


    @Test void performTask_reject() {

        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        assertEquals(asList(START, VISA, READ, VISA, SIGNATURE, EXTERNAL_SIGNATURE, SECURE_MAIL, ARCHIVE),
                instance.getTaskList().stream().map(Task::getExpectedAction).toList());
        assertEquals(asList(START, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getPerformedAction).toList());
        assertEquals(asList(EXAMPLE_ORIGIN_USER, null, null, null, null, null, null, null),
                instance.getTaskList().stream().map(Task::getAssignee).toList());
        assertTrue(instance.getTaskList().stream().limit(1).map(Task::getDate).allMatch(Objects::nonNull));
        assertTrue(instance.getTaskList().stream().skip(1).map(Task::getDate).allMatch(Objects::isNull));

        taskController.performTask(
                instance.getTaskList().get(1).getId(),
                new PerformTaskRequest(
                        REJECT,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP2_DESK,
                        null,
                        Map.of(
                                "key1", "value1",
                                "key2", "value2"
                        )
                )
        );

        // Finalize

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), DELETE, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(ARCHIVE, EXAMPLE_ORIGIN_USER));

        // History check

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);

        assertEquals(3, instance.getTaskList().size());
        assertTrue(instance.getTaskList().stream().map(Task::getDate).allMatch(Objects::nonNull));
        assertEquals(asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_ORIGIN_USER),
                instance.getTaskList().stream().map(Task::getAssignee).toList());
    }


    @Test void performTask_externalSignature_directBypass() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP1_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP2_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SIGNATURE, EXAMPLE_STEP3_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(SIGNATURE, EXAMPLE_STEP3_USER));

        // First sub-tasks of EXTERNAL_SIGNATURE

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXTERNAL_SIGNATURE, EXAMPLE_STEP4_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP4_USER));

        // Finalize

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SECURE_MAIL, EXAMPLE_STEP5_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP5_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), ARCHIVE, EXAMPLE_FINAL_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(ARCHIVE, EXAMPLE_FINAL_USER));

        // History check

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.stream().filter(s -> s.getExpectedAction() != READ).toList().size());
        assertTrue(taskList.stream().map(Task::getDate).allMatch(Objects::nonNull));
        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, EXAMPLE_STEP3_USER, EXAMPLE_STEP4_USER, EXAMPLE_STEP5_USER, EXAMPLE_FINAL_USER),
                taskList.stream().map(Task::getAssignee).toList()
        );
    }


    @Test void performTask_externalSignature_fillingThenBypass() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP1_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP2_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SIGNATURE, EXAMPLE_STEP3_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(SIGNATURE, EXAMPLE_STEP3_USER));

        // First sub-tasks of EXTERNAL_SIGNATURE, filling the form

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXTERNAL_SIGNATURE, EXAMPLE_STEP4_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(EXTERNAL_SIGNATURE, EXAMPLE_STEP4_USER));

        // Second sub-task of EXTERNAL_SIGNATURE, the actual signature or bypass

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXTERNAL_SIGNATURE, EXAMPLE_STEP4_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP4_USER));

        // Finalize

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SECURE_MAIL, EXAMPLE_STEP5_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP5_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), ARCHIVE, EXAMPLE_FINAL_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(ARCHIVE, EXAMPLE_FINAL_USER));

        // History check

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.size());
        assertTrue(taskList.stream().map(Task::getDate).allMatch(Objects::nonNull));
        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, EXAMPLE_STEP3_USER, EXAMPLE_STEP4_USER, EXAMPLE_STEP5_USER, EXAMPLE_FINAL_USER),
                taskList.stream().map(Task::getAssignee).toList()
        );
    }


    @Test void performTask_secure_mail_directBypass() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP1_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP2_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SIGNATURE, EXAMPLE_STEP3_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(SIGNATURE, EXAMPLE_STEP3_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXTERNAL_SIGNATURE, EXAMPLE_STEP4_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP4_USER));

        // First sub-tasks of SECURE_MAIL

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(1, taskList.size());
        taskController.performTask(
                instance.getTaskList().get(0).getId(),
                new PerformTaskRequest(BYPASS, EXAMPLE_STEP5_USER)
        );

        // Finalize

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        taskController.performTask(taskList.get(0).getId(), new PerformTaskRequest(ARCHIVE, EXAMPLE_FINAL_USER));

        // History check

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(7, taskList.size());
        assertTrue(taskList.stream().map(Task::getDate).allMatch(Objects::nonNull));
        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, EXAMPLE_STEP3_USER, EXAMPLE_STEP4_USER, EXAMPLE_STEP5_USER, EXAMPLE_FINAL_USER),
                taskList.stream().map(Task::getAssignee).toList()
        );
    }


    @Test void performTask_secure_mail_fillingThenBypass() {
        Instance instance = instanceController.createInstance(EXAMPLE_INSTANCE_TO_START_SUPPLIER.get());

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        Task task = assertReadExistenceAndGetMainTask(instance.getTaskList(), START, EXAMPLE_ORIGIN_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(START, EXAMPLE_ORIGIN_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP1_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP1_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), VISA, EXAMPLE_STEP2_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(VISA, EXAMPLE_STEP2_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SIGNATURE, EXAMPLE_STEP3_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(SIGNATURE, EXAMPLE_STEP3_USER));
        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), EXTERNAL_SIGNATURE, EXAMPLE_STEP4_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP4_USER));

        // First sub-tasks of SECURE_MAIL, filling the form

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SECURE_MAIL, EXAMPLE_STEP5_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(SECURE_MAIL, EXAMPLE_STEP5_USER));

        // Second sub-task of EXTERNAL_SIGNATURE, the actual signature or bypass

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), SECURE_MAIL, EXAMPLE_STEP5_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(BYPASS, EXAMPLE_STEP5_USER));

        // Finalize

        instance = instanceController.getInstance(instance.getId(), false, EXAMPLE_TENANT_ID);
        task = assertReadExistenceAndGetMainTask(instance.getTaskList(), ARCHIVE, EXAMPLE_FINAL_DESK);
        taskController.performTask(task.getId(), new PerformTaskRequest(ARCHIVE, EXAMPLE_FINAL_USER));

        // History check

        instance = instanceController.getInstance(instance.getId(), true, EXAMPLE_TENANT_ID);
        List<Task> taskList = instance.getTaskList().stream().filter(TestUtils::isMainAction).toList();
        assertEquals(
                asList(START, EXAMPLE_STEP1_ACTION, EXAMPLE_STEP2_ACTION, EXAMPLE_STEP3_ACTION, EXAMPLE_STEP4_ACTION, EXAMPLE_STEP5_ACTION, ARCHIVE),
                taskList.stream().map(Task::getExpectedAction).toList()
        );
        assertEquals(
                asList(EXAMPLE_ORIGIN_USER, EXAMPLE_STEP1_USER, EXAMPLE_STEP2_USER, EXAMPLE_STEP3_USER, EXAMPLE_STEP4_USER, EXAMPLE_STEP5_USER, EXAMPLE_FINAL_USER),
                taskList.stream().map(Task::getAssignee).toList()
        );
        assertTrue(taskList.stream().map(Task::getDate).allMatch(Objects::nonNull));
    }


    @Test void performTask_unknownId() {
        try {
            taskController.performTask(
                    "unknown_id",
                    new PerformTaskRequest(VISA, EXAMPLE_STEP1_USER)
            );

            fail();

        } catch (ResponseStatusException e) {
            assertEquals(NOT_FOUND, e.getStatus());
        }
    }


    @Test void checkForbiddenInnerVariables_visaValidArgument() {
        TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                VISA,
                EXAMPLE_STEP1_USER,
                EXAMPLE_STEP1_DESK,
                null,
                Map.of("valid_key", "value")
        ));
    }


    @Test void checkForbiddenInnerVariables_visaInvalidArgument() {
        assertThrows(
                ResponseStatusException.class,
                () -> TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                        VISA,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(WORKFLOW_INTERNAL + "my_invalid_key", "value")
                ))
        );
    }


    @Test void checkForbiddenInnerVariables_chainValidArgument() {
        TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                CHAIN,
                EXAMPLE_STEP1_USER,
                EXAMPLE_STEP1_DESK,
                null,
                Map.of(
                        META_ORIGIN_GROUP_ID, UUID.randomUUID().toString(),
                        META_FINAL_GROUP_ID, UUID.randomUUID().toString(),
                        META_VALIDATION_WORKFLOW_ID, UUID.randomUUID().toString()
                )
        ));
    }


    @Test void checkForbiddenInnerVariables_chainInvalidArgument() {
        assertThrows(
                ResponseStatusException.class,
                () -> TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                        CHAIN,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(
                                META_ORIGIN_GROUP_ID, UUID.randomUUID().toString(),
                                META_FINAL_GROUP_ID, UUID.randomUUID().toString(),
                                META_VALIDATION_WORKFLOW_ID, UUID.randomUUID().toString(),
                                WORKFLOW_INTERNAL + "my_invalid_key", "value"
                        )
                ))
        );
    }


    @Test void checkForbiddenInnerVariables_startValidArgument() {
        TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                START,
                EXAMPLE_STEP1_USER,
                EXAMPLE_STEP1_DESK,
                null,
                Map.of(META_VALIDATION_WORKFLOW_ID, UUID.randomUUID().toString())
        ));
    }


    @Test void checkForbiddenInnerVariables_startInvalidArgument() {
        assertThrows(
                ResponseStatusException.class,
                () -> TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                        START,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(
                                META_VALIDATION_WORKFLOW_ID, UUID.randomUUID().toString(),
                                WORKFLOW_INTERNAL + "my_invalid_key", "value"
                        )
                ))
        );
    }


    @Test void checkForbiddenInnerVariables_secondOpinionValidArgument() {
        TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                ASK_SECOND_OPINION,
                EXAMPLE_STEP1_USER,
                EXAMPLE_STEP1_DESK,
                null,
                Map.of(META_CANDIDATE_DELEGATE_GROUPS, UUID.randomUUID().toString())
        ));
    }


    @Test void checkForbiddenInnerVariables_secondOpinionInvalidArgument() {
        assertThrows(
                ResponseStatusException.class,
                () -> TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                        ASK_SECOND_OPINION,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(
                                META_CANDIDATE_DELEGATE_GROUPS, UUID.randomUUID().toString(),
                                WORKFLOW_INTERNAL + "my_invalid_key", "value"
                        )
                ))
        );
    }


    @Test void checkForbiddenInnerVariables_transferValidArgument() {
        TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                TRANSFER,
                EXAMPLE_STEP1_USER,
                EXAMPLE_STEP1_DESK,
                null,
                Map.of(META_CANDIDATE_DELEGATE_GROUPS, UUID.randomUUID().toString())
        ));
    }


    @Test void checkForbiddenInnerVariables_transferInvalidArgument() {
        assertThrows(
                ResponseStatusException.class,
                () -> TaskController.checkForbiddenInnerVariables(new PerformTaskRequest(
                        TRANSFER,
                        EXAMPLE_STEP1_USER,
                        EXAMPLE_STEP1_DESK,
                        null,
                        Map.of(
                                META_CANDIDATE_DELEGATE_GROUPS, UUID.randomUUID().toString(),
                                WORKFLOW_INTERNAL + "my_invalid_key", "value"
                        )
                ))
        );
    }


}
