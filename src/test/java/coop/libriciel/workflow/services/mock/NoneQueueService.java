/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.services.mock;

import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.services.queue.MessageQueueInterface;
import coop.libriciel.workflow.utils.TextUtils;
import lombok.extern.log4j.Log4j2;
import org.flowable.engine.runtime.Execution;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.variable.api.delegate.VariableScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.*;

import static coop.libriciel.workflow.models.Instance.META_DUE_DATE;
import static coop.libriciel.workflow.models.Task.META_CURRENT_CANDIDATE_GROUPS;
import static coop.libriciel.workflow.models.Visibility.PUBLIC;
import static coop.libriciel.workflow.utils.CollectionUtils.popValue;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toMap;

@Log4j2
@Service(MessageQueueInterface.SERVICE_NAME)
@ConditionalOnProperty(name = MessageQueueInterface.PROVIDER_KEY, havingValue = "none")
public class NoneQueueService implements MessageQueueInterface {


    @Override
    public void triggerHook(@NotNull String expectedAction, @Nullable String performedAction, @NotNull Execution execution, @Nullable DelegateTask task) {

        Task.Action expected = Optional.of(expectedAction).map(String::toUpperCase).map(Task.Action::valueOf).orElse(null);
        Task.Action performed = Optional.ofNullable(performedAction).map(String::toUpperCase).map(Task.Action::valueOf).orElse(null);
        Map<String, String> parsedVariables = Optional.ofNullable(task)
                .map(VariableScope::getVariables)
                .orElse(emptyMap())
                .entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey, entry -> String.valueOf(entry.getValue())));
        List<String> candidateGroups = Optional.ofNullable(popValue(parsedVariables, META_CURRENT_CANDIDATE_GROUPS))
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());
        Date dueDate = Optional.ofNullable(popValue(parsedVariables, META_DUE_DATE))
                .map(TextUtils::fromIso8601String)
                .orElse(null);

        Task notifiedTask = new Task(
                null,
                null,
                execution.getRootProcessInstanceId(),
                execution.getName(),
                expected,
                performed,
                null,
                (performed == null) ? new Date() : null,
                dueDate,
                (performed != null) ? new Date() : null,
                candidateGroups,
                emptyList(),
                emptyList(),
                null,
                null,
                null,
                false,
                parsedVariables,
                null,
                null,
                null,
                1L,
                0L,
                PUBLIC
        );

        // log.debug("notifiedTask : " + notifiedTask);
    }


}
