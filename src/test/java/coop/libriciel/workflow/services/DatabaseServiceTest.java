/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.services;

import coop.libriciel.workflow.models.requests.FilteringParameter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static coop.libriciel.workflow.models.SortBy.INSTANCE_NAME;
import static coop.libriciel.workflow.models.State.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class DatabaseServiceTest {


    @Autowired private DatabaseService databaseService;


    @Test void buildHistoricTasksSqlRequest() {

        String expected =
                """
                SELECT
                 act_hi_procinst_root.proc_inst_id_ AS INSTANCE_ID,
                 act_hi_procinst_root.NAME_ AS INSTANCE_NAME,
                 act_hi_identitylink_current.GROUP_ID_ AS CANDIDATE_GROUP,
                 act_hi_procinst_current.ID_ AS TASK_ID,
                 act_hi_taskinst_current.NAME_ AS TASK_NAME,
                 act_hi_taskinst_current.DUE_DATE_ AS DUE_DATE,
                 act_hi_procinst_root.start_time_ AS BEGIN_DATE,
                 act_hi_procinst_root.end_time_ AS END_DATE,
                 act_hi_variable_t.text_ AS TYPE_ID,
                 act_hi_variable_st.text_ AS SUBTYPE_ID,
                 NULL AS VALIDATION_DATE,
                 array_agg(DISTINCT ARRAY[ act_hi_varinst.NAME_, act_hi_varinst.TEXT_ ]) AS VARIABLES,
                 array_agg(DISTINCT task_read.assignee_) AS READ_BY,
                 NULL AS SORT_BY
                FROM act_hi_identitylink AS act_hi_identitylink_orig
                JOIN act_hi_taskinst AS act_hi_taskinst_orig
                  ON (
                    act_hi_taskinst_orig.ID_ = act_hi_identitylink_orig.task_id_
                    AND act_hi_taskinst_orig.end_time_ IS NOT NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_orig
                  ON act_hi_procinst_orig.ID_ = act_hi_taskinst_orig.PROC_INST_ID_
                JOIN act_hi_procinst AS act_hi_procinst_root
                  ON (
                    act_hi_procinst_root.business_key_ = act_hi_procinst_orig.business_key_
                    AND act_hi_procinst_root.super_process_instance_id_ IS NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_current
                  ON act_hi_procinst_current.business_key_ = act_hi_procinst_root.business_key_
                JOIN act_hi_taskinst AS act_hi_taskinst_current
                  ON (
                    act_hi_procinst_current.id_ = act_hi_taskinst_current.proc_inst_id_
                    AND act_hi_taskinst_current.end_time_ IS NULL
                    AND act_hi_taskinst_current.name_ NOT IN ( 'undo', 'read' )
                  )
                JOIN act_hi_identitylink AS act_hi_identitylink_current
                  ON (
                    act_hi_taskinst_current.id_ = act_hi_identitylink_current.task_id_
                    AND act_hi_identitylink_current.type_ = 'candidate'
                  )
                LEFT OUTER JOIN act_hi_varinst AS act_hi_variable_t
                  ON (
                    act_hi_variable_t.proc_inst_id_ = act_hi_procinst_root.id_
                    AND act_hi_variable_t.name_ = 'i_Parapheur_internal_type_id'
                  )
                LEFT OUTER JOIN act_hi_varinst AS act_hi_variable_st
                  ON (
                    act_hi_variable_st.proc_inst_id_ = act_hi_procinst_root.id_
                    AND act_hi_variable_st.name_ = 'i_Parapheur_internal_subtype_id'
                  )
                LEFT OUTER JOIN act_hi_taskinst AS task_read
                  ON (
                    task_read.proc_inst_id_ = act_hi_taskinst_current.PROC_INST_ID_
                    AND task_read.name_ = 'read'
                    AND task_read.end_time_ IS NOT NULL
                  )
                LEFT OUTER JOIN act_hi_varinst
                  ON act_hi_varinst.proc_inst_id_ = act_hi_procinst_root.id_
                WHERE (
                  act_hi_identitylink_orig.group_id_ = 'DRH'
                  AND act_hi_identitylink_orig.type_ = 'candidate'
                )
                GROUP BY INSTANCE_ID, INSTANCE_NAME, CANDIDATE_GROUP, TASK_ID, TASK_NAME, DUE_DATE, BEGIN_DATE, SORT_BY, END_DATE, TYPE_ID, SUBTYPE_ID, VALIDATION_DATE
                ORDER BY SORT_BY ASC NULLS FIRST, BEGIN_DATE ASC
                OFFSET 100 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = databaseService
                .buildHistoricTasksSqlRequest(
                        null,
                        singletonList(new FilteringParameter("DRH", null, null, null)),
                        DOWNSTREAM,
                        INSTANCE_NAME.toString(), true,
                        2, 50,
                        null, null, null,
                        null, null, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildRuntimeTasksSqlRequest_draft() {

        String expected =
                """
                SELECT
                  act_ru_execution.ROOT_PROC_INST_ID_ AS INSTANCE_ID,
                  act_ru_exec_root.NAME_ AS INSTANCE_NAME,
                  act_ru_identitylink.GROUP_ID_ AS CANDIDATE_GROUP,
                  act_ru_task.ID_ AS TASK_ID,
                  act_ru_task.NAME_ AS TASK_NAME,
                  act_ru_task.DUE_DATE_ AS DUE_DATE,
                  act_ru_exec_root.START_TIME_ AS BEGIN_DATE,
                  NULL AS END_DATE,
                  act_ru_variable_t.text_ AS TYPE_ID,
                  act_ru_variable_st.text_ AS SUBTYPE_ID,
                  task_start.end_time_ AS VALIDATION_DATE,
                  array_agg(DISTINCT ARRAY[ ACT_RU_VARIABLE_MD.NAME_, ACT_RU_VARIABLE_MD.TEXT_ ]) AS VARIABLES,
                  array_agg(DISTINCT task_read.assignee_) AS READ_BY,
                  act_ru_exec_root.NAME_ AS SORT_BY
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                JOIN act_ru_execution AS act_ru_exec_root
                  ON act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLE_MD
                  ON act_ru_execution.ROOT_PROC_INST_ID_ = ACT_RU_VARIABLE_MD.PROC_INST_ID_
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_t
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_t.PROC_INST_ID_
                    AND act_ru_variable_t.name_ = 'i_Parapheur_internal_type_id'
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_st
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_st.PROC_INST_ID_
                    AND act_ru_variable_st.name_ = 'i_Parapheur_internal_subtype_id'
                  )
                LEFT OUTER JOIN act_hi_taskinst AS task_start
                  ON (
                    task_start.proc_inst_id_ = act_ru_task.PROC_INST_ID_
                    AND task_start.name_ = 'main_start'
                  )
                LEFT OUTER JOIN act_hi_taskinst AS task_read
                  ON (
                    task_read.proc_inst_id_ = act_ru_task.PROC_INST_ID_
                    AND task_read.name_ = 'read'
                    AND task_read.end_time_ IS NOT NULL
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_SB
                  ON (
                   act_ru_execution.ROOT_PROC_INST_ID_ =  ACT_RU_VARIABLES_SB.PROC_INST_ID_
                   AND TRUE
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_DB
                  ON act_ru_execution.ROOT_PROC_INST_ID_ = ACT_RU_VARIABLES_DB.PROC_INST_ID_
                WHERE (
                  act_ru_identitylink.TYPE_ = 'candidate'
                  AND act_ru_identitylink.GROUP_ID_ IN ('DRH')
                  AND ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                  AND TRUE
                  AND act_ru_task.NAME_ IN ( 'workflow_internal_draft', 'main_start' )
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND act_ru_task.NAME_ <> 'read'
                )
                GROUP BY INSTANCE_ID, INSTANCE_NAME, CANDIDATE_GROUP, TASK_ID, TASK_NAME, DUE_DATE, BEGIN_DATE, SORT_BY, TYPE_ID, SUBTYPE_ID, VALIDATION_DATE
                ORDER BY SORT_BY ASC NULLS FIRST, BEGIN_DATE ASC
                OFFSET 100 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = databaseService
                .buildRuntimeTasksSqlRequest(
                        null,
                        singletonList(new FilteringParameter("DRH", null, null, null)),
                        DRAFT,
                        INSTANCE_NAME.toString(), true,
                        2, 50,
                        null, null, null,
                        null, null, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildRuntimeTasksSqlRequest_pending() {

        String expected =
                """
                SELECT
                  act_ru_execution.ROOT_PROC_INST_ID_ AS INSTANCE_ID,
                  act_ru_exec_root.NAME_ AS INSTANCE_NAME,
                  act_ru_identitylink.GROUP_ID_ AS CANDIDATE_GROUP,
                  act_ru_task.ID_ AS TASK_ID,
                  act_ru_task.NAME_ AS TASK_NAME,
                  act_ru_task.DUE_DATE_ AS DUE_DATE,
                  act_ru_exec_root.START_TIME_ AS BEGIN_DATE,
                  NULL AS END_DATE,
                  act_ru_variable_t.text_ AS TYPE_ID,
                  act_ru_variable_st.text_ AS SUBTYPE_ID,
                  task_start.end_time_ AS VALIDATION_DATE,
                  array_agg(DISTINCT ARRAY[ ACT_RU_VARIABLE_MD.NAME_, ACT_RU_VARIABLE_MD.TEXT_ ]) AS VARIABLES,
                  array_agg(DISTINCT task_read.assignee_) AS READ_BY,
                  act_ru_exec_root.NAME_ AS SORT_BY
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                JOIN act_ru_execution AS act_ru_exec_root
                  ON act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLE_MD
                  ON act_ru_execution.ROOT_PROC_INST_ID_ = ACT_RU_VARIABLE_MD.PROC_INST_ID_
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_t
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_t.PROC_INST_ID_
                    AND act_ru_variable_t.name_ = 'i_Parapheur_internal_type_id'
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_st
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_st.PROC_INST_ID_
                    AND act_ru_variable_st.name_ = 'i_Parapheur_internal_subtype_id'
                  )
                LEFT OUTER JOIN act_hi_taskinst AS task_start
                  ON (
                    task_start.proc_inst_id_ = act_ru_task.PROC_INST_ID_
                    AND task_start.name_ = 'main_start'
                  )
                LEFT OUTER JOIN act_hi_taskinst AS task_read
                  ON (
                    task_read.proc_inst_id_ = act_ru_task.PROC_INST_ID_
                    AND task_read.name_ = 'read' AND task_read.end_time_ IS NOT NULL
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_SB
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ =  ACT_RU_VARIABLES_SB.PROC_INST_ID_
                    AND TRUE
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_DB
                  ON act_ru_execution.ROOT_PROC_INST_ID_ =  ACT_RU_VARIABLES_DB.PROC_INST_ID_
                WHERE ( act_ru_identitylink.TYPE_ = 'candidate'
                  AND (
                    ( act_ru_identitylink.GROUP_ID_ IN ( 'DRH', 'Maire' ) AND ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'CEO' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey01' AND ACT_RU_VARIABLES_DB.TEXT_ = '01' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'DGS' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey02' AND ACT_RU_VARIABLES_DB.TEXT_ = '02' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'DGS' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey03' AND ACT_RU_VARIABLES_DB.TEXT_ = '03' )
                  )
                  AND TRUE
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_draft', 'main_start' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_rejected', 'main_delete' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_finished', 'main_archive' )
                  AND act_ru_task.NAME_ <> 'undo'
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND act_ru_task.NAME_ <> 'read'
                )
                GROUP BY INSTANCE_ID, INSTANCE_NAME, CANDIDATE_GROUP, TASK_ID, TASK_NAME, DUE_DATE, BEGIN_DATE, SORT_BY, TYPE_ID, SUBTYPE_ID, VALIDATION_DATE
                ORDER BY SORT_BY ASC NULLS FIRST, BEGIN_DATE ASC
                OFFSET 100 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = databaseService
                .buildRuntimeTasksSqlRequest(
                        null,
                        asList(
                                new FilteringParameter("DRH", null, null, null),
                                new FilteringParameter("Maire", null, null, null),
                                new FilteringParameter("CEO", null, "metadataKey01", "01"),
                                new FilteringParameter("DGS", null, "metadataKey02", "02"),
                                new FilteringParameter("DGS", null, "metadataKey03", "03")
                        ),
                        PENDING, INSTANCE_NAME.toString(),
                        true, 2, 50,
                        null, null, null,
                        null, null, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void countHistoricTasksSqlRequest() {

        String expected =
                """
                SELECT
                  count(DISTINCT act_hi_procinst_root.proc_inst_id_)
                FROM act_hi_identitylink AS act_hi_identitylink_orig
                JOIN act_hi_taskinst AS act_hi_taskinst_orig
                  ON (
                    act_hi_taskinst_orig.ID_ = act_hi_identitylink_orig.task_id_
                    AND act_hi_taskinst_orig.end_time_ IS NOT NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_orig
                  ON act_hi_procinst_orig.ID_ = act_hi_taskinst_orig.PROC_INST_ID_
                JOIN act_hi_procinst AS act_hi_procinst_root
                  ON (
                    act_hi_procinst_root.business_key_ = act_hi_procinst_orig.business_key_
                    AND act_hi_procinst_root.super_process_instance_id_ IS NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_current
                  ON act_hi_procinst_current.business_key_ = act_hi_procinst_root.business_key_
                JOIN act_hi_taskinst AS act_hi_taskinst_current
                  ON (
                    act_hi_procinst_current.id_ = act_hi_taskinst_current.proc_inst_id_
                    AND act_hi_taskinst_current.end_time_ IS NULL AND act_hi_taskinst_current.name_ NOT IN ( 'undo', 'read' )
                  )
                WHERE (
                  act_hi_identitylink_orig.group_id_ = 'DRH'
                  AND act_hi_identitylink_orig.type_ = 'candidate'
                )
                """.indent(1);

        String result = databaseService
                .countHistoricTasksSqlRequest(
                        null,
                        asList(new FilteringParameter("DRH", null, null, null)),
                        PENDING, null, null, null,
                        null, null, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void countRuntimeTasksSqlRequest() {

        String expected =
                """
                SELECT
                 count(DISTINCT act_ru_execution.ID_)
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                JOIN act_ru_execution AS act_ru_exec_root
                  ON act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_t
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_t.PROC_INST_ID_
                    AND act_ru_variable_t.name_ = 'i_Parapheur_internal_type_id'
                  )
                LEFT OUTER JOIN ACT_RU_VARIABLE AS act_ru_variable_st
                  ON (
                    act_ru_execution.ROOT_PROC_INST_ID_ = act_ru_variable_st.PROC_INST_ID_
                    AND act_ru_variable_st.name_ = 'i_Parapheur_internal_subtype_id'
                  )
                WHERE (
                  act_ru_identitylink.TYPE_ = 'candidate'
                  AND (
                    ( act_ru_identitylink.GROUP_ID_ IN ( 'DRH', 'Maire' ) AND ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'CEO' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey01' AND ACT_RU_VARIABLES_DB.TEXT_ = '01' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'DGS' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey02' AND ACT_RU_VARIABLES_DB.TEXT_ = '02' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'DGS' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadataKey03' AND ACT_RU_VARIABLES_DB.TEXT_ = '03' )
                  )
                  AND TRUE
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_draft', 'main_start' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_rejected', 'main_delete' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_finished', 'main_archive' )
                  AND act_ru_task.NAME_ <> 'undo'
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND TRUE
                  AND act_ru_task.NAME_ <> 'read'
                )
                """.indent(1);

        String result = databaseService
                .countRuntimeTasksSqlRequest(
                        null,
                        asList(
                                new FilteringParameter("DRH", null, null, null),
                                new FilteringParameter("Maire", null, null, null),
                                new FilteringParameter("CEO", null, "metadataKey01", "01"),
                                new FilteringParameter("DGS", null, "metadataKey02", "02"),
                                new FilteringParameter("DGS", null, "metadataKey03", "03")),
                        PENDING, null, null, null,
                        null, null, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildCountHistoricFolders() {

        String expected =
                """
                SELECT
                  'DRH' AS CANDIDATE_GROUP,
                  'downstream' AS STATE,
                  NULL AS FILTER_BY_METADATA_KEY,
                  NULL AS FILTER_BY_METADATA_VALUE,
                  count(DISTINCT act_hi_procinst_root.proc_inst_id_) AS INSTANCE_COUNT
                FROM act_hi_identitylink AS act_hi_identitylink_orig
                JOIN act_hi_taskinst AS act_hi_taskinst_orig
                  ON (
                    act_hi_taskinst_orig.ID_ = act_hi_identitylink_orig.task_id_
                    AND act_hi_taskinst_orig.end_time_ IS NOT NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_orig
                  ON act_hi_procinst_orig.ID_ = act_hi_taskinst_orig.PROC_INST_ID_
                JOIN act_hi_procinst AS act_hi_procinst_root
                  ON (
                    act_hi_procinst_root.business_key_ = act_hi_procinst_orig.business_key_
                    AND act_hi_procinst_root.super_process_instance_id_ IS NULL
                  )
                JOIN act_hi_procinst AS act_hi_procinst_current
                  ON act_hi_procinst_current.business_key_ = act_hi_procinst_root.business_key_
                JOIN act_hi_taskinst AS act_hi_taskinst_current
                  ON (
                    act_hi_procinst_current.id_ = act_hi_taskinst_current.proc_inst_id_
                    AND act_hi_taskinst_current.end_time_ IS NULL
                    AND act_hi_taskinst_current.name_ NOT IN ( 'undo', 'read' )
                  )
                WHERE (
                  act_hi_identitylink_orig.group_id_ = 'DRH'
                  AND act_hi_identitylink_orig.type_ = 'candidate'
                )
                """.indent(1);

        String result =
                databaseService
                        .buildCountHistoricFolders(
                                List.of(new FilteringParameter("DRH", null, null, null))
                        )
                        .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildCountRuntimeFolders() {

        String expected =
                """
                SELECT
                  act_ru_identitylink.GROUP_ID_ AS CANDIDATE_GROUP,
                  CASE
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_draft', 'main_start' ) THEN 'draft'
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_rejected', 'main_delete' ) THEN 'rejected'
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_finished', 'main_archive' ) THEN 'finished'
                    ELSE 'pending'
                  END AS STATE,
                  CASE WHEN ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                    THEN NULL
                    ELSE ACT_RU_VARIABLES_DB.NAME_
                  END AS FILTER_BY_METADATA_KEY,
                  CASE WHEN ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                    THEN NULL
                    ELSE ACT_RU_VARIABLES_DB.TEXT_
                  END AS FILTER_BY_METADATA_VALUE,
                  count(DISTINCT act_ru_execution.ID_) AS INSTANCE_COUNT
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_DB
                  ON act_ru_execution.ROOT_PROC_INST_ID_ = ACT_RU_VARIABLES_DB.PROC_INST_ID_
                WHERE ( act_ru_identitylink.TYPE_ = 'candidate'
                  AND (
                    ( act_ru_identitylink.GROUP_ID_ IN ( 'CEO', 'DRH' ) AND ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id' )
                    OR ( ACT_RU_VARIABLES_DB.NAME_ = 'metadata02' AND ACT_RU_VARIABLES_DB.TEXT_ = 'value 02' )
                    OR ( act_ru_identitylink.GROUP_ID_ = 'DGS' AND ACT_RU_VARIABLES_DB.NAME_ = 'metadata01' AND ACT_RU_VARIABLES_DB.TEXT_ = 'value 01' )
                  )
                  AND act_ru_task.NAME_ <> 'undo'
                  AND act_ru_task.NAME_ <> 'read'
                )
                GROUP BY CANDIDATE_GROUP, STATE, FILTER_BY_METADATA_KEY, FILTER_BY_METADATA_VALUE
                """.indent(1);

        String result =
                databaseService
                        .buildCountRuntimeFolders(
                                asList(
                                        new FilteringParameter("DRH", null, null, null),
                                        new FilteringParameter("CEO", null, null, null),
                                        new FilteringParameter("DGS", null, "metadata01", "value 01"),
                                        new FilteringParameter(null, null, "metadata02", "value 02")
                                ))
                        .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildTotalTaskRequest() {

        String expected =
                """
                SELECT
                  act_ru_identitylink.GROUP_ID_ AS CANDIDATE_GROUP,
                  CASE
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_draft', 'main_start' ) THEN 'draft'
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_rejected', 'main_delete' ) THEN 'rejected'
                    WHEN act_ru_task.NAME_ IN ( 'workflow_internal_finished', 'main_archive' ) THEN 'finished'
                  ELSE 'pending' END AS STATE,
                  CASE WHEN ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                    THEN NULL
                    ELSE ACT_RU_VARIABLES_DB.NAME_
                  END AS FILTER_BY_METADATA_KEY,
                  CASE WHEN ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                    THEN NULL
                    ELSE ACT_RU_VARIABLES_DB.TEXT_
                  END AS FILTER_BY_METADATA_VALUE,
                  count(DISTINCT act_ru_execution.ID_) AS INSTANCE_COUNT
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                LEFT OUTER JOIN ACT_RU_VARIABLE AS ACT_RU_VARIABLES_DB
                  ON act_ru_execution.ROOT_PROC_INST_ID_ = ACT_RU_VARIABLES_DB.PROC_INST_ID_
                WHERE ( act_ru_identitylink.TYPE_ = 'candidate'
                  AND act_ru_identitylink.GROUP_ID_ IN ( 'Compta', 'DRH' )
                  AND ACT_RU_VARIABLES_DB.NAME_ = 'workflow_internal_origin_group_id'
                  AND act_ru_task.NAME_ <> 'undo'
                  AND act_ru_task.NAME_ <> 'read'
                )
                GROUP BY CANDIDATE_GROUP, STATE, FILTER_BY_METADATA_KEY, FILTER_BY_METADATA_VALUE
                UNION
                SELECT
                  act_ru_identitylink.GROUP_ID_ AS CANDIDATE_GROUP,
                  'late' AS STATE,
                  NULL AS FILTER_BY_METADATA_KEY,
                  NULL AS FILTER_BY_METADATA_VALUE,
                  count(act_ru_execution.ID_) AS INSTANCE_COUNT
                FROM act_ru_identitylink
                JOIN act_ru_task
                  ON act_ru_identitylink.TASK_ID_ = act_ru_task.ID_
                JOIN act_ru_execution
                  ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                WHERE ( act_ru_identitylink.TYPE_ = 'candidate'
                  AND act_ru_identitylink.GROUP_ID_ IN ( 'Compta', 'DRH' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_draft', 'main_start' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_rejected', 'main_delete' )
                  AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_finished', 'main_archive' )
                  AND act_ru_task.NAME_ <> 'undo'
                  AND act_ru_task.NAME_ <> 'read'
                  AND act_ru_task.DUE_DATE_ IS NOT NULL
                  AND act_ru_task.DUE_DATE_ < CURRENT_TIMESTAMP
                )
                GROUP BY CANDIDATE_GROUP, STATE, FILTER_BY_METADATA_KEY, FILTER_BY_METADATA_VALUE
                """.indent(1);

        List<FilteringParameter> filteringParams = asList(
                new FilteringParameter("Compta", null, null, null),
                new FilteringParameter("DRH", null, null, null)
        );

        String result =
                databaseService.buildCountRuntimeFolders(filteringParams)
                        .union(databaseService.buildCountLateFolders(asList("Compta", "DRH")))
                        .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test void buildCountMetadataUsage() {

        String expected =
                """
                SELECT
                    NULL AS CANDIDATE_GROUP,
                    NULL AS STATE,
                    act_hi_varinst.NAME_ AS FILTER_BY_METADATA_KEY,
                    act_hi_varinst.TEXT_ AS FILTER_BY_METADATA_VALUE,
                    count(act_hi_procinst.ID_) AS INSTANCE_COUNT
                FROM act_hi_procinst
                JOIN act_hi_varinst
                  ON act_hi_procinst.PROC_INST_ID_ = act_hi_varinst.PROC_INST_ID_
                WHERE ( act_hi_procinst.SUPER_PROCESS_INSTANCE_ID_ IS NULL
                  AND ( (  act_hi_varinst.NAME_ = 'metadata01' AND act_hi_varinst.TEXT_ = 'test01' )
                   OR ( act_hi_varinst.NAME_ = 'metadata02' AND act_hi_varinst.TEXT_ = 'test02' )
                  ) )
                GROUP BY FILTER_BY_METADATA_KEY, FILTER_BY_METADATA_VALUE
                """.indent(1);

        String result = databaseService
                .buildCountMetadataUsage(
                        asList(
                                new FilteringParameter(null, null, "metadata01", "test01"),
                                new FilteringParameter(null, null, "metadata02", "test02")
                        ))
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }

}
