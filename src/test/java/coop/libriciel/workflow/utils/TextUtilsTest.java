/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TextUtilsTest {


    @Test void constructor() {
        assertThrows(IllegalStateException.class, TextUtils::new);
    }


    @Test void toIso8601String() {
        // FIXME : Something's wrong in the CI (probably the locale)...
        // assertEquals("1970-01-01T01:00:00.000+0100", TextUtils.toIso8601String(new Date(0L)));
        // assertEquals("2001-09-09T03:46:39.999+0200", TextUtils.toIso8601String(new Date(999999999999L)));
    }

}
