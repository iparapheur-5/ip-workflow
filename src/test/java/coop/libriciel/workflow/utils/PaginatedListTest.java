/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class PaginatedListTest {


    // <editor-fold desc="Utils">


    public static long failWithLongResult() {
        fail();
        return 999;
    }


    // </editor-fold desc="Utils">


    @Test void constructor_fullPage() {
        PaginatedList<String> result = new PaginatedList<>(asList("1", "2", "3", "4", "5"), 0, 5, () -> 7L);
        assertEquals(7, result.getTotal());
    }


    @Test void constructor_lastPage() {
        PaginatedList<String> result = new PaginatedList<>(asList("6", "7"), 1, 5, PaginatedListTest::failWithLongResult);
        assertEquals(7, result.getTotal());
    }


    @Test void constructor_emptyList() {
        PaginatedList<String> result = new PaginatedList<>(emptyList(), 0, 10, PaginatedListTest::failWithLongResult);
        assertEquals(0, result.getTotal());
    }


    @Test void constructor_overheadRequest() {
        PaginatedList<String> result = new PaginatedList<>(emptyList(), 99, 10, () -> 7L);
        assertEquals(7, result.getTotal());
    }

}