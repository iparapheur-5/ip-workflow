/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import coop.libriciel.workflow.models.Task;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static coop.libriciel.workflow.models.Task.Action.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtilsTest {


    @Test void parseMetadata() {

        String[][] dataTest = {{"testKey1", "testValue1"}, {"testKey2", "testValue2"}};
        Map<String, String> parsed = CollectionUtils.parseMetadata(dataTest);

        assertEquals("testValue1", parsed.get("testKey1"));
        assertEquals("testValue2", parsed.get("testKey2"));
        assertNull(parsed.get("testKey99"));

        assertNotNull(CollectionUtils.parseMetadata(null));
    }


    // <editor-fold desc="mergeSteps">


    @Test void mergeSteps_visaStep_pending() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").expectedAction(VISA).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_done() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").expectedAction(VISA).performedAction(VISA).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_upcoming() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("def00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_paperSignature() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").date(new Date(0L)).expectedAction(SIGNATURE).performedAction(PAPER_SIGNATURE).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_secondOpinion_pending() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").date(new Date(0L)).expectedAction(VISA).performedAction(SECOND_OPINION).build()));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id01").expectedAction(SECOND_OPINION).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(asList("id00", "id01", "def00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_secondOpinion_done() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).performedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), asList(
                Task.builder().id("id00").date(new Date(0L)).expectedAction(VISA).performedAction(SECOND_OPINION).build(),
                Task.builder().id("id01").date(new Date(1L)).expectedAction(SECOND_OPINION).performedAction(SECOND_OPINION).build(),
                Task.builder().id("id02").date(new Date(2L)).expectedAction(VISA).performedAction(VISA).build()
        ));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(asList("id00", "id01", "id02"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_transfer_pending() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").date(new Date(0L)).expectedAction(VISA).performedAction(TRANSFER).build()));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id01").expectedAction(VISA).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(asList("id00", "id01"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_transfer_done() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));

        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), asList(
                Task.builder().id("id00").date(new Date(0L)).expectedAction(VISA).performedAction(TRANSFER).build(),
                Task.builder().id("id01").date(new Date(1L)).expectedAction(VISA).performedAction(VISA).build()
        ));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(asList("id00", "id01"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_undone() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id99").date(new Date(0L)).expectedAction(UNDO).performedAction(UNDO).build()));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("def00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_afterUndo() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("d000").candidateGroups(singletonList("group00")).expectedAction(VISA).build()));
        definitionTasksMap.put(Pair.of(0L, 1L), singletonList(Task.builder().id("d001").candidateGroups(singletonList("group01")).expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("h000").candidateGroups(singletonList("group00")).date(new Date(0L)).expectedAction(VISA).performedAction(VISA).build()));
        historyTasksMap.put(Pair.of(0L, 1L), singletonList(Task.builder().id("h010").candidateGroups(singletonList("group00")).date(new Date(1L)).expectedAction(UNDO).performedAction(UNDO).build()));
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("h001").candidateGroups(singletonList("group00")).date(new Date(2L)).expectedAction(VISA).performedAction(VISA).build()));
        historyTasksMap.put(Pair.of(0L, 1L), singletonList(Task.builder().id("h011").candidateGroups(singletonList("group00")).date(new Date(3L)).expectedAction(UNDO).performedAction(UNDO).build()));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("p000").candidateGroups(singletonList("group00")).expectedAction(VISA).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(List.of("p000", "d001"), mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_visaStep_validatedAfterUndo() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(VISA).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), asList(
                Task.builder().id("id00").date(new Date(0L)).expectedAction(VISA).performedAction(UNDO).build(),
                Task.builder().id("id00").date(new Date(1L)).expectedAction(VISA).performedAction(VISA).build()
        ));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_externalSignatureStep_pending() {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(EXTERNAL_SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").expectedAction(EXTERNAL_SIGNATURE).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_externalSignatureStep_waiting() {
        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(EXTERNAL_SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id00").date(new Date(0L)).expectedAction(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();
        pendingTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("id01").expectedAction(EXTERNAL_SIGNATURE).build()));

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id01"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    @Test void mergeSteps_externalSignatureStep_done() {
        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(EXTERNAL_SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        historyTasksMap.put(Pair.of(0L, 0L), asList(
                Task.builder().id("id00").date(new Date(0L)).expectedAction(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                Task.builder().id("id01").date(new Date(1L)).expectedAction(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build()
        ));
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("id01"),
                mergedTasks.stream().map(Task::getId).toList());

        assertEquals(1, mergedTasks.size());
    }


    @Test void mergeSteps_externalSignatureStep_upcoming() {
        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        definitionTasksMap.put(Pair.of(0L, 0L), singletonList(Task.builder().id("def00").expectedAction(EXTERNAL_SIGNATURE).build()));
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        List<Task> mergedTasks = CollectionUtils.mergeTaskLists(
                definitionTasksMap,
                historyTasksMap,
                pendingTasksMap
        );

        // Tests

        assertNotNull(mergedTasks);
        assertEquals(singletonList("def00"),
                mergedTasks.stream().map(Task::getId).toList());
    }


    // </editor-fold desc="mergeSteps">


}
