/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.bpmn;

import coop.libriciel.workflow.TestUtils;
import org.apache.commons.io.IOUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.workflow.TestUtils.assertAndGetSingleUndoTask;
import static coop.libriciel.workflow.TestUtils.assertReadExistenceAndGetMainTask;
import static coop.libriciel.workflow.models.Instance.META_DUE_DATE;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.Task.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Locale.ROOT;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class ExternalActionTest {


    @Autowired private RepositoryService repositoryService;
    @Autowired private RuntimeService runtimeService;
    @Autowired private TaskService taskService;


    @BeforeEach void setup() {
        TestUtils.cleanup(taskService, runtimeService, repositoryService);

        try (InputStream actionTemplateInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/external_action.bpmn20.xml")) {

            InputStream safeInputStream = Optional.ofNullable(actionTemplateInputStream)
                    .orElseThrow(() -> new IOException("Missing internal external_action file"));

            String bpmn = IOUtils.toString(safeInputStream, UTF_8);
            // A StringSubstitutor would have been nicer, but it fails to catch the CDATA content.
            // A regular replaceAll will do...
            bpmn = bpmn.replaceAll("\\$\\{external_action}", EXTERNAL_SIGNATURE.toString().toLowerCase(ROOT));

            repositoryService
                    .createDeployment()
                    .addString("external_signature.bpmn20.xml", bpmn)
                    .deploy();

        } catch (IOException exception) {
            throw new RuntimeException("Error patching external_action reference BPMN", exception);
        }
    }


    @AfterEach void cleanup() {
        TestUtils.cleanup(taskService, runtimeService, repositoryService);
    }


    @Test void simple() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));

        previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertEquals(0, previousUserTasks.size());

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_waiting");

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void simple_withoutUndo() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, SIGNATURE.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertEquals(0, previousUserTasks.size());

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");
        assertNull(currentUserTasks.get(0).getDueDate());

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_waiting");

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void reject_1stStep() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");

        try {
            taskService.complete(currentTask.getId(), Map.of("action", REJECT.getFlowableString()));
            fail();
        } catch (FlowableException e) {
            // We're checking that the "reject" signal has been thrown.
            // But in this test case, no one was here to catch it.
            assertEquals("No catching boundary event found for error with errorCode 'reject', neither in same process nor in parent process", e.getMessage());
        }
    }


    @Test void reject_2cdStep() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));

        previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertEquals(0, previousUserTasks.size());

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_waiting");

        try {
            taskService.complete(currentTask.getId(), Map.of("action", REJECT.getFlowableString()));
            fail();
        } catch (FlowableException e) {
            // We're checking that the "reject" signal has been thrown.
            // But in this test case, no one was here to catch it.
            assertEquals("No catching boundary event found for error with errorCode 'reject', neither in same process nor in parent process", e.getMessage());
        }
    }


    @Test void bypass_1rstStep() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");

        taskService.complete(currentTask.getId(), Map.of("action", BYPASS.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void bypass_2cdStep() {

        runtimeService.startProcessInstanceByKey(
                EXTERNAL_SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                )
        );

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_fillingform");

        taskService.complete(currentTask.getId(), Map.of("action", EXTERNAL_SIGNATURE.getFlowableString()));

        previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertEquals(0, previousUserTasks.size());

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "external_signature_waiting");

        taskService.complete(currentTask.getId(), Map.of("action", BYPASS.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


}
