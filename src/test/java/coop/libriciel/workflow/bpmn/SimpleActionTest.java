/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.bpmn;

import coop.libriciel.workflow.controller.DefinitionController;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;

import static coop.libriciel.workflow.TestUtils.assertAndGetSingleUndoTask;
import static coop.libriciel.workflow.TestUtils.assertReadExistenceAndGetMainTask;
import static coop.libriciel.workflow.models.Instance.META_DUE_DATE;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.Task.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class SimpleActionTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private RepositoryService repositoryService;
    @Autowired private RuntimeService runtimeService;
    @Autowired private TaskService taskService;


    @BeforeEach void setup() {
        this.cleanup();

        definitionController.setupIParapheurDefinitions();
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @AfterEach void cleanup() {

        repositoryService.createDeploymentQuery()
                .list()
                .forEach(deployment -> repositoryService.deleteDeployment(deployment.getId(), true));
    }


    @Test void simple() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                ));

        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertAndGetSingleUndoTask(previousUserTasks);

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        assertEquals(230536800000L, currentTask.getDueDate().getTime());
        taskService.complete(currentTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));

        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void simple_withoutUndo() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, SIGNATURE.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        List<Task> previousUserTasks = taskService.createTaskQuery().taskCandidateGroup("previous_user").list();
        assertEquals(0, previousUserTasks.size());
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(currentTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void reject() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        try {
            taskService.complete(currentTask.getId(), Map.of("action", REJECT.getFlowableString()));
            fail();
        } catch (FlowableException e) {
            // We're checking that the "reject" signal has been thrown.
            // But in this test case, no one was here to catch it.
            assertEquals("No matching parent execution for error code reject found", e.getMessage());
        }
    }


    @Test void secondOpinion() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(
                currentTask.getId(),
                Map.of(
                        "action", ASK_SECOND_OPINION.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "delegate_user"
                ));

        List<Task> delegateUserTasks = taskService.createTaskQuery().taskCandidateGroup("delegate_user").list();
        Task secondedTask = assertReadExistenceAndGetMainTask(delegateUserTasks, "main_second_opinion");

        taskService.complete(secondedTask.getId(), Map.of("action", SECOND_OPINION.getFlowableString()));

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(currentTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void secondOpinion_undo() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(
                currentTask.getId(),
                Map.of(
                        "action", ASK_SECOND_OPINION.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "delegate_user"
                ));

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task undoTask = assertAndGetSingleUndoTask(currentUserTasks);

        taskService.complete(undoTask.getId());

        List<Task> delegateUserTasks = taskService.createTaskQuery().taskCandidateGroup("delegate_user").list();
        assertEquals(0, delegateUserTasks.size());

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(currentTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void transfer() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456",
                        META_DUE_DATE, "1977-04-22T01:00:00-05:00"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(
                currentTask.getId(),
                Map.of(
                        "action", TRANSFER.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "delegate_user"));

        List<Task> delegateUserTasks = taskService.createTaskQuery().taskCandidateGroup("delegate_user").list();
        Task delegatedTask = assertReadExistenceAndGetMainTask(delegateUserTasks, "main_signature");

        taskService.complete(delegatedTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void transfer_undo() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(
                currentTask.getId(),
                Map.of(
                        "action", TRANSFER.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "delegate_user"
                ));

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task undoTask = assertAndGetSingleUndoTask(currentUserTasks);
        assertNull(undoTask.getDueDate());

        taskService.complete(undoTask.getId());

        currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(currentTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


    @Test void transfer_double() {

        runtimeService.startProcessInstanceByKey(
                SIGNATURE.getFlowableString(),
                Map.of(
                        META_CURRENT_CANDIDATE_GROUPS, "current_user",
                        META_PREVIOUS_CANDIDATE_GROUPS, "previous_user",
                        META_PREVIOUS_ACTION, VISA.getFlowableString(),
                        "folder_id", "dossier_id_123456"
                ));

        List<Task> currentUserTasks = taskService.createTaskQuery().taskCandidateGroup("current_user").list();
        Task currentTask = assertReadExistenceAndGetMainTask(currentUserTasks, "main_signature");

        taskService.complete(
                currentTask.getId(),
                Map.of(
                        "action", TRANSFER.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "delegate_user"
                ));

        List<Task> delegateUserTasks = taskService.createTaskQuery().taskCandidateGroup("delegate_user").list();
        Task delegatedTask = assertReadExistenceAndGetMainTask(delegateUserTasks, "main_signature");

        taskService.complete(
                delegatedTask.getId(),
                Map.of(
                        "action", TRANSFER.getFlowableString(),
                        "workflow_internal_delegate_candidate_groups", "second_delegate_user"
                ));

        List<Task> secondDelegateUserTasks = taskService.createTaskQuery().taskCandidateGroup("second_delegate_user").list();
        Task secondDelegatedTask = assertReadExistenceAndGetMainTask(secondDelegateUserTasks, "main_signature");

        taskService.complete(secondDelegatedTask.getId(), Map.of("action", SIGNATURE.getFlowableString()));
        assertEquals(0, taskService.createTaskQuery().list().size());
    }


}
