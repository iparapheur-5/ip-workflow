/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.bpmn;

import coop.libriciel.workflow.controller.DefinitionController;
import coop.libriciel.workflow.models.WorkflowDefinition;
import org.apache.commons.lang3.StringUtils;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;

import static coop.libriciel.workflow.TestUtils.EXAMPLE_TENANT_ID;
import static coop.libriciel.workflow.TestUtils.assertReadExistenceAndGetMainTask;
import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.WorkflowDefinition.SortBy.NAME;
import static java.lang.Integer.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class FolderTest {


    @Autowired private DefinitionController definitionController;
    @Autowired private RepositoryService repositoryService;
    @Autowired private RuntimeService runtimeService;
    @Autowired private TaskService taskService;

    private static WorkflowDefinition folderDefinition;


    private void performVisaSignatureWorkflow(String firstCandidate, String secondCandidateGroup) {

        assertEquals(2, taskService.createTaskQuery().list().size());
        assertEquals(1, taskService.createTaskQuery().taskName("read").list().size());
        List<Task> drhTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup(firstCandidate).list();
        Task drhVisa = assertReadExistenceAndGetMainTask(drhTasks, "main_visa");
        assertEquals(230536800000L, drhVisa.getDueDate().getTime());

        taskService.complete(drhVisa.getId(), Map.of("action", VISA.getFlowableString()));

        assertEquals(3, taskService.createTaskQuery().list().size());
        assertEquals(1, taskService.createTaskQuery().taskName("read").list().size());
        assertEquals(1, taskService.createTaskQuery().taskName("undo").list().size());
        List<Task> presidentTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup(secondCandidateGroup).list();
        Task presidentSignature = assertReadExistenceAndGetMainTask(presidentTasks, "main_signature");
        assertEquals(230536800000L, presidentSignature.getDueDate().getTime());

        taskService.complete(presidentSignature.getId(), Map.of("action", SIGNATURE.getFlowableString()));
    }


    private void performSimpleWorkflow() {
        performVisaSignatureWorkflow("DRH", "Président");
    }


    private void performAlternateWorkflow() {
        performVisaSignatureWorkflow("Adjoint DRH", "DRH");
    }


    @BeforeEach void setup() {
        this.cleanup();

        definitionController.setupIParapheurDefinitions();

        repositoryService
                .createDeployment()
                .addClasspathResource("bpmn/flowableModeler_simple_workflow_patched.bpmn20.xml")
                .tenantId(EXAMPLE_TENANT_ID)
                .key("simple_workflow")
                .deploy();

        repositoryService
                .createDeployment()
                .addClasspathResource("bpmn/flowableModeler_alternate_workflow_patched.bpmn20.xml")
                .tenantId(EXAMPLE_TENANT_ID)
                .key("alternate_workflow")
                .deploy();

        folderDefinition = definitionController
                .getWorkflowDefinitions(0, MAX_VALUE, null, NAME, true, null)
                .getData()
                .stream()
                .filter(p -> StringUtils.equals(p.getKey(), FOLDER_DEPLOYMENT_KEY))
                .findFirst()
                .orElseGet(Assertions::fail);
    }


    @AfterEach void cleanup() {

        repositoryService.createDeploymentQuery()
                .list()
                .forEach(deployment -> repositoryService.deleteDeployment(deployment.getId(), true));
    }


    @Test void validation() {

        runtimeService.createProcessInstanceBuilder()
                .processDefinitionId(folderDefinition.getId())
                .name("name_01")
                .overrideProcessDefinitionTenantId(EXAMPLE_TENANT_ID)
                .businessKey("business_key_01")
                .variable(META_ORIGIN_GROUP_ID, "originDesk")
                .variable(META_FINAL_GROUP_ID, "finalDesk")
                .variable(META_CREATION_WORKFLOW_ID, null)
                .variable(META_VALIDATION_WORKFLOW_ID, "simple_workflow")
                .variable(META_DUE_DATE, "1977-04-22T01:00:00-05:00")
                .start();

        List<Task> startTasks = taskService.createTaskQuery().includeProcessVariables().includeTaskLocalVariables().taskCandidateGroup("originDesk").list();
        Task startTask = assertReadExistenceAndGetMainTask(startTasks, "main_start");
        assertEquals(230536800000L, startTask.getDueDate().getTime());

        assertNull(startTask.getProcessVariables().get(META_CREATION_WORKFLOW_ID));
        assertEquals("simple_workflow", startTask.getProcessVariables().get(META_VALIDATION_WORKFLOW_ID));

        taskService.complete(startTask.getId(), Map.of("action", START.getFlowableString()));

        performSimpleWorkflow();

        List<Task> finalTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup("finalDesk").list();
        Task archiveTask = assertReadExistenceAndGetMainTask(finalTasks, "main_archive");

        assertNull(archiveTask.getProcessVariables().get(META_CREATION_WORKFLOW_ID));
        assertEquals("simple_workflow", finalTasks.get(0).getProcessVariables().get(META_VALIDATION_WORKFLOW_ID));
    }


    @Test void creation_validation() {

        runtimeService.createProcessInstanceBuilder()
                .processDefinitionId(folderDefinition.getId())
                .name("name_01")
                .overrideProcessDefinitionTenantId(EXAMPLE_TENANT_ID)
                .businessKey("business_key_01")
                .variable(META_ORIGIN_GROUP_ID, "originDesk")
                .variable(META_FINAL_GROUP_ID, "finalDesk")
                .variable(META_CREATION_WORKFLOW_ID, "simple_workflow")
                .variable(META_VALIDATION_WORKFLOW_ID, "simple_workflow")
                .variable(META_DUE_DATE, "1977-04-22T01:00:00-05:00")
                .start();

        List<Task> startTasks = taskService.createTaskQuery().includeProcessVariables().includeTaskLocalVariables().taskCandidateGroup("originDesk").list();
        Task startTask = assertReadExistenceAndGetMainTask(startTasks, "main_start");

        assertEquals("originDesk", startTask.getProcessVariables().get(META_ORIGIN_GROUP_ID));
        assertEquals("finalDesk", startTask.getProcessVariables().get(META_FINAL_GROUP_ID));
        assertEquals("simple_workflow", startTask.getProcessVariables().get(META_CREATION_WORKFLOW_ID));
        assertEquals("simple_workflow", startTask.getProcessVariables().get(META_VALIDATION_WORKFLOW_ID));

        taskService.complete(startTask.getId(), Map.of("action", START.getFlowableString()));

        performSimpleWorkflow();
        performSimpleWorkflow();

        List<Task> finalTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup("finalDesk").list();
        Task archiveTask = assertReadExistenceAndGetMainTask(finalTasks, "main_archive");

        assertEquals("simple_workflow", archiveTask.getProcessVariables().get(META_VALIDATION_WORKFLOW_ID));
    }


    @Test void chain() {

        ProcessInstance processInstance = runtimeService.createProcessInstanceBuilder()
                .processDefinitionId(folderDefinition.getId())
                .name("name_01")
                .overrideProcessDefinitionTenantId(EXAMPLE_TENANT_ID)
                .businessKey("business_key_01")
                .variable(META_ORIGIN_GROUP_ID, "originDesk")
                .variable(META_FINAL_GROUP_ID, "finalDesk")
                .variable(META_CREATION_WORKFLOW_ID, "simple_workflow")
                .variable(META_VALIDATION_WORKFLOW_ID, "simple_workflow")
                .variable(META_DUE_DATE, "1977-04-22T01:00:00-05:00")
                .start();

        List<Task> startTasks = taskService.createTaskQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .taskCandidateGroup("originDesk")
                .list();

        Task startTask = assertReadExistenceAndGetMainTask(startTasks, "main_start");

        assertEquals("originDesk", startTask.getProcessVariables().get(META_ORIGIN_GROUP_ID));
        assertEquals("finalDesk", startTask.getProcessVariables().get(META_FINAL_GROUP_ID));
        assertEquals("simple_workflow", startTask.getProcessVariables().get(META_CREATION_WORKFLOW_ID));
        assertEquals("simple_workflow", startTask.getProcessVariables().get(META_VALIDATION_WORKFLOW_ID));

        taskService.complete(startTask.getId(), Map.of("action", START.getFlowableString()));

        // Creation workflow

        performSimpleWorkflow();

        // 1st validation workflow

        performSimpleWorkflow();

        // 1st chain

        List<Task> finalTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup("finalDesk").list();
        Task archiveTask = assertReadExistenceAndGetMainTask(finalTasks, "main_archive");

        runtimeService.setVariables(processInstance.getId(), Map.of("workflow_internal_validation_workflow_id", "alternate_workflow"));
        taskService.complete(
                archiveTask.getId(),
                Map.of("action", CHAIN.getFlowableString())
        );

        assertEquals(1L, runtimeService.createProcessInstanceQuery().includeProcessVariables().list().get(0)
                .getProcessVariables().get(META_LOOP_COUNT));

        // 2cd validation workflow

        performAlternateWorkflow();

        // 2cd chain

        finalTasks = taskService.createTaskQuery().includeProcessVariables().taskCandidateGroup("finalDesk").list();
        archiveTask = assertReadExistenceAndGetMainTask(finalTasks, "main_archive");

        runtimeService.setVariables(processInstance.getId(), Map.of("workflow_internal_validation_workflow_id", "simple_workflow"));
        taskService.complete(
                archiveTask.getId(),
                Map.of("action", CHAIN.getFlowableString())
        );

        assertEquals(2L, runtimeService.createProcessInstanceQuery().includeProcessVariables().list().get(0)
                .getProcessVariables().get(META_LOOP_COUNT));

        // 3rd validation workflow

        performSimpleWorkflow();
    }


}
