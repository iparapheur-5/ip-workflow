<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Workflow
  ~ Copyright (C) 2019-2023 Libriciel SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:flowable="http://flowable.org/bpmn"
    xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
    xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"
    xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <xsl:output
      indent="yes"
      encoding="UTF-8"/>


  <!-- Copy the entire XML -->

  <xsl:template match="@*|node()">

    <xsl:choose>

      <xsl:when test="name() = 'callActivity'">

        <!-- Finding previous task -->

        <xsl:variable name="currentTaskId" select="@id"/>

        <xsl:variable name="previousTaskId"
                      select="//bpmn:definitions/bpmn:process/bpmn:sequenceFlow[@targetRef = $currentTaskId]/@sourceRef"/>

        <xsl:variable name="isPreviousTaskRegular"
                      select="boolean(//bpmn:definitions/bpmn:process/bpmn:callActivity[(@id = $previousTaskId) and ((starts-with(@name, 'visa')) or starts-with(@name, 'signature'))])"/>

        <xsl:variable name="undoBoundaryEventId" select="concat(@id, '_undo_catch')"/>


        <!-- Undo boundary event -->

        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>

        <xsl:choose>
          <xsl:when test="$isPreviousTaskRegular">

            <xsl:element name="bpmn:boundaryEvent">

              <xsl:attribute name="id">
                <xsl:value-of select="$undoBoundaryEventId"/>
              </xsl:attribute>

              <xsl:attribute name="attachedToRef">
                <xsl:value-of select="$currentTaskId"/>
              </xsl:attribute>

              <xsl:element name="bpmn:errorEventDefinition">
                <xsl:attribute name="errorRef">
                  <xsl:value-of select="'undo'"/>
                </xsl:attribute>
              </xsl:element>

            </xsl:element>


            <!-- Undo back link -->

            <xsl:element name="bpmn:sequenceFlow">

              <xsl:attribute name="id">
                <xsl:value-of select="concat($undoBoundaryEventId, '_back_sequenceflow')"/>
              </xsl:attribute>

              <xsl:attribute name="sourceRef">
                <xsl:value-of select="$undoBoundaryEventId"/>
              </xsl:attribute>

              <xsl:attribute name="targetRef">
                <xsl:value-of select="$previousTaskId"/>
              </xsl:attribute>

            </xsl:element>

          </xsl:when>
        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


  <!--Removing the existing diagram-->

  <xsl:template match="//bpmn:definitions/bpmndi:BPMNDiagram"/>

</xsl:stylesheet>