<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Workflow
  ~ Copyright (C) 2019-2023 Libriciel SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:flowable="http://flowable.org/bpmn"
    xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
    xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"
    xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <xsl:output
      indent="yes"
      encoding="UTF-8"/>


  <!-- Copy the entire XML -->

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>


  <!-- callActivity attributes -->

  <xsl:template match="bpmn:callActivity/@name[starts-with(., 'visa') or starts-with(., 'signature')]">

    <xsl:attribute name="flowable:inheritBusinessKey">
      <xsl:value-of select="'true'"/>
    </xsl:attribute>

    <!-- calledElement -->

    <xsl:choose>
      <xsl:when test="starts-with(., 'visa')">
        <xsl:attribute name="calledElement">
          <xsl:value-of select="'i_Parapheur_internal_visa'"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="starts-with(., 'signature')">
        <xsl:attribute name="calledElement">
          <xsl:value-of select="'i_Parapheur_internal_signature'"/>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>

    <!-- Other needed parameters -->

    <xsl:attribute name="flowable:inheritVariables">
      <xsl:value-of select="'true'"/>
    </xsl:attribute>

    <xsl:attribute name="name">
      <xsl:value-of select="."/>
    </xsl:attribute>

  </xsl:template>

</xsl:stylesheet>
