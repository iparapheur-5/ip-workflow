
${Ansi.BRIGHT_BLACK}              ::::${Ansi.WHITE}####${Ansi.BRIGHT_BLACK}::::            ${Ansi.DEFAULT}                           &&      &&& &&
${Ansi.BRIGHT_BLACK}            ::::::${Ansi.WHITE}####${Ansi.BRIGHT_BLACK}::::::          ${Ansi.DEFAULT}                           &&     &&   &&
${Ansi.BRIGHT_BLACK}           ::::::::::::::::::         ${Ansi.DEFAULT}  &&      &&  &&&&&   &&&& &&  && &&   &&  &&&&&  &&      &&
${Ansi.BRIGHT_BLACK}          ::::::::${Ansi.WHITE}####${Ansi.BRIGHT_BLACK}::::::::        ${Ansi.DEFAULT}  &&      && &&   && &&    && &&  &&&& && &&   && &&      &&
${Ansi.BRIGHT_BLACK}         ::::::::${Ansi.WHITE}######${Ansi.BRIGHT_BLACK}::::::::       ${Ansi.DEFAULT}  &&  &&  && &     & &&    &&&    &&   && &     & &&  &&  &&
${Ansi.BRIGHT_BLACK}        :::::::::${Ansi.WHITE}######${Ansi.BRIGHT_BLACK}:::::::::      ${Ansi.DEFAULT}   && && &&  &&   && &&    && &&  &&   && &&   &&  && && &&
${Ansi.BRIGHT_BLACK}       ::::::::::::::::::::::::::     ${Ansi.DEFAULT}    &&  &&    &&&&&  &&    &&  && &&   &&  &&&&&    &&  &&
${Ansi.BRIGHT_BLACK}      :::::::::::${Ansi.WHITE}######${Ansi.BRIGHT_BLACK}:::::::::::
${Ansi.BRIGHT_BLACK}     :::::::::::${Ansi.WHITE}########${Ansi.BRIGHT_BLACK}:::::::::::   ${Ansi.YELLOW}      :: Workflow (${application.version}) :: Spring Boot (${spring-boot.version}) ::
${Ansi.BRIGHT_BLACK}     :::::::::::${Ansi.WHITE}########${Ansi.BRIGHT_BLACK}:::::::::::
${Ansi.DEFAULT}