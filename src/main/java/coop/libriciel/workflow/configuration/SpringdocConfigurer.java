/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SpringdocConfigurer {


    private @Value("${application.version?:DEVELOP}") String version;


    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .info(new Info()
                        .title("Workflow")
                        .description(
                                """
                                A Flowable wrapper with custom database requests integration, and unit-tests.
                                                        
                                Permissions are not checked here, and should be set on a previous level.
                                """
                        )
                        .version(version)
                        .license(new License().name("Affero GPL 3.0").url("https://www.gnu.org/licenses/agpl-3.0.en.html"))
                        .contact(new Contact().name("Libriciel SCOP").url("https://libriciel.fr").email("iparapheur@libriciel.coop"))
                )
                .externalDocs(new ExternalDocumentation().url("https://www.libriciel.fr/i-parapheur/"));
    }


}
