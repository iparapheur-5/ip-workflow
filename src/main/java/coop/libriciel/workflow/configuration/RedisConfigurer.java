/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.io.IOException;


@Log4j2
@Configuration
public class RedisConfigurer {


    public static final String TASK_HOOK_QUEUE = "i_Parapheur_internal_task_hook_trigger";


    @Bean
    ChannelTopic taskHookTopic() {
        return new ChannelTopic(TASK_HOOK_QUEUE);
    }


    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setValueSerializer(new RedisSerializer<>() {

            @Override public byte[] serialize(Object o) throws SerializationException {
                try {
                    return new ObjectMapper().writeValueAsBytes(o);
                } catch (JsonProcessingException e) {
                    throw new SerializationException(e.getMessage());
                }
            }

            @Override public Object deserialize(byte[] bytes) throws SerializationException {
                try {
                    return new ObjectMapper().readValue(bytes, Object.class);
                } catch (IOException e) {
                    throw new SerializationException(e.getMessage());
                }
            }

        });
        template.setConnectionFactory(connectionFactory);
        return template;
    }


}
