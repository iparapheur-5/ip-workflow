/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.services;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import coop.libriciel.workflow.models.SortBy;
import coop.libriciel.workflow.models.State;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.requests.FilteringParameter;
import coop.libriciel.workflow.models.requests.FilteringParameterCountResult;
import coop.libriciel.workflow.utils.TextUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.engine.ManagementService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.identitylink.service.impl.persistence.entity.HistoricIdentityLinkEntity;
import org.flowable.identitylink.service.impl.persistence.entity.IdentityLinkEntity;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.*;

import static coop.libriciel.workflow.models.Instance.META_ORIGIN_GROUP_ID;
import static coop.libriciel.workflow.models.SortBy.METADATA;
import static coop.libriciel.workflow.models.State.*;
import static coop.libriciel.workflow.utils.TextUtils.PSQL_RENDER_SETTINGS;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static java.util.Collections.emptyList;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsLast;
import static java.util.Locale.ROOT;
import static org.apache.commons.lang3.EnumUtils.getEnum;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.ParamType.INLINED;
import static org.jooq.impl.DSL.*;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Log4j2
@Service
public class DatabaseService {

    public static final String SQL_INSTANCE_ID = "INSTANCE_ID";
    public static final String SQL_INSTANCE_NAME = "INSTANCE_NAME";
    public static final String SQL_TASK_ID = "TASK_ID";
    public static final String SQL_TASK_NAME = "TASK_NAME";
    public static final String SQL_DUE_DATE = "DUE_DATE";
    public static final String SQL_VARIABLES = "VARIABLES";
    public static final String SQL_CANDIDATE_GROUP = "CANDIDATE_GROUP";
    public static final String SQL_STATE = "STATE";
    public static final String SQL_INSTANCE_COUNT = "INSTANCE_COUNT";
    public static final String SQL_SORT_BY = "SORT_BY";
    public static final String SQL_BEGIN_DATE = "BEGIN_DATE";
    public static final String SQL_END_DATE = "END_DATE";
    public static final String SQL_TYPE_ID = "TYPE_ID";
    public static final String SQL_SUBTYPE_ID = "SUBTYPE_ID";
    public static final String SQL_VALIDATION_DATE = "VALIDATION_DATE";
    public static final String SQL_READ_BY = "READ_BY";
    public static final String SQL_FILTER_BY_METADATA_KEY = "FILTER_BY_METADATA_KEY";
    public static final String SQL_FILTER_BY_METADATA_VALUE = "FILTER_BY_METADATA_VALUE";

    // Alias used when joining 'act_ru_execution' on root process ID (values in this row match the instance, not the task)
    public static final String TABLE_EXEC_ROOT_ALIAS = "act_ru_exec_root";
    public static final String INSTANCE_CREATION_TIME_FIELD = TABLE_EXEC_ROOT_ALIAS + ".START_TIME_";

    // Alias used when joining act_ru_variable on the name equal to META_CREATION_DATE for this instance
    public static final String TABLE_METADATA_CREATION_DATE_ALIAS = "act_ru_variable_vd";
    public static final String CREATION_DATE_METADATA_FIELD = TABLE_METADATA_CREATION_DATE_ALIAS + "._text_";


    private @Value("${spring.datasource.url}") String dbUrl;
    private @Value("${spring.datasource.username}") String dbUsername;
    private @Value("${spring.datasource.password}") String dbPassword;

    private final ManagementService managementService;
    private static HikariDataSource dataSource;

    private String tableHistProcInst;
    private String tableHistIdentityLink;
    private String tableHistTaskInst;
    private String tableHistVarInst;
    private String tableRuExecution;
    private String tableRuTask;
    private String tableRuIdentityLink;


    // <editor-fold desc="LifeCycle">


    public DatabaseService(ManagementService managementService) {
        this.managementService = managementService;
    }


    @PostConstruct
    public void init() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        config.setUsername(dbUsername);
        config.setPassword(dbPassword);
        dataSource = new HikariDataSource(config);
    }


    // </editor-fold desc="LifeCycle">


    public void initVariableNames() {
        tableHistProcInst = managementService.getTableName(HistoricProcessInstance.class).toLowerCase(ROOT);
        tableHistIdentityLink = managementService.getTableName(HistoricIdentityLinkEntity.class).toLowerCase(ROOT);
        tableHistTaskInst = managementService.getTableName(HistoricTaskInstance.class).toLowerCase(ROOT);
        tableHistVarInst = managementService.getTableName(HistoricVariableInstance.class).toLowerCase(ROOT);
        tableRuExecution = managementService.getTableName(Execution.class).toLowerCase(ROOT);
        tableRuTask = managementService.getTableName(org.flowable.task.api.Task.class).toLowerCase(ROOT);
        tableRuIdentityLink = managementService.getTableName(IdentityLinkEntity.class).toLowerCase(ROOT);
    }


    // <editor-fold desc="NativeQueries">


    @NotNull SelectOnConditionStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>>
    selectHistoricTask(@NotNull Field<String> sortBy) {

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("act_hi_procinst_root" + ".proc_inst_id_", String.class).as(SQL_INSTANCE_ID),
                        field("act_hi_procinst_root" + ".NAME_", String.class).as(SQL_INSTANCE_NAME),
                        field("act_hi_identitylink_current" + ".GROUP_ID_", String.class).as(SQL_CANDIDATE_GROUP),
                        field("act_hi_procinst_current" + ".ID_", String.class).as(SQL_TASK_ID),
                        field("act_hi_taskinst_current" + ".NAME_", String.class).as(SQL_TASK_NAME),
                        field("act_hi_taskinst_current" + ".DUE_DATE_", Date.class).as(SQL_DUE_DATE),
                        field("act_hi_procinst_root" + ".start_time_", Date.class).as(SQL_BEGIN_DATE),
                        field("act_hi_procinst_root" + ".end_time_", String.class).as(SQL_END_DATE),
                        field("act_hi_variable_t" + ".text_", String.class).as(SQL_TYPE_ID),
                        field("act_hi_variable_st" + ".text_", String.class).as(SQL_SUBTYPE_ID),
                        val(null, Date.class).as(SQL_VALIDATION_DATE), // FIXME
                        field(arrayAggDistinct(array(
                                field(tableHistVarInst + ".NAME_", String.class),
                                field(tableHistVarInst + ".TEXT_", String.class)
                        ))).as(SQL_VARIABLES),
                        field(arrayAggDistinct(field("task_read.assignee_", String.class))).as(SQL_READ_BY),
                        val(null, String.class).as(SQL_SORT_BY))

                .from(table(tableHistIdentityLink).as("act_hi_identitylink_orig"))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_orig"))
                .on(and(
                        field("act_hi_taskinst_orig" + ".ID_").equal(field("act_hi_identitylink_orig" + ".task_id_")),
                        field("act_hi_taskinst_orig" + ".end_time_").isNotNull()
                ))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_orig"))
                .on(field("act_hi_procinst_orig" + ".ID_").equal(field("act_hi_taskinst_orig" + ".PROC_INST_ID_")))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_root"))
                .on(and(
                                field("act_hi_procinst_root" + ".business_key_").equal(field("act_hi_procinst_orig" + ".business_key_"))),
                        field("act_hi_procinst_root" + ".super_process_instance_id_").isNull()
                )

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_current"))
                .on(field("act_hi_procinst_current" + ".business_key_").equal(field("act_hi_procinst_root.business_key_")))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_current"))
                .on(and(
                        field("act_hi_procinst_current" + ".id_").equal(field("act_hi_taskinst_current.proc_inst_id_")),
                        field("act_hi_taskinst_current" + ".end_time_").isNull(),
                        field("act_hi_taskinst_current" + ".name_").notIn(List.of(
                                val("undo", String.class),
                                val("read", String.class)
                        ))
                ))

                .innerJoin(table(tableHistIdentityLink).as("act_hi_identitylink_current"))
                .on(and(
                        field("act_hi_taskinst_current" + ".id_").equal(field("act_hi_identitylink_current.task_id_")),
                        field("act_hi_identitylink_current" + ".type_").eq("candidate")
                ))

                .leftJoin(table(tableHistVarInst).as("act_hi_variable_t"))
                .on(and(
                        field("act_hi_variable_t" + ".proc_inst_id_").equal(field("act_hi_procinst_root.id_")),
                        field("act_hi_variable_t" + ".name_").eq("i_Parapheur_internal_type_id")
                ))

                .leftJoin(table(tableHistVarInst).as("act_hi_variable_st"))
                .on(and(
                        field("act_hi_variable_st" + ".proc_inst_id_").equal(field("act_hi_procinst_root.id_")),
                        field("act_hi_variable_st" + ".name_").eq("i_Parapheur_internal_subtype_id")
                ))

                .leftOuterJoin(table(tableHistTaskInst).as("task_read"))
                .on(and(
                        field("task_read" + ".proc_inst_id_").equal(field("act_hi_taskinst_current.PROC_INST_ID_")),
                        field("task_read" + ".name_", String.class).equal("read"),
                        field("task_read" + ".end_time_", Date.class).isNotNull()
                ))

                .leftJoin(table(tableHistVarInst))
                .on(field(tableHistVarInst + ".proc_inst_id_").equal(field("act_hi_procinst_root.id_")));
    }


    @NotNull SelectOnConditionStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>>
    selectRuntimeTask(@NotNull Field<String> sortBy) {

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class).as(SQL_INSTANCE_ID),
                        field(TABLE_EXEC_ROOT_ALIAS + ".NAME_", String.class).as(SQL_INSTANCE_NAME),
                        field(tableRuIdentityLink + ".GROUP_ID_", String.class).as(SQL_CANDIDATE_GROUP),
                        field(tableRuTask + ".ID_", String.class).as(SQL_TASK_ID),
                        field(tableRuTask + ".NAME_", String.class).as(SQL_TASK_NAME),
                        field(tableRuTask + ".DUE_DATE_", Date.class).as(SQL_DUE_DATE),
                        field(INSTANCE_CREATION_TIME_FIELD, Date.class).as(SQL_BEGIN_DATE),
                        val(null, String.class).as(SQL_END_DATE),
                        field("act_ru_variable_t" + ".text_", String.class).as(SQL_TYPE_ID),
                        field("act_ru_variable_st" + ".text_", String.class).as(SQL_SUBTYPE_ID),
                        field("task_start" + ".end_time_", Date.class).as(SQL_VALIDATION_DATE),
                        field(arrayAggDistinct(array(
                                field("ACT_RU_VARIABLE_MD.NAME_", String.class),
                                field("ACT_RU_VARIABLE_MD.TEXT_", String.class)
                        ))).as(SQL_VARIABLES),
                        field(arrayAggDistinct(field("task_read.assignee_", String.class))).as(SQL_READ_BY),
                        sortBy.as(SQL_SORT_BY))

                .from(table(tableRuIdentityLink))

                .innerJoin(table(tableRuTask))
                .on(field(tableRuIdentityLink + ".TASK_ID_", String.class).equal(field(tableRuTask + ".ID_", String.class)))

                .innerJoin(table(tableRuExecution))
                .on(field(tableRuExecution + ".ID_", String.class).equal(field(tableRuTask + ".PROC_INST_ID_", String.class)))

                .innerJoin(table(tableRuExecution).as(TABLE_EXEC_ROOT_ALIAS))
                .on(field(TABLE_EXEC_ROOT_ALIAS + ".ID_", String.class).equal(field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class)))

                .innerJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLE_MD"))
                .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class).equal(field("ACT_RU_VARIABLE_MD.PROC_INST_ID_", String.class)))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("act_ru_variable_t"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class).equal(field("act_ru_variable_t.PROC_INST_ID_", String.class)),
                        field("act_ru_variable_t" + ".name_", String.class).equal(val("i_Parapheur_internal_type_id", String.class))
                ))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("act_ru_variable_st"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class).equal(field("act_ru_variable_st.PROC_INST_ID_", String.class)),
                        field("act_ru_variable_st" + ".name_", String.class).equal(val("i_Parapheur_internal_subtype_id", String.class))
                ))

                .leftOuterJoin(table(tableHistTaskInst).as("task_start"))
                .on(and(
                        field("task_start" + ".proc_inst_id_", String.class).equal(field("act_ru_task.PROC_INST_ID_", String.class)),
                        field("task_start" + ".name_", String.class).equal(val("main_start", String.class))
                ))

                .leftOuterJoin(table(tableHistTaskInst).as("task_read"))
                .on(and(
                        field("task_read" + ".proc_inst_id_", String.class).equal(field("act_ru_task.PROC_INST_ID_", String.class)),
                        field("task_read" + ".name_", String.class).equal(val("read", String.class)),
                        field("task_read" + ".end_time_", Date.class).isNotNull()
                ));
    }


    static List<Field<Object>> buildHistoricTasksFields() {
        return List.of(
                field(SQL_INSTANCE_ID),
                field(SQL_INSTANCE_NAME),
                field(SQL_CANDIDATE_GROUP),
                field(SQL_TASK_ID),
                field(SQL_TASK_NAME),
                field(SQL_DUE_DATE),
                field(SQL_BEGIN_DATE),
                field(SQL_SORT_BY),
                field(SQL_END_DATE),
                field(SQL_TYPE_ID),
                field(SQL_SUBTYPE_ID),
                field(SQL_VALIDATION_DATE)
        );
    }


    static List<Field<Object>> buildRuntimeTasksFields() {
        return List.of(
                field(SQL_INSTANCE_ID),
                field(SQL_INSTANCE_NAME),
                field(SQL_CANDIDATE_GROUP),
                field(SQL_TASK_ID),
                field(SQL_TASK_NAME),
                field(SQL_DUE_DATE),
                field(SQL_BEGIN_DATE),
                field(SQL_SORT_BY),
                field(SQL_TYPE_ID),
                field(SQL_SUBTYPE_ID),
                field(SQL_VALIDATION_DATE)
        );
    }


    Condition buildInstanceWhereConditions(@Nullable String tenantId,
                                           @Nullable List<FilteringParameter> filteringParameters,
                                           @Nullable State state,
                                           @Nullable String searchTerm,
                                           @Nullable String typeId,
                                           @Nullable String subtypeId,
                                           @Nullable Long createdAfterTime,
                                           @Nullable Long createdBeforeTime,
                                           @Nullable Long stillSinceTime) {
        return and(
                field(tableRuIdentityLink + ".TYPE_").equal("candidate"),
                buildPermissionCondition("ACT_RU_VARIABLES_DB", filteringParameters),
                Optional.ofNullable(tenantId)
                        .filter(StringUtils::isNotEmpty)
                        .map(t -> field(TABLE_EXEC_ROOT_ALIAS + ".tenant_id_", String.class).eq(t))
                        .orElse(trueCondition()),
                Optional.ofNullable(state)
                        .map(s -> switch (s) {
                            case DRAFT -> field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + DRAFT, "main_start");
                            case REJECTED -> field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + REJECTED, "main_delete");
                            case FINISHED -> field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + FINISHED, "main_archive");
                            case RETRIEVABLE -> field(tableRuTask + ".NAME_", String.class).equal("undo");
                            case LATE -> and(
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + DRAFT, "main_start"),
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + REJECTED, "main_delete"),
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + FINISHED, "main_archive"),
                                    field(tableRuTask + ".NAME_", String.class).notEqual("undo"),
                                    field(tableRuTask + ".due_date_", Date.class).isNotNull(),
                                    field(tableRuTask + ".due_date_", Date.class).lessThan(new Date())
                            );
                            default -> and(
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + DRAFT, "main_start"),
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + REJECTED, "main_delete"),
                                    field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + FINISHED, "main_archive"),
                                    field(tableRuTask + ".NAME_", String.class).notEqual("undo")
                            );
                        })
                        .orElse(trueCondition()),
                Optional.ofNullable(searchTerm)
                        .filter(StringUtils::isNotEmpty)
                        .map(s -> String.format("%%%s%%", s))
                        .map(w -> (Condition) field(TABLE_EXEC_ROOT_ALIAS + ".NAME_", String.class).likeIgnoreCase(w))
                        .orElse(trueCondition()),
                Optional.ofNullable(typeId)
                        .filter(StringUtils::isNotEmpty)
                        .map(i -> field("act_ru_variable_t" + ".text_", String.class).eq(i))
                        .orElse(trueCondition()),
                Optional.ofNullable(subtypeId)
                        .filter(StringUtils::isNotEmpty)
                        .map(i -> field("act_ru_variable_st" + ".text_", String.class).eq(i))
                        .orElse(trueCondition()),
                Optional.ofNullable(createdAfterTime)
                        .map(Timestamp::new)
                        .map(d -> and(
                                field(INSTANCE_CREATION_TIME_FIELD, Timestamp.class).isNotNull(),
                                field(INSTANCE_CREATION_TIME_FIELD, Timestamp.class).greaterOrEqual(d)
                        ))
                        .orElse(trueCondition()),
                Optional.ofNullable(createdBeforeTime)
                        .map(Timestamp::new)
                        .map(d -> and(
                                field(INSTANCE_CREATION_TIME_FIELD, Timestamp.class).isNotNull(),
                                field(INSTANCE_CREATION_TIME_FIELD, Timestamp.class).lessOrEqual(d)
                        ))
                        .orElse(trueCondition()),
                Optional.ofNullable(stillSinceTime)
                        .map(Timestamp::new)
                        .map(d -> and(
                                field(tableRuTask + ".CREATE_TIME_", Timestamp.class).isNotNull(),
                                field(tableRuTask + ".CREATE_TIME_", Timestamp.class).lessOrEqual(d)
                        ))
                        .orElse(trueCondition()),
                field(tableRuTask + ".NAME_").notEqual("read")
        );
    }


    private String truncateDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);
        return TextUtils.toIso8601String(calendar.getTime());
    }

    Condition buildPermissionCondition(@NotNull String metaTable, @Nullable List<FilteringParameter> filteringParameters) {

        if (CollectionUtils.isEmpty(filteringParameters)) {
            return trueCondition();
        }

        // We consider a `group:null:null rule as a "full group delegation" one.
        // We're filtering every one of those, and add those to the simpler group list.
        List<String> groups = filteringParameters.stream()
                .filter(d -> isNotEmpty(d.getGroupId()))
                .filter(d -> isEmpty(d.getFilterMetadataKey()))
                .map(FilteringParameter::getGroupId)
                .distinct()
                .sorted(naturalOrder())
                .toList();

        // Since we've sorted every `group:null:null` cases, we'll want all the others.
        // Irrelevant cases, like `null:key:value`, `group:null:value`, etc., are filtered out too.
        List<FilteringParameter> metadataFilters = filteringParameters.stream()
                .filter(d -> isNotEmpty(d.getGroupId()))
                .filter(d -> isNotEmpty(d.getFilterMetadataKey()))
                .filter(d -> isNotEmpty(d.getFilterMetadataValue()))
                // Sorting in alphabetical order, to ease unit-tests
                .sorted(Comparator.comparing(FilteringParameter::getGroupId, nullsLast(naturalOrder()))
                        .thenComparing(FilteringParameter::getFilterMetadataKey, nullsLast(naturalOrder()))
                        .thenComparing(FilteringParameter::getFilterMetadataValue, nullsLast(naturalOrder())))
                .toList();

        // Building SQL request.

        List<Condition> filteredConditionList = new ArrayList<>();

        if (!groups.isEmpty()) {
            filteredConditionList.add(and(
                    field(tableRuIdentityLink + ".GROUP_ID_").in(groups),
                    // The LEFT JOIN has to be reduced here
                    field(metaTable + ".NAME_").equal("workflow_internal_origin_group_id"))
            );
        }

        filteringParameters.stream()
                .filter(d -> isEmpty(d.getGroupId()))
                .filter(d -> isNotEmpty(d.getFilterMetadataKey()))
                .filter(d -> isNotEmpty(d.getFilterMetadataValue()))
                .map(d -> and(
                        field(metaTable + ".NAME_").equal(d.getFilterMetadataKey()),
                        field(metaTable + ".TEXT_").equal(d.getFilterMetadataValue())
                ))
                .forEach(filteredConditionList::add);

        metadataFilters.stream()
                .map(c -> and(
                        field(tableRuIdentityLink + ".GROUP_ID_").equal(c.getGroupId()),
                        field(metaTable + ".NAME_").equal(c.getFilterMetadataKey()),
                        field(metaTable + ".TEXT_").equal(c.getFilterMetadataValue())))
                .forEach(filteredConditionList::add);

        return or(filteredConditionList);
    }

    @NotNull SelectForUpdateStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>>
    buildHistoricTasksSqlRequest(@Nullable String tenantId,
                                 @Nullable List<FilteringParameter> filteringParameters,
                                 @Nullable State state,
                                 @Nullable String sortBy,
                                 boolean asc,
                                 int page,
                                 int pageSize,
                                 @Nullable String searchTerm,
                                 @Nullable String typeId,
                                 @Nullable String subtypeId,
                                 @Nullable Long createdAfterTime,
                                 @Nullable Long createdBeforeTime,
                                 @Nullable Long stillSinceTime) {

        // Building sort conditions

        Condition sqlMetadataCondition = trueCondition();
        String sqlSortColumn;

        switch (firstNonNull(getEnum(SortBy.class, sortBy), METADATA)) {
            case ACTION_TYPE -> sqlSortColumn = tableRuTask + ".NAME_";
            case INSTANCE_ID -> sqlSortColumn = tableRuExecution + ".ROOT_PROC_INST_ID_";
            case TASK_ID -> sqlSortColumn = tableRuTask + ".ID_";
//            case CREATION_NAME -> sqlSortColumn = TABLE_EXEC_ROOT_ALIAS + ".START_TIME_";
            case CREATION_DATE -> sqlSortColumn = INSTANCE_CREATION_TIME_FIELD;
            case STILL_SINCE_DATE -> sqlSortColumn = tableRuTask + ".CREATE_TIME_";
            case LATE_DATE -> sqlSortColumn = tableRuTask + ".DUE_DATE_";
            case INSTANCE_NAME -> sqlSortColumn = "act_ru_exec_root.NAME_";

            // If nothing above matches, sort by metadata
            default -> {
                sqlSortColumn = "ACT_RU_VARIABLES_SB.TEXT_";
                sqlMetadataCondition = field("ACT_RU_VARIABLES_SB.NAME_").equal(sortBy);
            }
        }

        String originGroupId = Optional.ofNullable(filteringParameters)
                .orElse(emptyList())
                .stream()
                .map(FilteringParameter::getGroupId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No origin desk id found in filtering parameters"));

        // Build up the SQL request

        SelectForUpdateStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>> result =
                selectHistoricTask(field(sqlSortColumn, String.class))

                        // FIXME
//                        .leftJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLES_SB"))
//                        .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("ACT_RU_VARIABLES_SB.PROC_INST_ID_")))
//                        .and(sqlMetadataCondition)
//
//                        .leftJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLES_DB"))
//                        .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("ACT_RU_VARIABLES_DB.PROC_INST_ID_")))

                        .where(and(
                                field("act_hi_identitylink_orig" + ".group_id_", String.class).eq(originGroupId),
                                field("act_hi_identitylink_orig" + ".type_", String.class).eq("candidate")
                        ))
                        .groupBy(buildHistoricTasksFields())

                        .orderBy(
                                asc ? field(SQL_SORT_BY).asc().nullsFirst() : field(SQL_SORT_BY).desc().nullsLast(),
                                field(SQL_BEGIN_DATE).asc()
                        )
                        .limit(pageSize)
                        .offset(pageSize * page);

        log.trace("buildTasksSqlRequest :\n{}", result.getSQL(INLINED));
        return result;
    }


    @NotNull SelectForUpdateStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>>
    buildRuntimeTasksSqlRequest(@Nullable String tenantId,
                                @Nullable List<FilteringParameter> filteringParameters,
                                @Nullable State state,
                                @Nullable String sortBy,
                                boolean asc,
                                int page,
                                int pageSize,
                                @Nullable String searchTerm,
                                @Nullable String typeId,
                                @Nullable String subtypeId,
                                @Nullable Long createdAfterTime,
                                @Nullable Long createdBeforeTime,
                                @Nullable Long stillSinceTime) {

        // Building sort conditions

        Condition sqlMetadataCondition = trueCondition();
        String sqlSortColumn;

        switch (firstNonNull(getEnum(SortBy.class, sortBy), METADATA)) {
            case ACTION_TYPE -> sqlSortColumn = tableRuTask + ".NAME_";
            case INSTANCE_ID -> sqlSortColumn = tableRuExecution + ".ROOT_PROC_INST_ID_";
            case TASK_ID -> sqlSortColumn = tableRuTask + ".ID_";
            // TODO not working correctly at the moment, we should use a metadata set at the correct time
//            case VALIDATION_START_DATE -> sqlSortColumn = TABLE_EXEC_ROOT_ALIAS + ".START_TIME_";
            case CREATION_DATE -> sqlSortColumn = INSTANCE_CREATION_TIME_FIELD;
            case STILL_SINCE_DATE -> sqlSortColumn = tableRuTask + ".CREATE_TIME_";
            case LATE_DATE -> sqlSortColumn = tableRuTask + ".DUE_DATE_";
            case INSTANCE_NAME -> sqlSortColumn = "act_ru_exec_root.NAME_";

            // If nothing above matches, sort by metadata
            default -> {
                sqlSortColumn = "ACT_RU_VARIABLES_SB.TEXT_";
                sqlMetadataCondition = field("ACT_RU_VARIABLES_SB.NAME_").equal(sortBy);
            }
        }

        // Build up the SQL request

        SelectForUpdateStep<Record14<String, String, String, String, String, Date, Date, String, String, String, Date, String[][], String[], String>> result =
                selectRuntimeTask(field(sqlSortColumn, String.class))

                        .leftJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLES_SB"))
                        .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("ACT_RU_VARIABLES_SB.PROC_INST_ID_")))
                        .and(sqlMetadataCondition)

                        .leftJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLES_DB"))
                        .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("ACT_RU_VARIABLES_DB.PROC_INST_ID_")))

                        .where(buildInstanceWhereConditions(tenantId, filteringParameters, state, searchTerm, typeId, subtypeId, createdAfterTime, createdBeforeTime, stillSinceTime))
                        .groupBy(buildRuntimeTasksFields())

                        .orderBy(
                                asc ? field(SQL_SORT_BY).asc().nullsFirst() : field(SQL_SORT_BY).desc().nullsLast(),
                                field(SQL_BEGIN_DATE).asc()
                        )
                        .limit(pageSize)
                        .offset(pageSize * page);

        log.trace("buildTasksSqlRequest :\n{}", result.getSQL(INLINED));
        return result;
    }


    @NotNull Select<Record1<Integer>>
    countRuntimeTasksSqlRequest(@Nullable String tenantId,
                                @Nullable List<FilteringParameter> filteringParameters,
                                @Nullable State state,
                                @Nullable String searchTerm,
                                @Nullable String typeId,
                                @Nullable String subtypeId,
                                @Nullable Long createdAfterTime,
                                @Nullable Long emitBeforeTime,
                                @Nullable Long stillSinceTime) {

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(countDistinct(field(tableRuExecution + ".ID_")))
                .from(table(tableRuIdentityLink))

                .innerJoin(table(tableRuTask))
                .on(field(tableRuIdentityLink + ".TASK_ID_").equal(field(tableRuTask + ".ID_")))

                .innerJoin(table(tableRuExecution))
                .on(field(tableRuExecution + ".ID_").equal(field(tableRuTask + ".PROC_INST_ID_")))

                .innerJoin(table(tableRuExecution).as(TABLE_EXEC_ROOT_ALIAS))
                .on(field(TABLE_EXEC_ROOT_ALIAS + ".ID_").equal(field(tableRuExecution + ".ROOT_PROC_INST_ID_")))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("act_ru_variable_t"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("act_ru_variable_t.PROC_INST_ID_")),
                        field("act_ru_variable_t" + ".name_").equal("i_Parapheur_internal_type_id")
                ))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("act_ru_variable_st"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("act_ru_variable_st.PROC_INST_ID_")),
                        field("act_ru_variable_st" + ".name_").equal("i_Parapheur_internal_subtype_id")
                ))

                .where(buildInstanceWhereConditions(tenantId, filteringParameters, state, searchTerm, typeId, subtypeId, createdAfterTime, emitBeforeTime, stillSinceTime));
    }


    @NotNull Select<Record1<Integer>>
    countHistoricTasksSqlRequest(@Nullable String tenantId,
                                 @Nullable List<FilteringParameter> filteringParameters,
                                 @Nullable State state,
                                 @Nullable String searchTerm,
                                 @Nullable String typeId,
                                 @Nullable String subtypeId,
                                 @Nullable Long createdAfterTime,
                                 @Nullable Long emitBeforeTime,
                                 @Nullable Long stillSinceTime) {

        String originGroupId = Optional.ofNullable(filteringParameters)
                .orElse(emptyList())
                .stream()
                .map(FilteringParameter::getGroupId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No origin desk id found in filtering parameters"));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(countDistinct(field("act_hi_procinst_root" + ".proc_inst_id_", String.class)))

                .from(table(tableHistIdentityLink).as("act_hi_identitylink_orig"))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_orig"))
                .on(and(
                        field("act_hi_taskinst_orig" + ".ID_").equal(field("act_hi_identitylink_orig" + ".task_id_")),
                        field("act_hi_taskinst_orig" + ".end_time_").isNotNull()
                ))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_orig"))
                .on(field("act_hi_procinst_orig" + ".ID_").equal(field("act_hi_taskinst_orig" + ".PROC_INST_ID_")))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_root"))
                .on(and(
                                field("act_hi_procinst_root" + ".business_key_").equal(field("act_hi_procinst_orig" + ".business_key_"))),
                        field("act_hi_procinst_root" + ".super_process_instance_id_").isNull()
                )

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_current"))
                .on(field("act_hi_procinst_current" + ".business_key_").equal(field("act_hi_procinst_root.business_key_")))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_current"))
                .on(and(
                        field("act_hi_procinst_current" + ".id_").equal(field("act_hi_taskinst_current.proc_inst_id_")),
                        field("act_hi_taskinst_current" + ".end_time_").isNull(),
                        field("act_hi_taskinst_current" + ".name_").notIn(List.of(
                                val("undo", String.class),
                                val("read", String.class)
                        ))
                ))

                .where(and(
                        field("act_hi_identitylink_orig" + ".group_id_", String.class).eq(originGroupId),
                        field("act_hi_identitylink_orig" + ".type_", String.class).eq("candidate")
                ));
    }


    @NotNull Select<Record5<String, String, String, String, Integer>> buildCountHistoricFolders(@NotNull List<FilteringParameter> filteringParameters) {

        String originGroupId = Optional.of(filteringParameters)
                .orElse(emptyList())
                .stream()
                .map(FilteringParameter::getGroupId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No origin desk id found in filtering parameters"));

        SelectConditionStep<Record5<String, String, String, String, Integer>> result = using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        val(originGroupId, String.class).as(SQL_CANDIDATE_GROUP),
                        val(DOWNSTREAM, String.class).as(SQL_STATE),
                        val(null, String.class).as(SQL_FILTER_BY_METADATA_KEY),
                        val(null, String.class).as(SQL_FILTER_BY_METADATA_VALUE),
                        countDistinct(field("act_hi_procinst_root" + ".proc_inst_id_", String.class)).as(SQL_INSTANCE_COUNT)
                )

                .from(table(tableHistIdentityLink).as("act_hi_identitylink_orig"))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_orig"))
                .on(and(
                        field("act_hi_taskinst_orig" + ".ID_").equal(field("act_hi_identitylink_orig" + ".task_id_")),
                        field("act_hi_taskinst_orig" + ".end_time_").isNotNull()
                ))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_orig"))
                .on(field("act_hi_procinst_orig" + ".ID_").equal(field("act_hi_taskinst_orig" + ".PROC_INST_ID_")))

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_root"))
                .on(and(
                                field("act_hi_procinst_root" + ".business_key_").equal(field("act_hi_procinst_orig" + ".business_key_"))),
                        field("act_hi_procinst_root" + ".super_process_instance_id_").isNull()
                )

                .innerJoin(table(tableHistProcInst).as("act_hi_procinst_current"))
                .on(field("act_hi_procinst_current" + ".business_key_").equal(field("act_hi_procinst_root.business_key_")))

                .innerJoin(table(tableHistTaskInst).as("act_hi_taskinst_current"))
                .on(and(
                        field("act_hi_procinst_current" + ".id_").equal(field("act_hi_taskinst_current.proc_inst_id_")),
                        field("act_hi_taskinst_current" + ".end_time_").isNull(),
                        field("act_hi_taskinst_current" + ".name_").notIn(List.of(
                                val("undo", String.class),
                                val("read", String.class)
                        ))
                ))

                .where(and(
                        field("act_hi_identitylink_orig" + ".group_id_", String.class).eq(originGroupId),
                        field("act_hi_identitylink_orig" + ".type_", String.class).eq("candidate")
                ));

        log.trace("buildCountFolders:\n{}", result.getSQL(INLINED));
        return result;
    }


    @NotNull Select<Record5<String, String, String, String, Integer>> buildCountRuntimeFolders(@NotNull List<FilteringParameter> filteringParameters) {

        Select<Record5<String, String, String, String, Integer>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(tableRuIdentityLink + ".GROUP_ID_", String.class).as(SQL_CANDIDATE_GROUP),
                        decode()
                                .when(field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + DRAFT, "main_start"), DRAFT.toString())
                                .when(field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + REJECTED, "main_delete"), REJECTED.toString())
                                .when(field(tableRuTask + ".NAME_", String.class).in(WORKFLOW_INTERNAL + FINISHED, "main_archive"), FINISHED.toString())
                                .otherwise(PENDING.toString()).as(SQL_STATE),
                        decode().when(field("ACT_RU_VARIABLES_DB.NAME_", String.class).equal(META_ORIGIN_GROUP_ID),
                                        val(null, String.class)).otherwise(field("ACT_RU_VARIABLES_DB.NAME_", String.class))
                                .as(SQL_FILTER_BY_METADATA_KEY),
                        decode().when(field("ACT_RU_VARIABLES_DB.NAME_", String.class).equal(META_ORIGIN_GROUP_ID),
                                        val(null, String.class))
                                .otherwise(field("ACT_RU_VARIABLES_DB.TEXT_", String.class))
                                .as(SQL_FILTER_BY_METADATA_VALUE),
                        countDistinct(field(tableRuExecution + ".ID_")).as(SQL_INSTANCE_COUNT)
                )

                .from(table(tableRuIdentityLink))

                .innerJoin(table(tableRuTask))
                .on(field(tableRuIdentityLink + ".TASK_ID_").equal(field(tableRuTask + ".ID_")))

                .innerJoin(table(tableRuExecution))
                .on(field(tableRuExecution + ".ID_").equal(field(tableRuTask + ".PROC_INST_ID_")))

                .leftJoin(table("ACT_RU_VARIABLE").as("ACT_RU_VARIABLES_DB"))
                .on(field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("ACT_RU_VARIABLES_DB.PROC_INST_ID_")))

                .where(field(tableRuIdentityLink + ".TYPE_").equal("candidate"))
                .and(buildPermissionCondition("ACT_RU_VARIABLES_DB", filteringParameters))
                .and(field(tableRuTask + ".NAME_").notEqual("undo"))
                .and(field(tableRuTask + ".NAME_").notEqual("read"))

                .groupBy(field(SQL_CANDIDATE_GROUP), field(SQL_STATE), field(SQL_FILTER_BY_METADATA_KEY), field(SQL_FILTER_BY_METADATA_VALUE));

        log.trace("buildCountFolders:\n{}", result.getSQL(INLINED));
        return result;
    }


    @NotNull Select<Record5<String, String, String, String, Integer>> buildCountLateFolders(@NotNull List<String> groups) {

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(tableRuIdentityLink + ".GROUP_ID_", String.class).as(SQL_CANDIDATE_GROUP),
                        val(LATE.toString()).as(SQL_STATE),
                        val(null, String.class).as(SQL_FILTER_BY_METADATA_KEY),
                        val(null, String.class).as(SQL_FILTER_BY_METADATA_VALUE),
                        count(field(tableRuExecution + ".ID_")).as(SQL_INSTANCE_COUNT)
                )
                .from(table(tableRuIdentityLink))
                .innerJoin(table(tableRuTask))
                .on(field(tableRuIdentityLink + ".TASK_ID_").equal(field(tableRuTask + ".ID_")))
                .innerJoin(table(tableRuExecution))
                .on(field(tableRuExecution + ".ID_").equal(field(tableRuTask + ".PROC_INST_ID_")))

                .where(field(tableRuIdentityLink + ".TYPE_").equal("candidate"))
                .and(field(tableRuIdentityLink + ".GROUP_ID_").in(groups))
                .and(field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + DRAFT, "main_start"))
                .and(field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + REJECTED, "main_delete"))
                .and(field(tableRuTask + ".NAME_", String.class).notIn(WORKFLOW_INTERNAL + FINISHED, "main_archive"))
                .and(field(tableRuTask + ".NAME_", String.class).notEqual("undo"))
                .and(field(tableRuTask + ".NAME_", String.class).notEqual("read"))
                .and(field(tableRuTask + ".DUE_DATE_").isNotNull())
                .and(field(tableRuTask + ".DUE_DATE_").lessThan(now()))

                .groupBy(field(SQL_CANDIDATE_GROUP), field(SQL_STATE), field(SQL_FILTER_BY_METADATA_KEY), field(SQL_FILTER_BY_METADATA_VALUE));
    }


    @NotNull Select<Record5<String, String, String, String, Integer>> buildCountMetadataUsage(@Nullable List<FilteringParameter> filteringParameters) {

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        val(null, String.class).as(SQL_CANDIDATE_GROUP),
                        val(null, String.class).as(SQL_STATE),
                        field(tableHistVarInst + ".NAME_", String.class).as(SQL_FILTER_BY_METADATA_KEY),
                        field(tableHistVarInst + ".TEXT_", String.class).as(SQL_FILTER_BY_METADATA_VALUE),
                        count(field(tableHistProcInst + ".ID_")).as(SQL_INSTANCE_COUNT)
                )
                .from(tableHistProcInst)
                .innerJoin(table(tableHistVarInst))
                .on(field(tableHistProcInst + ".PROC_INST_ID_").equal(field(tableHistVarInst + ".PROC_INST_ID_")))

                .where(field(tableHistProcInst + ".SUPER_PROCESS_INSTANCE_ID_").isNull())
                .and(buildPermissionCondition(tableHistVarInst, filteringParameters))

                .groupBy(field(SQL_FILTER_BY_METADATA_KEY), field(SQL_FILTER_BY_METADATA_VALUE));
    }


    // </editor-fold desc="NativeQueries">


    // <editor-fold desc="Utils">


    private @NotNull List<FilteringParameterCountResult> executeCount(@NotNull State state, @NotNull List<FilteringParameter> filteringParameters) {

        Select<Record5<String, String, String, String, Integer>> countRequest = (state == DOWNSTREAM)
                ? buildCountHistoricFolders(filteringParameters)
                : buildCountRuntimeFolders(filteringParameters);

        return executeCount(countRequest)
                .stream()
                // Since we're retrieving 2 states in the `buildCountDraftAndPendingFolders` request,
                // we filter to get only the relevant one
                .filter(c -> c.getState() == state)
                .toList();
    }


    private @NotNull List<FilteringParameterCountResult> executeCount(@NotNull Select<Record5<String, String, String, String, Integer>> request) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(request)
                    .stream()
                    .map(FilteringParameterCountResult::new)
                    .toList();
        } catch (SQLException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot reach database", e);
        }
    }


    // </editor-fold desc="Utils">


    public @NotNull List<Task> getTasks(@Nullable String tenantId,
                                        @Nullable List<FilteringParameter> filteringParameters,
                                        @Nullable State state,
                                        @NotNull String sortBy,
                                        boolean asc,
                                        int page,
                                        int pageSize,
                                        @Nullable String typeId,
                                        @Nullable String subtypeId,
                                        @Nullable String searchTerm,
                                        @Nullable Long createdAfterTime,
                                        @Nullable Long createdBeforeTime,
                                        @Nullable Long stillSinceTime) {

        log.debug("getTasks state:{} sortBy:{} asc:{} page:{} pageSize:{} searchTerm:{}, createdAfterTime : {}, createdBeforeTime:{} stillSinceTime:{}",
                state, sortBy, asc, page, pageSize, searchTerm, createdAfterTime, createdBeforeTime, stillSinceTime);

        try (Connection connection = dataSource.getConnection()) {
            if (state == DOWNSTREAM) {
                return DSL.using(connection, PSQL_RENDER_SETTINGS)
                        .fetch(buildHistoricTasksSqlRequest(
                                tenantId,
                                filteringParameters,
                                state,
                                sortBy,
                                asc,
                                page,
                                pageSize,
                                searchTerm,
                                typeId,
                                subtypeId,
                                createdAfterTime,
                                createdBeforeTime,
                                stillSinceTime
                        ))
                        .stream()
                        .map(Task::new)
                        .toList();
            } else {
                return DSL.using(connection, PSQL_RENDER_SETTINGS)
                        .fetch(buildRuntimeTasksSqlRequest(
                                tenantId,
                                filteringParameters,
                                state,
                                sortBy,
                                asc,
                                page,
                                pageSize,
                                searchTerm,
                                typeId,
                                subtypeId,
                                createdAfterTime,
                                createdBeforeTime,
                                stillSinceTime
                        ))
                        .stream()
                        .map(Task::new)
                        .toList();
            }
        } catch (SQLException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot reach database", e);
        }
    }


    public int countTasks(@Nullable String tenantId, @Nullable List<FilteringParameter> filteringParameters,
                          @Nullable State state, @Nullable String typeId, @Nullable String subtypeId, @Nullable String searchTerm,
                          @Nullable Long createdAfterTime, @Nullable Long emitBeforeTime, @Nullable Long stillSinceTime) {

        log.debug("countTasks state:{} searchTerm:{} emitBeforeTime:{}", state, searchTerm, emitBeforeTime);

        try (Connection connection = dataSource.getConnection()) {
            if (state == DOWNSTREAM) {
                return DSL.using(connection, PSQL_RENDER_SETTINGS)
                        .fetchOptional(countHistoricTasksSqlRequest(
                                tenantId,
                                filteringParameters,
                                state,
                                searchTerm,
                                typeId,
                                subtypeId,
                                createdAfterTime,
                                emitBeforeTime,
                                stillSinceTime
                        ))
                        .map(r -> r.get(0, Integer.class))
                        .orElse(-1);
            } else {
                return DSL.using(connection, PSQL_RENDER_SETTINGS)
                        .fetchOptional(countRuntimeTasksSqlRequest(
                                tenantId,
                                filteringParameters,
                                state,
                                searchTerm,
                                typeId,
                                subtypeId,
                                createdAfterTime,
                                emitBeforeTime,
                                stillSinceTime
                        ))
                        .map(r -> r.get(0, Integer.class))
                        .orElse(-1);
            }
        } catch (SQLException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Cannot reach database", e);
        }
    }


    public @NotNull List<FilteringParameterCountResult> getCount(@NotNull State state, @NotNull List<FilteringParameter> filteringParameters) {
        return executeCount(state, filteringParameters);
    }


    public @NotNull List<FilteringParameterCountResult> getCount(@NotNull List<String> groups) {
        return executeCount(
                buildCountRuntimeFolders(
                        groups.stream()
                                .map(i -> new FilteringParameter(i, null, null, null))
                                .toList()
                ).union(buildCountLateFolders(groups))
        );
    }


    public @NotNull List<FilteringParameterCountResult> getMetadataUsage(@NotNull List<FilteringParameter> filteringParameters) {
        return executeCount(buildCountMetadataUsage(filteringParameters));
    }


}
