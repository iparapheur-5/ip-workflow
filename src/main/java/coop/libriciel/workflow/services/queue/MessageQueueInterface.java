/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.services.queue;

import org.flowable.engine.runtime.Execution;
import org.flowable.task.service.delegate.DelegateTask;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public interface MessageQueueInterface {

    String SERVICE_NAME = "messageQueueService";
    String PROVIDER_KEY = "service.queue.provider";


    @SuppressWarnings("unused")
    void triggerHook(@NotNull String expectedAction, @Nullable String state, @NotNull Execution execution, @Nullable DelegateTask task);


}
