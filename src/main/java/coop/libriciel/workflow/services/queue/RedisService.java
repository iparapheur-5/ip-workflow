/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.services.queue;

import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.utils.TextUtils;
import lombok.extern.log4j.Log4j2;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.variable.api.delegate.VariableScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

import java.util.*;

import static coop.libriciel.workflow.models.Instance.META_DUE_DATE;
import static coop.libriciel.workflow.models.Task.META_CURRENT_CANDIDATE_GROUPS;
import static coop.libriciel.workflow.models.Task.META_NOTIFIED_GROUPS;
import static coop.libriciel.workflow.models.Task.NotificationType.ACTION_PERFORMED;
import static coop.libriciel.workflow.models.Task.NotificationType.NEW_ON_DESK;
import static coop.libriciel.workflow.models.Visibility.CONFIDENTIAL;
import static coop.libriciel.workflow.utils.CollectionUtils.popValue;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toMap;

@Log4j2
@Service(MessageQueueInterface.SERVICE_NAME)
@ConditionalOnProperty(name = MessageQueueInterface.PROVIDER_KEY, havingValue = "redis")
public class RedisService implements MessageQueueInterface {


    // <editor-fold desc="Beans">


    private final RedisTemplate<String, Object> redisTemplate;
    private final ChannelTopic taskHookTopic;
    private final HistoryService historyService;


    @Autowired
    public RedisService(RedisTemplate<String, Object> redisTemplate, ChannelTopic taskHookTopic, HistoryService historyService) {
        this.redisTemplate = redisTemplate;
        this.taskHookTopic = taskHookTopic;
        this.historyService = historyService;
    }


    // </editor-fold desc="Beans">


    /**
     * This method is called directly from the action's BPMN.
     */
    @SuppressWarnings("unused")
    public void triggerHook(@NotNull String expectedAction, @Nullable String performedAction, @NotNull Execution execution, @Nullable DelegateTask task) {

        Task.Action expected = Optional.of(expectedAction).map(String::toUpperCase).map(Task.Action::valueOf).orElse(null);
        Task.Action performed = Optional.ofNullable(performedAction).map(String::toUpperCase).map(Task.Action::valueOf).orElse(null);
        String taskId = Optional.ofNullable(task).map(DelegateTask::getId).orElse(null);
        Map<String, String> parsedVariables = Optional.ofNullable(task)
                .map(VariableScope::getVariables)
                .orElse(((VariableScope) execution).getVariables())
                .entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey, entry -> String.valueOf(entry.getValue())));

        List<String> candidateGroups = Optional.ofNullable(popValue(parsedVariables, META_CURRENT_CANDIDATE_GROUPS))
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        List<String> notifiedGroups = Optional.ofNullable(popValue(parsedVariables, META_NOTIFIED_GROUPS))
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        Date dueDate = Optional.ofNullable(popValue(parsedVariables, META_DUE_DATE))
                .map(TextUtils::fromIso8601String)
                .orElse(null);

        String rootProcessInstanceName = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(execution.getRootProcessInstanceId())
                .list()
                .stream()
                .findFirst()
                .map(HistoricProcessInstance::getName)
                .orElse(null);

        Task notifiedTask = new Task(
                taskId,
                null,
                execution.getRootProcessInstanceId(),
                rootProcessInstanceName,
                expected,
                performed,
                performedAction != null ? ACTION_PERFORMED : NEW_ON_DESK,
                (performed == null) ? new Date() : null,
                dueDate,
                (performed != null) ? new Date() : null,
                candidateGroups,
                notifiedGroups,
                emptyList(),
                emptyList(),
                null,
                null,
                false,
                parsedVariables,
                null,
                null,
                null,
                1L,
                0L,
                CONFIDENTIAL
        );

        redisTemplate.convertAndSend(taskHookTopic.getTopic(), notifiedTask);
    }


}
