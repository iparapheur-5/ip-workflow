/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.services;

import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.Task;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.CallActivity;
import org.flowable.bpmn.model.IOParameter;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Log4j2
@Getter
@Service
public class FlowableService {


    // <editor-fold desc="Beans">


    private final TaskService taskService;
    private final RuntimeService runtimeService;
    private final HistoryService historyService;
    private final RepositoryService repositoryService;


    public FlowableService(HistoryService historyService, RuntimeService runtimeService, TaskService taskService, RepositoryService repositoryService) {
        this.taskService = taskService;
        this.runtimeService = runtimeService;
        this.historyService = historyService;
        this.repositoryService = repositoryService;
    }


    // </editor-fold desc="Beans">


    public @NotNull Optional<Instance> getRuntimeInstance(@NotNull String id) {
        return Optional.ofNullable(
                        runtimeService
                                .createProcessInstanceQuery()
                                .includeProcessVariables()
                                .excludeSubprocesses(false)
                                .processInstanceId(id)
                                .singleResult())
                .map(Instance::new);
    }


    public @NotNull Optional<Instance> getHistoricInstance(@NotNull String id) {
        return Optional.ofNullable(
                        historyService
                                .createHistoricProcessInstanceQuery()
                                .includeProcessVariables()
                                .excludeSubprocesses(false)
                                .processInstanceId(id)
                                .singleResult())
                .map(Instance::new);
    }


    public @NotNull List<Task> getPendingTasks(@NotNull String businessKey) {
        return taskService
                .createTaskQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .includeIdentityLinks()
                .processInstanceBusinessKey(businessKey)
                .list()
                .stream()
                .map(Task::new)
                .peek(t -> t.setPending(true))
                .toList();
    }


    public @NotNull List<Task> getHistoricTasks(@NotNull String businessKey) {
        return historyService
                .createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .includeIdentityLinks()
                .processInstanceBusinessKey(businessKey)
                // FIXME : The withChildren should work, but doesn't.
                // So we have to go with the businessKey.
                // Maybe set another NativeQuery ?
                // .processInstanceIdWithChildren(processInstance.getId())
                .list()
                .stream()
                .map(Task::new)
                .toList();
    }


    public @NotNull Optional<ProcessDefinition> getProcessDefinition(@NotNull String deploymentKey, String tenantId) {

        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();

        if (isNotEmpty(tenantId)) {
            query.processDefinitionTenantId(tenantId);
        }

        return Optional.ofNullable(
                query.processDefinitionKey(deploymentKey)
                        .latestVersion()
                        .singleResult()
        );
    }


    public @NotNull Map<Long, List<Task>> getDefinitionTasks(@Nullable String processDefinitionId, long workflowIndex, String tenantId) {

        log.debug("getDefinitionTasks processDefId={} workflowIndex={}, tenantId={}", processDefinitionId, workflowIndex, tenantId);

        if (isEmpty(processDefinitionId)) {
            return emptyMap();
        }

        // Fetching the process definition
        // to scrape the future steps
        BpmnModel modelDefinition = getBpmnModel(processDefinitionId, tenantId);

        // Retrieving the "definition" of the process
        // Maybe some day Flowable will support it natively, until then...
        AtomicLong fallbackId = new AtomicLong(-1L);
        Map<Long, List<Task>> result = new HashMap<>();

        modelDefinition.getMainProcess()
                .getFlowElements()
                .stream()
                // That's an ugly cast, but not as ugly as storing a metadata elsewhere.
                // So... Meh !
                .filter(e -> (e instanceof CallActivity))
                .map(e -> (CallActivity) e)
                .forEach(c -> {
                    Task task = new Task(c);
                    // Adding unknown tasks too, with a default ID.
                    // Just in case someone finds the way to do a mixed internal/external process.
                    task.setStepIndex(Optional.ofNullable(task.getStepIndex()).orElseGet(fallbackId::getAndDecrement));
                    task.setWorkflowIndex(workflowIndex);
                    result.putIfAbsent(task.getStepIndex(), new ArrayList<>());
                    result.get(task.getStepIndex()).add(task);
                });

        return result;
    }


    public BpmnModel getBpmnModel(@NotNull String processDefinitionIdOrKey, String tenantId) {
        BpmnModel modelDefinition;
        try {
            modelDefinition = repositoryService.getBpmnModel(processDefinitionIdOrKey);
        } catch (FlowableObjectNotFoundException e) {
            modelDefinition = getProcessDefinition(processDefinitionIdOrKey, tenantId)
                    .map(ProcessDefinition::getId)
                    .map(repositoryService::getBpmnModel)
                    .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unknown process key/id"));
        }
        return modelDefinition;
    }


    public static Long findMetadataIndexFromActivity(CallActivity c, String metadataKey) {
        return c.getInParameters()
                .stream()
                .filter(p -> StringUtils.equals(p.getTarget(), metadataKey))
                .findFirst()
                .map(IOParameter::getSourceExpression)
                .filter(NumberUtils::isCreatable)
                .map(NumberUtils::createLong)
                .orElse(null);
    }

}
