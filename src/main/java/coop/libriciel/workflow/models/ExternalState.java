/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;
import org.jetbrains.annotations.NotNull;


@Schema(enumAsRef = true)
public enum ExternalState {

    @JsonEnumDefaultValue
    FORM,
    ACTIVE,
    SIGNED,
    REFUSED,
    EXPIRED,
    ERROR;


    static ExternalState fromTaskName(@NotNull String taskName) {
        return switch (taskName) {
            case "external_signature_waiting", "secure_mail_waiting", "ipng_waiting" -> ACTIVE;
            case "external_signature_fillingform", "secure_mail_fillingform", "ipng_fillingform" -> FORM;
            default -> null;
        };
    }

}
