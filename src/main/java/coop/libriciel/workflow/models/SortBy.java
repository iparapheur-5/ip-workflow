/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum SortBy {

    VALIDATION_START_DATE,
    @JsonEnumDefaultValue CREATION_DATE,
    STILL_SINCE_DATE,
    LATE_DATE,
    END_DATE,
    TASK_ID,
    INSTANCE_ID,
    INSTANCE_NAME,
    ACTION_TYPE,
    METADATA,
    LAST_ACTION_DATE;

    public static final String INSTANCE_NAME_VALUE = "INSTANCE_NAME";

}
