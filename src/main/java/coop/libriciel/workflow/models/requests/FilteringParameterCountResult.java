/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.models.requests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import static coop.libriciel.workflow.services.DatabaseService.SQL_INSTANCE_COUNT;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FilteringParameterCountResult extends FilteringParameter {


    private Integer count;


    public FilteringParameterCountResult(@NotNull Record record) {
        super(record);
        this.count = record.get(SQL_INSTANCE_COUNT, Integer.class);
    }


}
