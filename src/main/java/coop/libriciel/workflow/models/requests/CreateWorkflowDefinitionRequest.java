/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CreateWorkflowDefinitionRequest {

    public static final String API_DOC =
            """
            The workflow definition must be a valid BPMN, encoded in Base64.

            The Workflow file name should end with ".bpmn20.xml"

            Setting the patch will try to switch any 'visa my_desk' named task
            into a true VISA/SIGN sub-activity, with proper 'my_desk' group candidates.
            It will set up any possible UNDO operation too.
            """;

    public static final String API_EXAMPLE =
            """
            {
               "bpmnPatchNeeded": true,
               "workflowDefinition": "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGRlZmluaXRpb25zIHhtbG5zPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9CUE1OLzIw\
               MTAwNTI0L01PREVMIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIgogICAgICAgICAgICAgeG1sbnM6eHNkPSJodHRwOi8vd3d3LnczLm9\
               yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6Zmxvd2FibGU9Imh0dHA6Ly9mbG93YWJsZS5vcmcvYnBtbiIKICAgICAgICAgICAgIHhtbG5zOmJwbW5kaT0iaHR0cDovL3d3dy5vbWcub3JnL3\
               NwZWMvQlBNTi8yMDEwMDUyNC9ESSIKICAgICAgICAgICAgIHhtbG5zOm9tZ2RjPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9ERC8yMDEwMDUyNC9EQyIgeG1sbnM6b21nZGk9Imh0dHA6L\
               y93d3cub21nLm9yZy9zcGVjL0RELzIwMTAwNTI0L0RJIgogICAgICAgICAgICAgdHlwZUxhbmd1YWdlPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgZXhwcmVzc2lvbkxh\
               bmd1YWdlPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L1hQYXRoIgogICAgICAgICAgICAgdGFyZ2V0TmFtZXNwYWNlPSJodHRwOi8vd3d3LmZsb3dhYmxlLm9yZy9wcm9jZXNzZGVmIj4KICA\
               8cHJvY2VzcyBpZD0ic2ltcGxlX3dvcmtmbG93IiBuYW1lPSJTaW1wbGUgd29ya2Zsb3ciIGlzRXhlY3V0YWJsZT0idHJ1ZSI+CiAgICA8ZG9jdW1lbnRhdGlvbj5TdHJhaWdoZm9yd2FyZC\
               B2aXNhLXNpZ25hdHVyZSB3b3JrZmxvdzwvZG9jdW1lbnRhdGlvbj4KICAgIDxzdGFydEV2ZW50IGlkPSJzdGFydEV2ZW50MSIvPgogICAgPGNhbGxBY3Rpdml0eSBpZD0idmlzYV9EUkgiI\
               G5hbWU9InZpc2EgRFJIIiBmbG93YWJsZTppbmhlcml0QnVzaW5lc3NLZXk9InRydWUiLz4KICAgIDxjYWxsQWN0aXZpdHkgaWQ9InNpZ25hdHVyZV9QcmVzaWRlbnQiIG5hbWU9InNpZ25h\
               dHVyZSBQcsOpc2lkZW50IiBmbG93YWJsZTppbmhlcml0QnVzaW5lc3NLZXk9InRydWUiLz4KICAgIDxlbmRFdmVudCBpZD0ic2lkLTYwQTE0NDY4LTA0OTgtNEJBQS05RkNFLTAzQUU0M0N\
               GNDdGNyIvPgogICAgPHNlcXVlbmNlRmxvdyBpZD0ic2lkLTI0ODUwQTRDLTI3NUMtNEE0My05NjA4LUNGMzU2MjU1MzE3RiIgc291cmNlUmVmPSJzaWduYXR1cmVfUHJlc2lkZW50IgogIC\
               AgICAgICAgICAgICAgICB0YXJnZXRSZWY9InNpZC02MEExNDQ2OC0wNDk4LTRCQUEtOUZDRS0wM0FFNDNDRjQ3RjciLz4KICAgIDxzZXF1ZW5jZUZsb3cgaWQ9InNpZC1FODFFNzcyQy0xR\
               DlFLTQzM0YtOEFBRS03RjVGM0I1QzU5MjUiIHNvdXJjZVJlZj0ic3RhcnRFdmVudDEiCiAgICAgICAgICAgICAgICAgIHRhcmdldFJlZj0idmlzYV9EUkgiLz4KICAgIDxzZXF1ZW5jZUZs\
               b3cgaWQ9InNpZC1EMDVEOTA5RC05NTZFLTRBN0YtQkE1RC02MDA4RjREQzY1NDEiIHNvdXJjZVJlZj0idmlzYV9EUkgiCiAgICAgICAgICAgICAgICAgIHRhcmdldFJlZj0ic2lnbmF0dXJ\
               lX1ByZXNpZGVudCIvPgogIDwvcHJvY2Vzcz4KPC9kZWZpbml0aW9ucz4=",
               "workflowName": "simple_workflow.bpmn20.xml"
            }
            """;


    private final String workflowDefinitionBase64;
    private final String workflowName;
    private final boolean isBpmnPatchNeeded;

}
