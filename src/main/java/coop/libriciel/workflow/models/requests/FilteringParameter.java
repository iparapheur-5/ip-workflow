/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.models.requests;

import coop.libriciel.workflow.models.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import java.util.Optional;

import static coop.libriciel.workflow.services.DatabaseService.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilteringParameter {

    protected String groupId;
    protected State state;
    protected String filterMetadataKey;
    protected String filterMetadataValue;


    public FilteringParameter(@NotNull Record record) {
        this.groupId = record.get(SQL_CANDIDATE_GROUP, String.class);
        this.state = Optional.ofNullable(record.get(SQL_STATE, String.class))
                .map(String::toUpperCase)
                .map(State::valueOf)
                .orElse(null);
        this.filterMetadataKey = record.get(SQL_FILTER_BY_METADATA_KEY, String.class);
        this.filterMetadataValue = record.get(SQL_FILTER_BY_METADATA_VALUE, String.class);
    }

}
