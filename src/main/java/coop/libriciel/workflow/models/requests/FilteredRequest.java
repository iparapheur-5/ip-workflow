/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.models.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilteredRequest {

    public static final String API_DOC =
            """
            Every field is required.

            *delegationRules* : Adding some `group/metadata/value` rule.
            This will concatenate other's groups tasks to the current requested tasks list. A way to allow delegations.
            - `group02/key01/value01` : adds task from `group02` with matching `key01/value01` metadata.
            - `group02/null/null` adds every `group02` pending tasks.
            - `group02/key01/null` will be ignored
            - `group02/null/value01` will be ignored
            - `null/*/*` will be ignored
            """;
 
    private List<FilteringParameter> filteringParameters = new ArrayList<>();
    private FolderFilter folderFilter = new FolderFilter();

}
