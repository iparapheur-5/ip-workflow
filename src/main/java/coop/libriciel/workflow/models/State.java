/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import io.swagger.v3.oas.annotations.media.Schema;
import org.jetbrains.annotations.NotNull;


@Schema(enumAsRef = true)
public enum State {

    DRAFT,
    RETRIEVABLE,
    FINISHED,
    VALIDATED,
    REJECTED,
    LATE,
    PENDING,
    DOWNSTREAM;


    public static final String API_DOC_ID_VALUE = "State";
    public static final String API_PATH = "state";


    static State fromTaskName(@NotNull String taskName) {
        return switch (taskName) {
            case "workflow_internal_draft", "main_start" -> DRAFT;
            case "workflow_internal_retrievable", "undo" -> RETRIEVABLE;
            case "workflow_internal_rejected", "main_delete" -> REJECTED;
            case "workflow_internal_finished", "main_archive" -> FINISHED;
            default -> PENDING;
        };
    }


    @Override public String toString() {
        return super.toString().toLowerCase();
    }


}
