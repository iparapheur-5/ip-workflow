/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

import static coop.libriciel.workflow.models.StepDefinition.ParallelType.AND;
import static coop.libriciel.workflow.models.StepDefinition.ParallelType.OR;

@Log4j2
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepDefinition {


    @Schema(enumAsRef = true)
    public enum ParallelType {
        OR,
        AND
    }


    private @NotNull Task.Action type = Task.Action.UNKNOWN;
    private @NotNull String id = "0";
    private String name;
    private List<String> validatingDeskIds;
    private List<String> notifiedDeskIds;
    private List<String> mandatoryValidationMetadataIds;
    private List<String> mandatoryRejectionMetadataIds;
    private ParallelType parallelType = OR;


    public StepDefinition(List<Task> list) {
        log.trace("StepDefinition from List<Task> {}", list);

        if (list.size() == 0) {
            throw new IllegalStateException("Cannot build a stepDefinition from 0 tasks");
        }

        Task t = list.get(0);
        id = t.getId();
        type = t.getExpectedAction();

        validatingDeskIds = list.stream()
                .map(Task::getCandidateGroups)
                .flatMap(Collection::stream)
                .toList();

        notifiedDeskIds = list.stream()
                .map(Task::getNotifiedGroups)
                .flatMap(Collection::stream)
                .toList();

        mandatoryValidationMetadataIds = list.stream()
                .map(Task::getMandatoryValidationMetadata)
                .flatMap(Collection::stream)
                .toList();

        mandatoryRejectionMetadataIds = list.stream()
                .map(Task::getMandatoryRejectionMetadata)
                .flatMap(Collection::stream)
                .toList();

        parallelType = list.size() > 1 ? AND : OR;
        name = null;
    }


}

