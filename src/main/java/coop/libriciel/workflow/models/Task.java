/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.models;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.CollectionUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.flowable.bpmn.model.CallActivity;
import org.flowable.bpmn.model.IOParameter;
import org.flowable.task.api.TaskInfo;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Stream;

import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.State.VALIDATED;
import static coop.libriciel.workflow.models.Task.Action.START;
import static coop.libriciel.workflow.models.Visibility.CONFIDENTIAL;
import static coop.libriciel.workflow.services.DatabaseService.*;
import static coop.libriciel.workflow.utils.CollectionUtils.parseMetadata;
import static coop.libriciel.workflow.utils.CollectionUtils.popValue;
import static coop.libriciel.workflow.utils.TextUtils.*;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;


@Data
@Log4j2
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

    public static final String API_DOC_ID_VALUE = "Task id";
    public static final String API_PATH = "taskId";

    public static final String META_CURRENT_CANDIDATE_GROUPS = WORKFLOW_INTERNAL + "current_candidate_groups";
    public static final String META_PREVIOUS_CANDIDATE_GROUPS = WORKFLOW_INTERNAL + "previous_candidate_groups";
    public static final String META_PREVIOUS_ACTION = WORKFLOW_INTERNAL + "previous_action";
    public static final String META_NOTIFIED_GROUPS = WORKFLOW_INTERNAL + "notified_groups";
    public static final String META_MANDATORY_VALIDATION_METADATA = WORKFLOW_INTERNAL + "mandatory_validation_metadata";
    public static final String META_MANDATORY_REJECTION_METADATA = WORKFLOW_INTERNAL + "mandatory_rejection_metadata";
    public static final String META_STEP_INDEX = WORKFLOW_INTERNAL + "step_index";
    public static final String META_CANDIDATE_DELEGATED_BY_GROUP_ID = WORKFLOW_INTERNAL + "delegated_by_group_id";


    @Getter
    @AllArgsConstructor
    public enum NotificationType {

        ACTION_PERFORMED,
        NEW_ON_DESK,
        LATE

    }


    @Getter
    @Schema(enumAsRef = true)
    @AllArgsConstructor
    public enum Action {

        @JsonEnumDefaultValue
        VISA(I_PARAPHEUR_INTERNAL + "visa", "Visa"),
        SEAL(I_PARAPHEUR_INTERNAL + "seal", "Cachet"),
        SIGNATURE(I_PARAPHEUR_INTERNAL + "signature", "Signature"),
        PAPER_SIGNATURE(I_PARAPHEUR_INTERNAL + "paper_signature", "Signature papier"),
        EXTERNAL_SIGNATURE(I_PARAPHEUR_INTERNAL + "external_signature", "Signature externe"),
        SECURE_MAIL(I_PARAPHEUR_INTERNAL + "secure_mail", "Mail sécurisé"),
        IPNG(I_PARAPHEUR_INTERNAL + "ipng", "IPNG"), // TODO change IPNG name
        REJECT(I_PARAPHEUR_INTERNAL + "reject", "Rejet"),

        TRANSFER(I_PARAPHEUR_INTERNAL + "transfer", "Transfert"),
        SECOND_OPINION(I_PARAPHEUR_INTERNAL + "second", "Avis complémentaire"),
        ASK_SECOND_OPINION(I_PARAPHEUR_INTERNAL + "ask_second", "Demande d'avis complémentaire"),
        CREATE(I_PARAPHEUR_INTERNAL + "create", "Création"),
        START(I_PARAPHEUR_INTERNAL + "start", "Démarrer"),
        ARCHIVE(I_PARAPHEUR_INTERNAL + "archive", "Archiver"),
        DELETE(I_PARAPHEUR_INTERNAL + "delete", "Suppression"),
        BYPASS(I_PARAPHEUR_INTERNAL + "bypass", "Contournement"),
        CHAIN(I_PARAPHEUR_INTERNAL + "chain", "Enchaîner"),
        RECYCLE(I_PARAPHEUR_INTERNAL + "recycle", "Recycler"),
        IPNG_RETURN(I_PARAPHEUR_INTERNAL + "ipng_return", "Retour IPNG"), // TODO change IPNG name

        UNDO(I_PARAPHEUR_INTERNAL + "undo", "Droit de remords"),
        READ(I_PARAPHEUR_INTERNAL + "read", "Lecture"),
        UNKNOWN(I_PARAPHEUR_INTERNAL + "unknown", "Inconnue");


        private final String flowableString;
        private final String prettyPrintString;


        // <editor-fold desc="Utils">


        /**
         * It has to be a final static constant, to be used in Param annotations.
         */
        public static final String VISA_STRING_VALUE = "VISA";


        /**
         * Utility class to get the appropriate Action from task's name.
         * It may be an Action if it is taken from history, may be a Step name if it is taken from definitions...
         *
         * @param string the Task name, or the task definition
         * @return a proper action, {@link Action#VISA} if unknown
         */
        public static @NotNull Action fromString(@NotNull String string) {
            return switch (string) {
                case "i_Parapheur_internal_archive", "workflow_internal_finished", "main_archive" -> ARCHIVE;
                case "i_Parapheur_internal_bypass" -> BYPASS;
                case "i_Parapheur_internal_chain" -> CHAIN;
                case "workflow_internal_rejected", "main_delete" -> DELETE;
                case "i_Parapheur_internal_external_signature", "external_signature_fillingform", "external_signature_waiting" -> EXTERNAL_SIGNATURE;
                case "i_Parapheur_internal_ipng", "ipng_fillingform", "ipng_waiting" -> IPNG;
                case "i_Parapheur_internal_ipng_return" -> IPNG_RETURN;
                case "i_Parapheur_internal_paper_signature" -> PAPER_SIGNATURE;
                case "i_Parapheur_internal_reject" -> REJECT;
                case "i_Parapheur_internal_seal", "main_seal" -> SEAL;
                case "i_Parapheur_internal_second", "second_visa", "main_second_opinion" -> SECOND_OPINION;
                case "i_Parapheur_internal_ask_second" -> ASK_SECOND_OPINION;
                case "i_Parapheur_internal_secure_mail", "secure_mail_fillingform", "secure_mail_waiting" -> SECURE_MAIL;
                case "i_Parapheur_internal_signature", "main_signature" -> SIGNATURE;
                case "i_Parapheur_internal_start", "draft", "workflow_internal_draft", "main_start" -> START;
                case "i_Parapheur_internal_transfer" -> TRANSFER;
                case "i_Parapheur_internal_undo", "undo" -> UNDO;
                case "i_Parapheur_internal_read", "read" -> READ;
                case "i_Parapheur_internal_visa", "main_visa" -> VISA;
                default -> {
                    log.warn("Unknown action : {}", string);
                    yield UNKNOWN;
                }
            };
        }


        // </editor-fold desc="Utils">

    }


    private String id;
    private String legacyId;
    private String instanceId;
    private String instanceName;
    private Action expectedAction;
    private Action performedAction;
    private NotificationType notificationType;
    private Date beginDate;
    private Date dueDate;
    private Date date;
    private List<String> candidateGroups;
    private List<String> notifiedGroups;
    private List<String> mandatoryValidationMetadata;
    private List<String> mandatoryRejectionMetadata;
    private String assignee;
    private String delegatedByGroupId;
    private @Builder.Default boolean pending = false;
    private @Builder.Default Map<String, String> variables = new HashMap<>();
    private @Builder.Default ExternalState externalState = null;
    private @Builder.Default State state = null;
    private Set<String> readByUserIds;
    private Long workflowIndex;
    private Long stepIndex;
    private Visibility visibility;


    // <editor-fold desc="Constructors">


    public Task(@NotNull TaskInfo task) {
        log.debug("Task from TaskInfo name:{} id:{} name:{} instanceId:{}", task.getName(), task.getId(), task.getName(), task.getProcessInstanceId());

        id = task.getId();
        expectedAction = Action.fromString(task.getName());
        beginDate = task.getCreateTime();
        dueDate = task.getDueDate();
        assignee = task.getAssignee();

        externalState = ExternalState.fromTaskName(task.getName());
        state = State.fromTaskName(task.getName());

        // Populate metadata

        variables = new HashMap<>(Stream
                .concat(task.getProcessVariables().entrySet().stream(), task.getTaskLocalVariables().entrySet().stream())
                .filter(entry -> entry.getValue() != null)
                .collect(toMap(
                        Entry::getKey,
                        entry -> entry.getValue().toString())
                )
        );

        // Parse inner metadata values

        legacyId = popValue(variables, META_LEGACY_ID);

        visibility = Optional.ofNullable(popValue(variables, META_VISIBILITY))
                .map(Visibility::valueOf)
                .orElse(CONFIDENTIAL);

        Optional.ofNullable(popValue(variables, META_DUE_DATE))
                .ifPresent(dateTimeString -> dueDate = fromIso8601String(dateTimeString));

        candidateGroups = CollectionUtils.toCandidateGroupIds(task.getIdentityLinks());

        notifiedGroups = Optional.ofNullable(popValue(variables, META_NOTIFIED_GROUPS))
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        mandatoryValidationMetadata = Optional.ofNullable(popValue(variables, META_MANDATORY_VALIDATION_METADATA))
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        mandatoryRejectionMetadata = Optional.ofNullable(popValue(variables, META_MANDATORY_REJECTION_METADATA))
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        delegatedByGroupId = Optional.ofNullable(popValue(variables, META_CANDIDATE_DELEGATED_BY_GROUP_ID))
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .orElse(null);

        workflowIndex = switch (expectedAction) {
            case START -> 0L;
            case ARCHIVE, DELETE -> NumberUtils.createLong(String.valueOf(MAX_VALUE));
            default -> Optional.ofNullable(popValue(variables, META_WORKFLOW_INDEX))
                    .filter(NumberUtils::isCreatable)
                    .map(NumberUtils::createLong)
                    .orElse(null);
        };

        stepIndex = switch (expectedAction) {
            case START, ARCHIVE, DELETE -> 0L;
            default -> Optional.ofNullable(popValue(variables, META_STEP_INDEX))
                    .filter(NumberUtils::isCreatable)
                    .map(NumberUtils::createLong)
                    .orElse(null);
        };
    }


    public Task(@NotNull HistoricTaskInstance task) {
        this((TaskInfo) task);
        log.debug("Task from HistoricTaskInstance name:{} id:{} name:{}", task.getName(), task.getId(), task.getName());

        beginDate = task.getTime();
        date = task.getEndTime();
        pending = (beginDate != null) && (date == null);

        if (task.getName().equals("read") && Objects.nonNull(date)) {
            state = VALIDATED;
        }

        // TODO : This seems like a weird case fix, we should check if a task action is not registered in a wrong scope.
        //  The issue is easily fixable, though.
        performedAction = (date == null)
                ? null :
                (expectedAction == START)
                        ? START
                        : Optional.ofNullable(popValue(variables, "action")).map(Action::fromString).orElse(null);
    }


    public Task(@NotNull Record record) {
        log.trace("Task from Record record:{}", record);

        id = record.get(SQL_TASK_ID, String.class);
        expectedAction = Action.fromString(record.get(SQL_TASK_NAME, String.class));
        dueDate = record.get(SQL_DUE_DATE, Date.class);
        beginDate = record.get(SQL_BEGIN_DATE, Date.class);
        assignee = null;
        instanceId = record.get(SQL_INSTANCE_ID, String.class);
        instanceName = record.get(SQL_INSTANCE_NAME, String.class);
        externalState = ExternalState.fromTaskName(record.get(SQL_TASK_NAME, String.class));
        state = State.fromTaskName(record.get(SQL_TASK_NAME, String.class));
        if (StringUtils.equals(record.get(SQL_TASK_NAME, String.class), "read") && Objects.nonNull(date)) {
            state = VALIDATED;
        }

        variables = parseMetadata(record.get(SQL_VARIABLES, String[][].class));

        legacyId = popValue(variables, META_LEGACY_ID);

        visibility = Optional.ofNullable(popValue(variables, META_VISIBILITY))
                .map(Visibility::valueOf)
                .orElse(CONFIDENTIAL);

        Optional.ofNullable(popValue(variables, META_DUE_DATE))
                .ifPresent(dateTimeString -> dueDate = fromIso8601String(dateTimeString));
        String candidateGroupsString = record.get(SQL_CANDIDATE_GROUP, String.class);

        candidateGroups = asList(candidateGroupsString.split(","));

        readByUserIds = Optional.ofNullable(record.get(SQL_READ_BY, String[].class))
                .map(Arrays::asList)
                .orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty) // TODO : Filter that directly in the SQL
                .collect(toSet());

        delegatedByGroupId = Optional.ofNullable(popValue(variables, META_CANDIDATE_DELEGATED_BY_GROUP_ID))
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .orElse(null);
    }


    public Task(@NotNull CallActivity callActivity) {
        log.trace("Task from CallActivity name:{} id:{}", callActivity.getName(), callActivity.getId());

        expectedAction = Action.fromString(callActivity.getCalledElement());

        candidateGroups = callActivity.getInParameters()
                .stream()
                .filter(p -> StringUtils.equals(p.getTarget(), META_CURRENT_CANDIDATE_GROUPS))
                .findFirst()
                .map(IOParameter::getSourceExpression)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        notifiedGroups = callActivity.getInParameters()
                .stream()
                .filter(p -> StringUtils.equals(p.getTarget(), META_NOTIFIED_GROUPS))
                .findFirst()
                .map(IOParameter::getSourceExpression)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        mandatoryValidationMetadata = callActivity.getInParameters()
                .stream()
                .filter(p -> StringUtils.equals(p.getTarget(), META_MANDATORY_VALIDATION_METADATA))
                .findFirst()
                .map(IOParameter::getSourceExpression)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        mandatoryRejectionMetadata = callActivity.getInParameters()
                .stream()
                .filter(p -> StringUtils.equals(p.getTarget(), META_MANDATORY_REJECTION_METADATA))
                .findFirst()
                .map(IOParameter::getSourceExpression)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());

        workflowIndex = FlowableService.findMetadataIndexFromActivity(callActivity, META_WORKFLOW_INDEX);
        stepIndex = FlowableService.findMetadataIndexFromActivity(callActivity, META_STEP_INDEX);
    }


    // </editor-fold desc="Constructors">


}
