/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static coop.libriciel.workflow.utils.TextUtils.I_PARAPHEUR_INTERNAL;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

@Log4j2
@Data
@NoArgsConstructor
public class WorkflowDefinition {


    public static final String API_DOC_ID_VALUE = "Workflow definition Id";
    public static final String API_ID_PATH = "definitionId";
    public static final String API_DOC_KEY_VALUE = "Workflow definition key";
    public static final String API_KEY_PATH = "definitionKey";
    public static final String API_DOC_DEPLOYMENT_ID_VALUE = "Workflow definition deployment Id";
    public static final String API_DEPLOYMENT_ID_PATH = "definitionDeploymentId";

    public static String FINAL_DESK_ATTR_NAME = I_PARAPHEUR_INTERNAL + "final_desk_attr";
    public static String FINAL_NOTIFIED_DESKS_ATTR_NAME = I_PARAPHEUR_INTERNAL + "final_notified_desks_attr";


    public enum SortBy {
        NAME, KEY, ID
    }


    @Schema(example = "example_workflow_01")
    private String id;

    @Schema(example = "example_workflow_01")
    private String key;

    @Schema(accessMode = READ_ONLY)
    private int version;

    @Schema(accessMode = READ_ONLY)
    private String deploymentId;

    @Schema(description =
            """
            We're assuming that we're counting every usage, past, pending or upcoming here.
            Special case : If we're using a single workflow twice, as creation and validation, it will count as 2 usages, even if there is only one folder.
            """
    )
    private long usageCount;

    @Schema(example = "Example workflow")
    private String name;

    @Schema(accessMode = READ_ONLY)
    private String category = null;

    @Schema(accessMode = READ_ONLY)
    private boolean isSuspended = false;

    @Schema(example =
            """
            [
                { "id":"id01", "type": "VISA", "validatingDeskIds": ["group_01_id"], "parallelType": "OR" },
                { "id":"id02", "type": "SIGNATURE", "validatingDeskIds": ["group_02_id"], "parallelType": "OR" }
            ]
            """)
    private List<StepDefinition> steps;

    private String finalDeskId;
    private List<String> finalNotifiedDeskIds;


    // <editor-fold desc="Constructors">


    public WorkflowDefinition(@NotNull Deployment deployment) {
        log.debug("Definition from Deployment:{}", deployment);

        id = deployment.getId();
        key = deployment.getKey();
        name = deployment.getName();
        category = deployment.getCategory();
        version = -1;
        isSuspended = false;

        deploymentId = deployment.getParentDeploymentId();
        usageCount = -1;
        steps = null;
    }


    public WorkflowDefinition(@NotNull ProcessDefinition processDefinition) {
        log.debug("Definition from ProcessDefinition:{}", processDefinition);

        id = processDefinition.getId();
        key = processDefinition.getKey();
        name = processDefinition.getName();
        category = processDefinition.getCategory();
        version = processDefinition.getVersion();
        isSuspended = processDefinition.isSuspended();

        deploymentId = processDefinition.getDeploymentId();
        usageCount = -1;
        steps = null;
    }


    // </editor-fold desc="Constructors">

}
