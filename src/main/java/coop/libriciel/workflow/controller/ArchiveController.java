/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.SortBy;
import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.flowable.engine.impl.HistoricProcessInstanceQueryProperty;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.LongSupplier;

import static coop.libriciel.workflow.models.SortBy.INSTANCE_NAME_VALUE;
import static coop.libriciel.workflow.utils.ApiUtils.*;
import static java.util.Collections.emptyList;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Log4j2
@RestController
@RequestMapping("archive")
@Tag(name = "archive", description = "Archive operations")
public class ArchiveController {


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;
    private final HistoryService historyService;


    @Autowired
    public ArchiveController(FlowableService flowableService, HistoryService historyService) {
        this.flowableService = flowableService;
        this.historyService = historyService;
    }


    // </editor-fold desc="Beans">


    @GetMapping
    @Operation(description = "Get instance list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Instance> getHistoricInstances(@RequestParam String tenantId,
                                                                 @RequestParam(required = false, defaultValue = "0") int page,
                                                                 @RequestParam(required = false, defaultValue = "50") int pageSize,
                                                                 @RequestParam(required = false, defaultValue = INSTANCE_NAME_VALUE) SortBy sortBy,
                                                                 @RequestParam(required = false, defaultValue = "true") boolean asc,
                                                                 @RequestParam(required = false) Long stillSinceDate) {
        log.debug("getHistoricInstances tenantId:{} sortBy:{} asc:{}", tenantId, sortBy, asc);

        HistoricProcessInstanceQuery query = historyService
                .createHistoricProcessInstanceQuery()
                .includeProcessVariables()
                .excludeSubprocesses(true)
                .processInstanceTenantId(tenantId)
                .finished();

        switch (sortBy) {
            case INSTANCE_NAME -> query.orderBy(new HistoricProcessInstanceQueryProperty("RES.NAME_"));
            case CREATION_DATE -> query.orderByProcessInstanceStartTime();
            case END_DATE -> query.orderByProcessInstanceEndTime();
            default -> query.orderByProcessInstanceId();
        }

        if (asc) {
            query.asc();
        } else {
            query.desc();
        }

        Date parsedStillSinceDate = Optional.ofNullable(stillSinceDate).map(Date::new).orElse(null);

        List<Instance> instances = query
                .finishedBefore(parsedStillSinceDate)
                .listPage(page * pageSize, pageSize)
                .stream()
                .map(Instance::new)
                .toList();

        LongSupplier totalSupplier = () -> historyService
                .createHistoricProcessInstanceQuery()
                .processInstanceTenantId(tenantId)
                .finishedBefore(parsedStillSinceDate)
                .excludeSubprocesses(true)
                .count();

        return new PaginatedList<>(instances, page, pageSize, totalSupplier);
    }


    @GetMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Get instance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull Instance getHistoricInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Instance.API_PATH) String id) {
        log.debug("getHistoricInstance id:{}", id);

        Instance result = flowableService.getRuntimeInstance(id)
                .or(() -> flowableService.getHistoricInstance(id))
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        log.debug("getInstance metadata:{}", result.getVariables());
        log.debug("getInstance businessKey:{}", result.getBusinessKey());

        // Merge everything into one single map, build and send back the result
        result.setTaskList(emptyList());

        log.debug("getInstance taskSize:{} result:{}", result.getTaskList().size(), result);
        return result;
    }


    @DeleteMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Delete instance")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteHistoricInstance(@Parameter(description = "Instance Id")
                                       @PathVariable(name = Instance.API_PATH) String id) {

        log.debug("deleteHistoricInstance id:{}", id);

        try {
            historyService.createHistoricProcessInstanceQuery().processInstanceId(id).deleteWithRelatedData();
        } catch (FlowableObjectNotFoundException | NullPointerException e) {
            log.trace(e.getMessage());
            throw new ResponseStatusException(NOT_FOUND, "Nothing to delete with this id");
        }
    }


}
