/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.Instance;
import coop.libriciel.workflow.models.SortBy;
import coop.libriciel.workflow.models.State;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.requests.EditFolderRequest;
import coop.libriciel.workflow.models.requests.FilteringParameter;
import coop.libriciel.workflow.services.DatabaseService;
import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.CollectionUtils;
import coop.libriciel.workflow.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URLDecoder;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.LongSupplier;

import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.SortBy.INSTANCE_NAME_VALUE;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.Visibility.CONFIDENTIAL;
import static coop.libriciel.workflow.utils.ApiUtils.*;
import static coop.libriciel.workflow.utils.CollectionUtils.safeAdd;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static coop.libriciel.workflow.utils.TextUtils.toIso8601String;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.*;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpStatus.*;

@Log4j2
@RestController
@RequestMapping("instance")
@Tag(name = "instance", description = "Instance operations")
public class InstanceController {


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;
    private final HistoryService historyService;
    private final RuntimeService runtimeService;
    private final DatabaseService databaseService;


    @Autowired
    public InstanceController(FlowableService flowableService, RuntimeService runtimeService, HistoryService historyService, DatabaseService databaseService) {
        this.flowableService = flowableService;
        this.historyService = historyService;
        this.runtimeService = runtimeService;
        this.databaseService = databaseService;
    }


    // </editor-fold desc="Beans">


    @PostMapping
    @Operation(description = "Create new instance")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull Instance createInstance(@Parameter(name = "body")
                                            @RequestBody Instance instance) {

        log.info("createInstance tenantId:{} key:{} dueDate:{} creationW:{} validationW:{} variables:{}",
                instance.getTenantId(), instance.getDeploymentKey(), instance.getDueDate(), instance.getCreationWorkflowId(),
                instance.getValidationWorkflowId(), instance.getVariables());

        if (isEmpty(instance.getValidationWorkflowId())) {
            throw new ResponseStatusException(BAD_REQUEST, "ValidationWorkflow needed");
        }

        if (instance.getVariables().keySet().stream().anyMatch(var -> var.startsWith(WORKFLOW_INTERNAL))) {
            throw new ResponseStatusException(BAD_REQUEST, "Instance variables starting with `" + WORKFLOW_INTERNAL + "` are reserved");
        }

        ProcessDefinition processDefinition = flowableService
                .getProcessDefinition(instance.getDeploymentKey(), null)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given deployment key is unknown"));

        HashMap<String, Object> variables = new HashMap<>(instance.getVariables());
        Optional.ofNullable(instance.getDueDate()).ifPresent(d -> variables.put(META_DUE_DATE, toIso8601String(d)));
        Optional.ofNullable(instance.getCreationWorkflowId()).ifPresent(w -> variables.put(META_CREATION_WORKFLOW_ID, w));

        Optional.ofNullable(instance.getVisibility())
                .filter(visibility -> visibility != CONFIDENTIAL)
                .map(Enum::toString)
                .ifPresent(visibilityString -> variables.put(META_VISIBILITY, visibilityString));

        variables.put(META_VALIDATION_WORKFLOW_ID, instance.getValidationWorkflowId());
        variables.put(META_ORIGIN_GROUP_ID, instance.getOriginGroup());
        variables.put(META_FINAL_GROUP_ID, instance.getFinalGroup());
        variables.put(META_FINAL_NOTIFIED_GROUPS, instance.getFinalNotifiedGroups());
        variables.put(META_STEPS, instance.getTaskList());
        variables.put(META_LEGACY_ID, instance.getVariables().get(META_LEGACY_ID));
        variables.put(META_CREATION_DATE, toIso8601String(Date.from(Instant.now())));

        ProcessInstance processInstance = runtimeService
                .createProcessInstanceBuilder()
                .processDefinitionKey(processDefinition.getKey())
                .name(instance.getName())
                .businessKey(instance.getBusinessKey())
                .variables(variables)
                .overrideProcessDefinitionTenantId(instance.getTenantId())
                .start();

        instance.setId(processInstance.getId());
        instance.setStartTime(processInstance.getStartTime());

        return instance;
    }


    @NotNull private PaginatedList<Task> searchInstancesWithParams(String tenantId,
                                                                   int page,
                                                                   int pageSize,
                                                                   SortBy sortBy,
                                                                   boolean asc,
                                                                   String typeId,
                                                                   String subtypeId,
                                                                   String legacyId,
                                                                   String searchTerm,
                                                                   Long emitBeforeTime,
                                                                   Long stillSinceTime,
                                                                   State state,
                                                                   List<FilteringParameter> filteringParams) {

        List<FilteringParameter> mutableFilteringParams = new ArrayList<>(filteringParams);

        if (StringUtils.isNotEmpty(legacyId)) {
            mutableFilteringParams.add(new FilteringParameter(null, null, META_LEGACY_ID, legacyId));
        }

        if (mutableFilteringParams.isEmpty()) {
            mutableFilteringParams = null;
        }

        String decodedSearchTerm = Optional.ofNullable(searchTerm)
                .map(value -> URLDecoder.decode(value, UTF_8))
                .orElse(null);

        List<Task> instances = databaseService
                .getTasks(
                        tenantId, mutableFilteringParams, state, sortBy.name(), asc, page, pageSize,
                        typeId, subtypeId, decodedSearchTerm, null, emitBeforeTime, stillSinceTime
                );

        LongSupplier totalSupplier = () -> databaseService
                .countTasks(tenantId, null, state, typeId, subtypeId, searchTerm, null, emitBeforeTime, stillSinceTime);

        return new PaginatedList<>(instances, page, pageSize, totalSupplier);
    }


    public record InstanceSearchRequest(List<String> groupIds,
                                        String typeId,
                                        String subtypeId,
                                        String legacyId,
                                        String searchTerm,
                                        Long emitBeforeTime,
                                        Long stillSinceTime,
                                        State state) {}


    @PostMapping("search")
    @Operation(description = "Get instance list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Task> searchInstances(@RequestParam String tenantId,
                                                        @RequestParam(required = false, defaultValue = "0") int page,
                                                        @RequestParam(required = false, defaultValue = "50") int pageSize,
                                                        @RequestParam(required = false, defaultValue = INSTANCE_NAME_VALUE) SortBy sortBy,
                                                        @RequestParam(required = false, defaultValue = "true") boolean asc,
                                                        @RequestBody InstanceSearchRequest request) {

        log.debug("searchInstances group:{} state:{}", request.groupIds, request.state);

        List<FilteringParameter> filteringParams = Optional.ofNullable(request.groupIds)
                .stream()
                .flatMap(Collection::stream)
                .filter(StringUtils::isNotEmpty)
                .map(grpId -> new FilteringParameter(grpId, null, null, null))
                .toList();

        return searchInstancesWithParams(tenantId, page, pageSize, sortBy, asc, request.typeId, request.subtypeId, request.legacyId, request.searchTerm, request.emitBeforeTime, request.stillSinceTime, request.state, filteringParams);

    }

    @GetMapping
    @Operation(description = "Get instance list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Task> getInstances(@RequestParam String tenantId,
                                                     @RequestParam(required = false, defaultValue = "0") int page,
                                                     @RequestParam(required = false, defaultValue = "50") int pageSize,
                                                     @RequestParam(required = false, defaultValue = INSTANCE_NAME_VALUE) SortBy sortBy,
                                                     @RequestParam(required = false, defaultValue = "true") boolean asc,
                                                     @RequestParam(required = false) String groupId,
                                                     @RequestParam(required = false) String typeId,
                                                     @RequestParam(required = false) String subtypeId,
                                                     @RequestParam(required = false) String legacyId,
                                                     @RequestParam(required = false) String searchTerm,
                                                     @RequestParam(required = false) Long emitBeforeTime,
                                                     @RequestParam(required = false) Long stillSinceTime,
                                                     @RequestParam(required = false) State state) {

        log.debug("getInstances group:{} state:{}", groupId, state);

        List<FilteringParameter> filteringParams = new ArrayList<>(Optional.ofNullable(groupId)
                .filter(StringUtils::isNotEmpty)
                .map(grpId -> new FilteringParameter(grpId, null, null, null))
                .map(Collections::singletonList)
                .orElse(emptyList()));

        return searchInstancesWithParams(tenantId, page, pageSize, sortBy, asc, typeId, subtypeId, legacyId, searchTerm, emitBeforeTime, stillSinceTime, state, filteringParams);
    }


    @GetMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Get instance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull Instance getInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                         @PathVariable(name = Instance.API_PATH) String id,
                                         @RequestParam(defaultValue = "false") boolean withHistory,
                                         @RequestParam(required = false) String tenantId) {

        log.debug("getInstance id:{} withHistory:{}", id, withHistory);

        Instance result = flowableService.getRuntimeInstance(id)
                .or(() -> flowableService.getHistoricInstance(id))
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        log.debug("getInstance metadata:{}", result.getVariables());
        log.debug("getInstance businessKey:{}", result.getBusinessKey());

        // We have 3 types of tasks : The theoretical (future) ones, the historic (past) ones, and the pending (present) ones.
        // Fetching tasks one by one, sometimes we'll want to add a task to an existing list...
        // Building everything into 3 separate maps, and merging those at the end is way easier.
        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap = new TreeMap<>();
        TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap = new TreeMap<>();

        AtomicBoolean inEndOfWorkflow = new AtomicBoolean(false);
        if (result.getEndTime() == null) {
            List<Task> pendingtasks = flowableService.getPendingTasks(result.getBusinessKey());
            inEndOfWorkflow.set(
                    pendingtasks.stream().anyMatch(task -> task.getExpectedAction() == ARCHIVE)
            );

            pendingtasks.forEach(t -> safeAdd(pendingTasksMap, Pair.of(t.getWorkflowIndex(), t.getStepIndex()), t));
        } else {
            inEndOfWorkflow.set(true);
        }


        if (withHistory) {

            // Retrieving history

            flowableService.getHistoryService()
                    .createHistoricTaskInstanceQuery()
                    .processInstanceBusinessKey(result.getBusinessKey())
                    .includeProcessVariables()
                    .includeTaskLocalVariables()
                    .includeIdentityLinks()
                    .list()
                    .stream()
                    .map(Task::new)
                    // We don't want current tasks here, they'll be retrieved with appropriate permissions later
                    .filter(t -> t.getDate() != null)
                    .forEach(t -> safeAdd(historyTasksMap, Pair.of(t.getWorkflowIndex(), t.getStepIndex()), t));

            // Retrieving the original definition.
            // Useless if the process is already ended, thus every task will be in the historic ones.
            if (!inEndOfWorkflow.get()) {

                // Building up the trivial start and end steps definitions.
                safeAdd(definitionTasksMap,
                        Pair.of(0L, 0L), Task.builder()
                                .candidateGroups(singletonList(result.getOriginGroup()))
                                .expectedAction(START)
                                .build()
                );

                long loopCount = Long.parseLong(result.getVariables().getOrDefault(META_LOOP_COUNT, "0"));
                safeAdd(definitionTasksMap,
                        Pair.of((long) MAX_VALUE + loopCount, 0L), Task.builder()
                                .candidateGroups(singletonList(result.getFinalGroup()))
                                .notifiedGroups(result.getFinalNotifiedGroups())
                                .expectedAction(ARCHIVE)
                                .build()
                );

                // If any task has been performed in an index > 1, then the creation workflow is already finished.
                // Everything is in history, the definition would be useless.
                boolean hasCreationWorkflow = StringUtils.isNotEmpty(result.getCreationWorkflowId());
                boolean isNotInValidationAlready = historyTasksMap.keySet().stream()
                        .map(Pair::getLeft)
                        .filter(Objects::nonNull)
                        .noneMatch(i -> i > 1);

                if (hasCreationWorkflow && isNotInValidationAlready) {

                    Map<Pair<Long, Long>, List<Task>> creationWorkflowDefinition = flowableService
                            .getDefinitionTasks(result.getCreationWorkflowId(), 1L, tenantId)
                            .entrySet().stream()
                            .peek(e -> e.getValue().stream().filter(Objects::nonNull).forEach(t -> t.setWorkflowIndex(1L)))
                            .collect(toMap(e -> Pair.of(1L, e.getKey()), Map.Entry::getValue));

                    definitionTasksMap.putAll(creationWorkflowDefinition);
                }

                // If any chain was performed, we'll only want the last workflow's definition.
                // Every other would have been finished, entirely in history too.
                long validationWorkflowIndex = 2L + loopCount;

                Map<Pair<Long, Long>, List<Task>> validationWorkflowDefinition = flowableService
                        .getDefinitionTasks(result.getValidationWorkflowId(), validationWorkflowIndex, tenantId)
                        .entrySet().stream()
                        .peek(e -> e.getValue().stream().filter(Objects::nonNull).forEach(t -> t.setWorkflowIndex(validationWorkflowIndex)))
                        .collect(toMap(e -> Pair.of(validationWorkflowIndex, e.getKey()), Map.Entry::getValue));

                definitionTasksMap.putAll(validationWorkflowDefinition);
                definitionTasksMap.forEach((key, value) -> log.debug("      modelTask : {} {}", key, value));
            }
        }


        // Merge everything into one single map, build and send back the result
        result.setTaskList(CollectionUtils.mergeTaskLists(definitionTasksMap, historyTasksMap, pendingTasksMap));

        log.debug("getInstance taskSize:{} result:{}", result.getTaskList().size(), result);
        return result;
    }


    @PutMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Edit instance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void editInstanceMetadata(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                     @PathVariable(name = Instance.API_PATH) String id,
                                     @RequestBody EditFolderRequest request) {

        log.debug("editInstanceMetadata id:{} request:{}", id, request);

        if (request.getMetadata().keySet().stream().anyMatch(var -> var.startsWith(WORKFLOW_INTERNAL))) {
            throw new ResponseStatusException(BAD_REQUEST, "Instance variables starting with `" + WORKFLOW_INTERNAL + "` are reserved");
        }

        // Actual edit

        Optional.ofNullable(request.getName())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(n -> runtimeService.setProcessInstanceName(id, n));

        runtimeService.setVariables(id, request.getMetadata());
    }


    @PutMapping("{" + Instance.API_PATH + "}/changeCreationWorkflow")
    @Operation(description = "Edit instance creation workflow definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void editInstanceCreationWorkflowDefinition(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Instance.API_PATH) String instanceId,
                                                       @RequestBody Instance instance) {
        runtimeService.setVariables(
                instanceId,
                singletonMap(META_CREATION_WORKFLOW_ID, instance.getCreationWorkflowId())
        );
    }


    @PutMapping("{" + Instance.API_PATH + "}/changeValidationWorkflow")
    @Operation(description = "Edit instance validation workflow definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void editInstanceValidationWorkflowDefinition(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Instance.API_PATH) String instanceId,
                                                         @RequestBody Instance instance) {
        runtimeService.setVariables(
                instanceId,
                Map.of(
                        META_VALIDATION_WORKFLOW_ID, instance.getValidationWorkflowId(),
                        META_FINAL_GROUP_ID, instance.getFinalGroup()
                )
        );
    }


    @DeleteMapping("{" + Instance.API_PATH + "}")
    @Operation(description = "Delete instance")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteInstance(@Parameter(description = Instance.API_DOC_ID_VALUE)
                               @PathVariable(name = Instance.API_PATH) String id) {

        log.debug("deleteWorkflow id:{}", id);
        boolean hasDeletedSomething = false;

        // The instance id may be in any repository.
        // We have to trigger every delete.

        try {
            runtimeService.deleteProcessInstance(id, "Cleanup");
            hasDeletedSomething = true;
        } catch (FlowableObjectNotFoundException | NullPointerException e) {log.trace(e.getMessage());}

        try {
            historyService.createHistoricProcessInstanceQuery().processInstanceId(id).deleteWithRelatedData();
            hasDeletedSomething = true;
        } catch (FlowableObjectNotFoundException | NullPointerException e) {log.trace(e.getMessage());}

        // Integrity check

        if (!hasDeletedSomething) {
            throw new ResponseStatusException(NOT_FOUND, "Nothing to delete with this id");
        }
    }


    @GetMapping("{" + Instance.API_PATH + "}/readTasks")
    @Operation(description = "Get an un-filtered read task list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull List<Task> getReadTasks(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                            @PathVariable(name = Instance.API_PATH) String id) {
        log.debug("getReadTasks id:{}", id);

        String businessKey = flowableService.getHistoricInstance(id)
                .map(Instance::getBusinessKey)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        return flowableService.getHistoryService()
                .createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .includeIdentityLinks()
                .processInstanceBusinessKey(businessKey)
                .taskName("read")
                .list()
                .stream()
                .map(Task::new)
                .toList();
    }


    @GetMapping("{" + Instance.API_PATH + "}/historyTasks")
    @Operation(description = "Get an un-filtered flattened event list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public @NotNull List<Task> getHistoryTasks(@Parameter(description = Instance.API_DOC_ID_VALUE)
                                               @PathVariable(name = Instance.API_PATH) String id) {
        log.debug("historyTasks id:{}", id);

        String businessKey = flowableService.getHistoricInstance(id)
                .map(Instance::getBusinessKey)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "The given instance id is unknown"));

        log.debug("historyTasks businessKey:{}", businessKey);
        List<Task> result = flowableService.getHistoricTasks(businessKey);

        // Cleanup

        result.stream()
                // Optional tasks should not inherit main metadata.
                .filter(task -> (task.getExpectedAction() == READ) || (task.getExpectedAction() == UNDO))
                .forEach(task -> task.setVariables(emptyMap()));

        // Sending back results

        log.debug("historyTasks result:");
        result.forEach(t -> log.debug("  - id:{} user:{} date:{} state:{}", t.getId(), t.getAssignee(), t.getDate(), t.getState()));
        return result;
    }


}
