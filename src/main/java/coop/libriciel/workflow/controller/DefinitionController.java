/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.StepDefinition;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.WorkflowDefinition;
import coop.libriciel.workflow.models.requests.FilteringParameter;
import coop.libriciel.workflow.models.requests.FilteringParameterCountResult;
import coop.libriciel.workflow.services.DatabaseService;
import coop.libriciel.workflow.services.FlowableService;
import coop.libriciel.workflow.utils.ApiUtils;
import coop.libriciel.workflow.utils.PaginatedList;
import coop.libriciel.workflow.utils.XmlUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.*;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.LongSupplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.StepDefinition.ParallelType.OR;
import static coop.libriciel.workflow.models.Task.*;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.utils.ApiUtils.*;
import static coop.libriciel.workflow.utils.XmlUtils.OUTPUTKEYS_INDENT_AMOUNT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.Locale.ROOT;
import static java.util.regex.Pattern.MULTILINE;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;
import static javax.xml.transform.OutputKeys.*;
import static javax.xml.xpath.XPathConstants.NODESET;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.join;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping("definitions")
@Tag(name = "definition", description = "Definition operations")
public class DefinitionController {


    static final Comparator<Triple<Integer, Integer, Integer>> VERSION_NUMBER_COMPARATOR =
            comparing(Triple<Integer, Integer, Integer>::getLeft)
                    .thenComparing(Triple::getMiddle)
                    .thenComparing(Triple::getRight);


    // <editor-fold desc="Beans">


    private final FlowableService flowableService;
    private final RepositoryService repositoryService;
    private final DatabaseService databaseService;


    @Autowired
    public DefinitionController(FlowableService flowableService, RepositoryService repositoryService, DatabaseService databaseService) {
        this.flowableService = flowableService;
        this.repositoryService = repositoryService;
        this.databaseService = databaseService;
    }


    @PostConstruct
    public void init() {
        this.setupIParapheurDefinitions();
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Utils">


    public static @NotNull String patchBpmn(ClassLoader loader, InputStream bpmnInputStream) throws TransformerException {

        SAXTransformerFactory stf = (SAXTransformerFactory) TransformerFactory.newInstance();
        stf.setFeature(FEATURE_SECURE_PROCESSING, true);

        TransformerHandler th1 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/add_flowable_xmlns.xslt")));
        TransformerHandler th2 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/set_process_executable.xslt")));
        TransformerHandler th3 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_attributes.xslt")));
        TransformerHandler th4 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_candidates.xslt")));
        TransformerHandler th5 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_undo.xslt")));
        TransformerHandler th6 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/one_liner.xslt")));

        // There are some differences between OracleJDK and OpenJDK results.
        // Removing new lines (with the one_liner.xslt, and re-indenting it makes the results unit-tests friendly.

        TransformerHandler th7 = stf.newTransformerHandler();
        th7.getTransformer().setOutputProperty(METHOD, "xml");
        th7.getTransformer().setOutputProperty(ENCODING, UTF_8.name());
        th7.getTransformer().setOutputProperty(INDENT, "yes");
        th7.getTransformer().setOutputProperty(OUTPUTKEYS_INDENT_AMOUNT, "2");

        // Building the chain of transformers, and feeding the inputStream

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        th1.setResult(new SAXResult(th2));
        th2.setResult(new SAXResult(th3));
        th3.setResult(new SAXResult(th4));
        th4.setResult(new SAXResult(th5));
        th5.setResult(new SAXResult(th6));
        th6.setResult(new SAXResult(th7));
        th7.setResult(new StreamResult(baos));

        stf.newTransformer().transform(new StreamSource(bpmnInputStream), new SAXResult(th1));

        // There are some differences between OracleJDK and OpenJDK results.
        // Canonicalization makes the results unit-tests friendly.
        String canonicalized = XmlUtils.canonicalize(baos.toByteArray());

        // Deleting line feeds, from https://bpmn.io/
        canonicalized = StringUtils.replace(canonicalized, "&#xA;\"", "\"");

        return canonicalized;
    }


    private static @NonNull CallActivity buildCallActivity(@NonNull String id, @NotNull String name, @NotNull String validator, @NotNull StepDefinition step,
                                                           @NotNull StepDefinition previousStep, @NotNull String currentStepIndex) {

        String previousValidatorsConcat = String.join(",", previousStep.getValidatingDeskIds());

        CallActivity callActivity = new CallActivity();
        callActivity.setId(id);
        callActivity.setName(name);
        callActivity.setCalledElement(step.getType().getFlowableString());
        callActivity.setInheritBusinessKey(true);
        callActivity.setInheritVariables(true);
        callActivity.setFallbackToDefaultTenant(true);
        callActivity.setInParameters(Stream
                .of(
                        buildFlowableIn(true, validator, META_CURRENT_CANDIDATE_GROUPS),
                        buildFlowableIn(true, previousValidatorsConcat, META_PREVIOUS_CANDIDATE_GROUPS),
                        buildFlowableIn(true, previousStep.getType().getFlowableString(), META_PREVIOUS_ACTION),
                        buildFlowableIn(true, currentStepIndex, META_STEP_INDEX),
                        Optional.ofNullable(step.getNotifiedDeskIds())
                                .filter(CollectionUtils::isNotEmpty)
                                .map(l -> join(l, ","))
                                .map(s -> buildFlowableIn(true, s, META_NOTIFIED_GROUPS))
                                .orElse(null),
                        Optional.ofNullable(step.getMandatoryValidationMetadataIds())
                                .filter(CollectionUtils::isNotEmpty)
                                .map(l -> join(l, ","))
                                .map(s -> buildFlowableIn(true, s, META_MANDATORY_VALIDATION_METADATA))
                                .orElse(null),
                        Optional.ofNullable(step.getMandatoryRejectionMetadataIds())
                                .filter(CollectionUtils::isNotEmpty)
                                .map(l -> join(l, ","))
                                .map(s -> buildFlowableIn(true, s, META_MANDATORY_REJECTION_METADATA))
                                .orElse(null),
                        buildFlowableIn(false, META_DUE_DATE, META_DUE_DATE),
                        buildFlowableIn(false, META_WORKFLOW_INDEX, META_WORKFLOW_INDEX)
                )
                .filter(Objects::nonNull)
                .toList()
        );

        return callActivity;
    }


    private static @NonNull SequenceFlow buildSequenceFlow(@NonNull String sourceId, @NonNull String targetId) {

        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setSourceRef(sourceId);
        sequenceFlow.setTargetRef(targetId);
        sequenceFlow.setId("sid-" + sourceId + "-" + targetId);

        return sequenceFlow;
    }


    private static @NonNull IOParameter buildFlowableIn(boolean isSourceExpression, @Nullable String source, @NonNull String target) {

        IOParameter ioParameter = new IOParameter();
        ioParameter.setTarget(target);

        if (isSourceExpression) {
            ioParameter.setSourceExpression(source);
        } else {
            ioParameter.setSource(source);
        }

        return ioParameter;
    }


    // </editor-fold desc="Utils">


    public static @NonNull BpmnModel definitionToBpmn(@NotNull WorkflowDefinition workflowDefinition) {

        String startEventId = "startEvent1";
        String endEventId = "endEvent1";

        // Build flowable objects

        Process process = new Process();
        process.setId(workflowDefinition.getId());
        process.setName(workflowDefinition.getName());
        process.setExecutable(true);
        process.setDocumentation("Straightforward visa-signature workflow");

        StartEvent startEvent = new StartEvent();
        startEvent.setId(startEventId);

        process.addFlowElement(startEvent);

        ErrorEventDefinition errorEventDefinition = new ErrorEventDefinition();
        errorEventDefinition.setErrorCode("undo");

        StepDefinition startStep = new StepDefinition(START, startEventId, START.getPrettyPrintString(), singletonList("unknown"), emptyList(), emptyList(), emptyList(), OR);
        AtomicReference<StepDefinition> previousStepAtomicRef = new AtomicReference<>(startStep);
        AtomicInteger index = new AtomicInteger(0);
        workflowDefinition.getSteps()
                .forEach(step -> {

                    StepDefinition previousStep = previousStepAtomicRef.getAndSet(step);
                    String currentStepIndex = String.valueOf(index.getAndIncrement());

                    if (step.getParallelType() == OR) {

                        // OR steps are pretty straightforward : We just have to concat group ids with commas.
                        // Seems hacky, but it is the way the BPMN file is built with the Flowable Modeler : a single string, with commas.

                        String validatorsConcat = String.join(",", step.getValidatingDeskIds());
                        CallActivity callActivity = buildCallActivity(step.getId(), step.getName(), validatorsConcat, step, previousStep, currentStepIndex);
                        process.addFlowElement(callActivity);
                        process.addFlowElement(buildSequenceFlow(previousStep.getId(), callActivity.getId()));

                        if ((previousStep.getType() == VISA)
                            && (step.getValidatingDeskIds().size() < 2)
                            && (previousStep.getValidatingDeskIds().size() < 2)) {

                            BoundaryEvent boundaryEvent = new BoundaryEvent();
                            boundaryEvent.setId(step.getId() + "_undo_catch");
                            boundaryEvent.setAttachedToRef(callActivity);
                            boundaryEvent.addEventDefinition(errorEventDefinition);

                            process.addFlowElement(boundaryEvent);
                            process.addFlowElement(buildSequenceFlow(boundaryEvent.getId(), previousStep.getId()));
                        }

                    } else {

                        // AND steps are separated UserActivities, wrapped with 2 in-out ParallelGateways.
                        // Every validator has his own separate task.

                        ParallelGateway parallelGatewayStart = new ParallelGateway();
                        parallelGatewayStart.setId(String.format("%s_start_parallel_gateway", step.getId()));

                        ParallelGateway parallelGatewayEnd = new ParallelGateway();
                        parallelGatewayEnd.setId(step.getId());

                        process.addFlowElement(parallelGatewayStart);
                        process.addFlowElement(parallelGatewayEnd);
                        process.addFlowElement(buildSequenceFlow(previousStep.getId(), parallelGatewayStart.getId()));

                        AtomicInteger validatorIndex = new AtomicInteger(0);
                        step.getValidatingDeskIds()
                                .forEach(validator -> {
                                    String callActivityId = String.format("%s_validator_%s", step.getId(), validatorIndex.getAndIncrement());
                                    String callActivityName = String.format("%s validator_%s", step.getName(), validatorIndex.get());
                                    CallActivity callActivity = buildCallActivity(
                                            callActivityId, callActivityName, validator, step, previousStep, currentStepIndex
                                    );
                                    process.addFlowElement(callActivity);
                                    process.addFlowElement(buildSequenceFlow(parallelGatewayStart.getId(), callActivity.getId()));
                                    process.addFlowElement(buildSequenceFlow(callActivity.getId(), parallelGatewayEnd.getId()));
                                });
                    }
                });

        EndEvent endEvent = new EndEvent();
        endEvent.setId(endEventId);

        process.addFlowElement(endEvent);
        process.addFlowElement(buildSequenceFlow(previousStepAtomicRef.get().getId(), endEvent.getId()));

        StringDataObject finalDeskIdValue = new StringDataObject();
        finalDeskIdValue.setId(WorkflowDefinition.FINAL_DESK_ATTR_NAME);
        finalDeskIdValue.setName(WorkflowDefinition.FINAL_DESK_ATTR_NAME);
        finalDeskIdValue.setValue(workflowDefinition.getFinalDeskId());

        ItemDefinition finalDeskIdDef = new ItemDefinition();
        finalDeskIdDef.setStructureRef("xsd:string");
        finalDeskIdValue.setItemSubjectRef(finalDeskIdDef);

        process.addFlowElement(finalDeskIdValue);

        StringDataObject finalNotifiedDesksValue = new StringDataObject();
        finalNotifiedDesksValue.setId(WorkflowDefinition.FINAL_NOTIFIED_DESKS_ATTR_NAME);
        finalNotifiedDesksValue.setName(WorkflowDefinition.FINAL_NOTIFIED_DESKS_ATTR_NAME);
        finalNotifiedDesksValue.setValue(Optional
                .ofNullable(workflowDefinition.getFinalNotifiedDeskIds())
                .map(list -> String.join(",", list))
                .orElse(EMPTY));

        ItemDefinition finalNotifiedDesksDef = new ItemDefinition();
        finalNotifiedDesksDef.setStructureRef("xsd:string");
        finalNotifiedDesksValue.setItemSubjectRef(finalNotifiedDesksDef);

        process.addFlowElement(finalNotifiedDesksValue);

        BpmnModel bpmnModel = new BpmnModel();
        bpmnModel.addProcess(process);

        return bpmnModel;
    }


    static @NotNull String getDefinitionFromBpmn(@NotNull String bpmn) {

        try (InputStream bpmnInputStream = new ByteArrayInputStream(bpmn.getBytes())) {

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(bpmnInputStream);
            NodeList nodeList = (NodeList) XPathFactory
                    .newInstance()
                    .newXPath()
                    .compile("/definitions/process/documentation")
                    .evaluate(xmlDocument, NODESET);

            return Optional.of(nodeList)
                    .filter(l -> l.getLength() > 0)
                    .map(l -> l.item(0))
                    .map(Node::getTextContent)
                    .orElse(EMPTY);

        } catch (IOException | XPathExpressionException | SAXException | ParserConfigurationException ioException) {
            log.error("Error retrieving workflow definition from bpmn : {}", ioException.getMessage());
            log.debug("full error : ", ioException);
        }

        return EMPTY;
    }


    public static @NotNull Triple<Integer, Integer, Integer> getVersionFromDefinition(@NotNull String definition) {

        Pattern pattern = Pattern.compile("^\\s*Version\\s*(?<major>\\d+)(?:\\.(?<minor>\\d+))?(?:\\.(?<patch>\\d+))?\\s*$", MULTILINE);
        Matcher matcher = pattern.matcher(definition);

        return Optional.of(matcher)
                .filter(Matcher::find)
                .map(m -> Triple.of(
                        Optional.ofNullable(m.group("major")).map(Integer::valueOf).orElse(1),
                        Optional.ofNullable(m.group("minor")).map(Integer::valueOf).orElse(0),
                        Optional.ofNullable(m.group("patch")).map(Integer::valueOf).orElse(0)
                ))
                .orElse(Triple.of(1, 0, 0));
    }


    @PostMapping("i-parapheur")
    @Operation(description = "Init i-Parapheur workflows")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public void setupIParapheurDefinitions() {
        log.info("setupIParapheurDefinitions...");

        Map<String, String> deploymentToDo = new HashMap<>();
        deploymentToDo.put(FOLDER_DEPLOYMENT_KEY, getBpmn("bpmn/folder.bpmn20.xml"));

        Stream.of(START, VISA, SEAL, SIGNATURE, DELETE, ARCHIVE, SIGNATURE, IPNG_RETURN, SECOND_OPINION)
                .forEach(simpleAction -> deploymentToDo.put(simpleAction.getFlowableString(), getSimpleActionBpmn(simpleAction)));

        Stream.of(EXTERNAL_SIGNATURE, SECURE_MAIL, IPNG)
                .forEach(externalAction -> deploymentToDo.put(externalAction.getFlowableString(), getExternalActionBpmn(externalAction)));

        deploymentToDo.entrySet()
                .stream()
                .filter(e -> {

                    // Version number is not kept in the BPMN file,
                    // and versions will be variable amongst different installations.
                    // We test the documentation's version to see if we have to upgrade the process.

                    String newDefinitionDescription = getDefinitionFromBpmn(e.getValue());
                    String oldDefinitionDescription = flowableService
                            .getProcessDefinition(e.getKey(), null)
                            .map(ProcessDefinition::getDescription)
                            .orElse(EMPTY);

                    Triple<Integer, Integer, Integer> newVersion = getVersionFromDefinition(newDefinitionDescription);
                    Triple<Integer, Integer, Integer> oldVersion = getVersionFromDefinition(oldDefinitionDescription);
                    int versionDelta = VERSION_NUMBER_COMPARATOR.compare(newVersion, oldVersion);

                    log.debug("oldDefinitionDesc:{} newDefinitionDesc:{}", oldDefinitionDescription, newDefinitionDescription);
                    return (versionDelta > 0);
                })
                .peek(e -> log.info("Deploying i-Parapheur definition : " + e.getKey()))
                .forEach(e -> repositoryService
                        .createDeployment()
                        .addString(String.format("%s.bpmn20.xml", e.getKey()), e.getValue())
                        .name(e.getKey())
                        .key(e.getKey())
                        .deploy());
    }


    @NotNull String getExternalActionBpmn(Action action) {

        if (!asList(SECURE_MAIL, EXTERNAL_SIGNATURE, IPNG).contains(action)) {
            throw new RuntimeException(String.format("The simple action template cannot be applied to the action:%s", action));
        }

        try (InputStream actionTemplateInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/external_action.bpmn20.xml");
             InputStream safeInputStream = Optional.ofNullable(actionTemplateInputStream)
                     .orElseThrow(() -> new IOException("Missing internal external_action file"))) {

            String bpmn = IOUtils.toString(safeInputStream, UTF_8);
            // A StringSubstitutor would have been nicer, but it fails to catch the CDATA content.
            // A regular replaceAll will do...
            return bpmn.replaceAll("\\$\\{external_action}", action.toString().toLowerCase(ROOT));

        } catch (IOException exception) {
            throw new RuntimeException("Error patching external_action reference BPMN", exception);
        }
    }


    @NotNull String getSimpleActionBpmn(Action action) {

        if (!asList(START, VISA, SEAL, SECOND_OPINION, SIGNATURE, DELETE, ARCHIVE, IPNG_RETURN).contains(action)) {
            throw new RuntimeException(String.format("The simple action template cannot be applied to the action:%s", action));
        }

        try (InputStream actionTemplateInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/simple_action.bpmn20.xml");
             InputStream safeInputStream = Optional.ofNullable(actionTemplateInputStream)
                     .orElseThrow(() -> new IOException("Missing internal simple_action file"))) {

            String bpmn = IOUtils.toString(safeInputStream, UTF_8);
            // A StringSubstitutor would have been nicer, but it fails to catch the CDATA content.
            // A regular replaceAll will do...
            return bpmn.replaceAll("\\$\\{simple_action}", action.toString().toLowerCase(ROOT));

        } catch (IOException exception) {
            throw new RuntimeException("Error patching simple_action reference BPMN", exception);
        }
    }


    private @NotNull String getBpmn(@NotNull String resource) {

        try (InputStream bpmnInputStream = getClass().getClassLoader().getResourceAsStream(resource);
             InputStream safeInputStream = Optional.ofNullable(bpmnInputStream)
                     .orElseThrow(() -> new IOException("Missing internal resource file"))) {

            return IOUtils.toString(safeInputStream, UTF_8);

        } catch (IOException exception) {
            throw new RuntimeException("Error retrieving internal BPMN", exception);
        }
    }


    @PostMapping
    @Operation(description = "Create a new definition")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
    })
    public @NotNull WorkflowDefinition createWorkflowDefinition(@Parameter(name = "body", required = true)
                                                                @RequestBody WorkflowDefinition workflowDefinition,
                                                                @RequestParam String tenantId) {

        log.info("createWorkflowDefinition id:{} key:{} name:{}", workflowDefinition.getId(), workflowDefinition.getKey(), workflowDefinition.getName());
        log.info("createWorkflowDefinition actions:{}", workflowDefinition.getSteps().stream().map(StepDefinition::getType).toList());

        // Integrity check

        if (workflowDefinition.getSteps().stream()
                .map(StepDefinition::getValidatingDeskIds)
                .anyMatch(CollectionUtils::isEmpty)) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Validators list should not be null or empty");
        }

        if (workflowDefinition.getSteps().stream()
                .map(StepDefinition::getValidatingDeskIds)
                .flatMap(Collection::stream)
                .anyMatch(StringUtils::isEmpty)) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Validator should not be a null or empty string");
        }

        if (workflowDefinition.getSteps().stream()
                .map(StepDefinition::getValidatingDeskIds)
                .flatMap(Collection::stream)
                .anyMatch(v -> v.contains(","))) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Validators should not contain any ',' character. This is a reserved one in BPMN.");
        }

        //

        BpmnModel bpmn = definitionToBpmn(workflowDefinition);

        Deployment deployment = repositoryService
                .createDeployment()
                .tenantId(tenantId)
                .name(workflowDefinition.getName())
                .key(workflowDefinition.getId())
                .addBpmnModel(String.format("%s.bpmn20.xml", workflowDefinition.getId()), bpmn)
                .deploy();

        workflowDefinition.setDeploymentId(deployment.getId());

        log.info("createWorkflowDefinition {}", workflowDefinition);
        return workflowDefinition;
    }


    @PutMapping("{" + WorkflowDefinition.API_ID_PATH + "}")
    @Operation(description = "Update an existing definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull WorkflowDefinition updateWorkflowDefinition(@Parameter(description = WorkflowDefinition.API_DOC_ID_VALUE)
                                                                @PathVariable(name = WorkflowDefinition.API_ID_PATH) String processDefinitionId,
                                                                @Parameter(name = "body", required = true)
                                                                @RequestBody WorkflowDefinition newWorkflowDefinition,
                                                                @RequestParam String tenantId) {

        log.debug("updateWorkflowDefinition - new definition : {}", newWorkflowDefinition);

        ProcessDefinitionQuery query = repositoryService
                .createProcessDefinitionQuery()
                .latestVersion()
                .processDefinitionTenantId(tenantId)
                .processDefinitionId(processDefinitionId);

        if (query.count() < 1) {
            throw new ResponseStatusException(NOT_FOUND, "could not find workflow definition with id : " + processDefinitionId);
        }

        newWorkflowDefinition.setId(newWorkflowDefinition.getKey());
        BpmnModel bpmn = definitionToBpmn(newWorkflowDefinition);

        Deployment deployment = repositoryService
                .createDeployment()
                .tenantId(tenantId)
                .name(newWorkflowDefinition.getName())
                .key(newWorkflowDefinition.getKey())
                .addBpmnModel(newWorkflowDefinition.getKey() + ".bpmn20.xml", bpmn)
                .deploy();

        newWorkflowDefinition.setDeploymentId(deployment.getId());

        log.info("updated workflow definition : {}", newWorkflowDefinition);
        return newWorkflowDefinition;
    }

//    public @NotNull WorkflowDefinition createWorkflowDefinition(@ApiParam(name = "body", value = CreateWorkflowDefinitionRequest.API_DOC, required = true)
//                                                        @RequestBody CreateWorkflowDefinitionRequest createRequest) {
//
//        if (!createRequest.getWorkflowName().endsWith(".bpmn20.xml")) {
//            throw new ResponseStatusException(BAD_REQUEST, "workflowName does not end with \".bpmn20.xml\"");
//        }
//
//        String bpmn;
//
//        try {
//            byte[] bpmnByteArray = Base64.getDecoder().decode(createRequest.getWorkflowDefinitionBase64());
//            log.debug("createWorkflowDefinition before patch : {}", new String(bpmnByteArray, UTF_8));
//
//            bpmn = createRequest.isBpmnPatchNeeded() ?
//                    patchBpmn(classLoader, new ByteArrayInputStream(bpmnByteArray)) : new String(bpmnByteArray, UTF_8);
//
//        } catch (IllegalArgumentException e) {
//            log.error("Bpmn decoding error", e);
//            throw new ResponseStatusException(BAD_REQUEST, "Bpmn decoding error", e);
//        } catch (TransformerException e) {
//            log.error("Bpmn patch error", e);
//            throw new ResponseStatusException(BAD_REQUEST, "Bpmn patch error", e);
//        }
//
//        log.debug("createWorkflowDefinition after patch : {}", bpmn);
//
//        Deployment deployment = flowableService.getProcessEngine().getRepositoryService()
//                .createDeployment()
//                .name(createRequest.getWorkflowName())
//                .addString(createRequest.getWorkflowName(), bpmn)
//                .deploy();
//
//        WorkflowDefinition result = new WorkflowDefinition(deployment);
//        log.info("createWorkflowDefinition {}", result);
//        return result;
//    }


    @GetMapping
    @Operation(description = "List definitions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<WorkflowDefinition> getWorkflowDefinitions(@Parameter(description = PaginatedList.API_DOC_PAGE)
                                                                             @RequestParam(defaultValue = "0") int page,
                                                                             @Parameter(description = PaginatedList.API_DOC_PAGE_SIZE)
                                                                             @RequestParam(defaultValue = "50") int pageSize,
                                                                             @RequestParam String tenantId,
                                                                             @Parameter(description = "Sort by column")
                                                                             @RequestParam(defaultValue = "NAME") WorkflowDefinition.SortBy sortBy,
                                                                             @Parameter(description = "Sort ascending")
                                                                             @RequestParam(defaultValue = "true") boolean asc,
                                                                             @Parameter(description = "Searching for a specific workflow name")
                                                                             @RequestParam(required = false) String searchTerm) {

        // Building request

        ProcessDefinitionQuery query = repositoryService
                .createProcessDefinitionQuery()
                .latestVersion();

        Optional.ofNullable(tenantId)
                .ifPresent(query::processDefinitionTenantId);

        switch (sortBy) {
            case ID -> query.orderByProcessDefinitionId();
            case KEY -> query.orderByProcessDefinitionKey();
            default -> query.orderByProcessDefinitionName();
        }

        if (asc) {
            query.asc();
        } else {
            query.desc();
        }

        Optional.ofNullable(searchTerm)
                .map(value -> URLDecoder.decode(value, UTF_8))
                .ifPresent(s -> query.processDefinitionNameLikeIgnoreCase("%" + s + "%"));

        // Transform to list

        List<WorkflowDefinition> workflowDefinitions = query.listPage((page * pageSize), pageSize)
                .stream()
                .map(WorkflowDefinition::new)
                .toList();

        // Computing parent deployment children count

        Set<String> deploymentIds = workflowDefinitions.stream()
                .map(WorkflowDefinition::getDeploymentId)
                .collect(toSet());

        ProcessDefinitionQuery processDefinitionInDeploymentIdQuery = repositoryService
                .createProcessDefinitionQuery();
        Optional.ofNullable(tenantId)
                .ifPresent(processDefinitionInDeploymentIdQuery::processDefinitionTenantId);
        List<ProcessDefinition> sameDeploymentProcesses = processDefinitionInDeploymentIdQuery
                .deploymentIds(deploymentIds)
                .list();

        workflowDefinitions.forEach(workflowDefinition ->
                workflowDefinition.setUsageCount(
                        sameDeploymentProcesses.stream()
                                .filter(p -> StringUtils.equals(p.getDeploymentId(), workflowDefinition.getDeploymentId()))
                                .count())
        );

        // Computing usage

        List<FilteringParameter> countParams = workflowDefinitions
                .stream()
                .flatMap(w -> Stream.of(
                        new FilteringParameter(null, null, META_CREATION_WORKFLOW_ID, w.getKey()),
                        new FilteringParameter(null, null, META_VALIDATION_WORKFLOW_ID, w.getKey())
                ))
                .toList();

        List<FilteringParameterCountResult> countResultList = databaseService.getMetadataUsage(countParams);

        workflowDefinitions.forEach(w ->
                w.setUsageCount(
                        countResultList.stream()
                                .filter(c -> StringUtils.equals(w.getKey(), c.getFilterMetadataValue()))
                                .mapToInt(FilteringParameterCountResult::getCount)
                                .sum()
                ));

        // Sending back result

        log.info("getWorkflowDefinitions page:{} pageSize:{} data:{}", page, pageSize, workflowDefinitions);

        LongSupplier totalSupplier = () -> repositoryService.createProcessDefinitionQuery()
                .processDefinitionTenantId(tenantId)
                .latestVersion()
                .count();

        return new PaginatedList<>(workflowDefinitions, page, pageSize, totalSupplier);
    }


    @GetMapping("{" + WorkflowDefinition.API_ID_PATH + "}")
    @Operation(description = "Get one definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ApiUtils.ErrorResponse.class))),
    })
    public WorkflowDefinition getWorkflowDefinitionById(@Parameter(description = WorkflowDefinition.API_DOC_ID_VALUE)
                                                        @PathVariable(name = WorkflowDefinition.API_ID_PATH) String processDefinitionId,
                                                        @RequestParam String tenantId) {

        ProcessDefinitionQuery query = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionTenantId(tenantId)
                .latestVersion()
                .processDefinitionId(processDefinitionId);

        WorkflowDefinition workflowDefinition = Optional.ofNullable(query.singleResult())
                .map(WorkflowDefinition::new)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "could not find workflow definition with id : " + processDefinitionId));

        populateWorkflowDefinition(processDefinitionId, tenantId, workflowDefinition);

        // Sending back result

        log.info("getWorkflowDefinitionById actions:{}", workflowDefinition.getSteps().stream().map(StepDefinition::getType).collect(toSet()));
        return workflowDefinition;
    }


    @GetMapping("byKey/{" + WorkflowDefinition.API_KEY_PATH + "}")
    @Operation(description = "Get one definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinition getWorkflowDefinitionByKey(@Parameter(description = WorkflowDefinition.API_DOC_KEY_VALUE)
                                                         @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String processDefinitionKey,
                                                         @RequestParam String tenantId) {

        ProcessDefinitionQuery query = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionTenantId(tenantId)
                .latestVersion()
                .processDefinitionKey(processDefinitionKey);

        WorkflowDefinition workflowDefinition = Optional.ofNullable(query.singleResult())
                .map(WorkflowDefinition::new)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "could not find workflow definition with id : " + processDefinitionKey));

        populateWorkflowDefinition(processDefinitionKey, tenantId, workflowDefinition);

        return workflowDefinition;
    }


    private void populateWorkflowDefinition(String processDefinitionIdOrKey, String tenantId, WorkflowDefinition workflowDefinition) {
        BpmnModel definitionModel = this.flowableService.getBpmnModel(processDefinitionIdOrKey, tenantId);
        Process process = definitionModel.getMainProcess();

        String finalDeskId = (String) process.getDataObjects()
                .stream()
                .filter(dataObj -> StringUtils.equals(dataObj.getName(), WorkflowDefinition.FINAL_DESK_ATTR_NAME))
                .filter(dataObj -> (dataObj instanceof StringDataObject))
                .findAny()
                .map(ValuedDataObject::getValue)
                .orElse(null);

        List<String> finalNotifiedDesks = Arrays
                .stream(process.getDataObjects()
                        .stream()
                        .filter(dataObj -> StringUtils.equals(dataObj.getName(), WorkflowDefinition.FINAL_NOTIFIED_DESKS_ATTR_NAME))
                        .filter(dataObj -> (dataObj instanceof StringDataObject))
                        .findAny()
                        .map(ValuedDataObject::getValue)
                        .map(Object::toString)
                        .orElse(EMPTY)
                        .split(",")
                )
                .filter(StringUtils::isNotEmpty)
                .toList();

        workflowDefinition.setFinalDeskId(finalDeskId);
        workflowDefinition.setFinalNotifiedDeskIds(finalNotifiedDesks);

        // Computing usage
        workflowDefinition.setUsageCount(getMetadataUsageCountForDefinition(workflowDefinition));

        // Computing definition
        List<StepDefinition> stepDefs = getStepsForWorkflowDefinition(workflowDefinition.getId());
        workflowDefinition.setSteps(stepDefs);
    }


    @DeleteMapping("{" + WorkflowDefinition.API_DEPLOYMENT_ID_PATH + "}")
    @Operation(description = "Delete specified definition")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteWorkflowDefinition(@Parameter(description = WorkflowDefinition.API_DOC_DEPLOYMENT_ID_VALUE)
                                         @PathVariable(name = WorkflowDefinition.API_DEPLOYMENT_ID_PATH) String id) {

        log.info("deleteWorkflowDefinition {}", id);

        try {
            repositoryService.deleteDeployment(id);
        } catch (FlowableObjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Deployment id not known", e);
        }
    }


    @NotNull private List<StepDefinition> getStepsForWorkflowDefinition(String processDefinitionId) {

        TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap = new TreeMap<>(
                flowableService.getDefinitionTasks(processDefinitionId, 2L, null)
                        .entrySet().stream()
                        .collect(toMap(e -> Pair.of(2L, e.getKey()), Map.Entry::getValue))
        );

        List<StepDefinition> stepDefs = definitionTasksMap.values().stream()
                .map(StepDefinition::new)
                .toList();

        log.trace("getStepsForWorkflowDefinition {}", stepDefs);
        return stepDefs;
    }


    private int getMetadataUsageCountForDefinition(WorkflowDefinition workflowDefinition) {

        List<FilteringParameterCountResult> countResultList = databaseService.getMetadataUsage(asList(
                new FilteringParameter(null, null, META_CREATION_WORKFLOW_ID, workflowDefinition.getKey()),
                new FilteringParameter(null, null, META_VALIDATION_WORKFLOW_ID, workflowDefinition.getKey())));

        int usageCount = countResultList.stream()
                .mapToInt(FilteringParameterCountResult::getCount)
                .sum();

        log.trace("getMetadataUsageCountForDefinition {}", usageCount);
        return usageCount;
    }


}
