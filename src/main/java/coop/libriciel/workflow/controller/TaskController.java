/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.requests.PerformTaskRequest;
import coop.libriciel.workflow.services.DatabaseService;
import coop.libriciel.workflow.services.FlowableService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.identitylink.api.IdentityLinkInfo;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.*;

import static coop.libriciel.workflow.models.Instance.*;
import static coop.libriciel.workflow.models.Task.Action.*;
import static coop.libriciel.workflow.models.Task.META_CANDIDATE_DELEGATED_BY_GROUP_ID;
import static coop.libriciel.workflow.utils.ApiUtils.*;
import static coop.libriciel.workflow.utils.CollectionUtils.popValue;
import static coop.libriciel.workflow.utils.TextUtils.WORKFLOW_INTERNAL;
import static java.util.Collections.emptyList;
import static org.flowable.identitylink.api.IdentityLinkType.CANDIDATE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Log4j2
@RestController
@RequestMapping("task")
@Tag(name = "task", description = "Task operations")
public class TaskController {


    // <editor-fold desc="Beans">


    private final DatabaseService dbService;
    private final FlowableService flowableService;
    private final TaskService taskService;
    private final RuntimeService runtimeService;


    @Autowired
    public TaskController(DatabaseService databaseService, FlowableService flowableService, TaskService taskService, RuntimeService runtimeService) {
        this.taskService = taskService;
        this.flowableService = flowableService;
        this.dbService = databaseService;
        this.runtimeService = runtimeService;
    }


    @PostConstruct
    public void init() {
        dbService.initVariableNames();
    }


    // </editor-fold desc="Beans">


    @GetMapping("{" + Task.API_PATH + "}")
    @Operation(description = "Get specific task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Task getTask(@Parameter(description = Task.API_DOC_ID_VALUE)
                        @PathVariable(Task.API_PATH) String taskId) {

        log.debug("getTask id:{}", taskId);
        return flowableService.getHistoryService()
                .createHistoricTaskInstanceQuery()
                .taskId(taskId)
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .includeIdentityLinks()
                .listPage(0, 1)
                .stream()
                .findFirst()
                .map(Task::new)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Task id not found"));
    }


    @PostMapping("{" + Task.API_PATH + "}")
    @Operation(description = "Perform given task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void performTask(@Parameter(description = Task.API_DOC_ID_VALUE)
                            @PathVariable(name = Task.API_PATH) String taskId,
                            @Parameter(name = "body", description = "Every field is mandatory")
                            @RequestBody PerformTaskRequest request) {

        log.debug("Perform task:{} action:{} user:{} variables:{}", taskId, request.getAction(), request.getUserId(), request.getVariables());

        // Metadata check and cleanup

        request.setVariables(new HashMap<>(request.getVariables())); // Mutable copy
        checkForbiddenInnerVariables(request);
        setInnerRootVariables(taskId, request.getVariables());

        // Metadata reset to go back to FORM external state
        if (Set.of(UNDO, REJECT, BYPASS).contains(request.getAction())) {
            List<String> variablesToRemove = new ArrayList<>();
            variablesToRemove.add("i_Parapheur_internal_status");
            variablesToRemove.add("i_Parapheur_internal_transaction_id");
            variablesToRemove.add("i_Parapheur_internal_pastell_document_id");
            taskService.removeVariables(taskId, variablesToRemove);
        }

        HashMap<String, Object> taskVars = new HashMap<>(request.getVariables());
        taskVars.put("action", request.getAction().getFlowableString());

        // Computing groupIds to store
        List<String> existingCandidateGroups;
        try {
            existingCandidateGroups = taskService.getIdentityLinksForTask(taskId).stream()
                    .filter(link -> StringUtils.equals(link.getType(), CANDIDATE))
                    .map(IdentityLinkInfo::getGroupId)
                    .toList();
        } catch (FlowableObjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "The given taskId or definitionKey was not found when claimed", e);
        }

        boolean isOrParallelTask = existingCandidateGroups.size() > 1;
        boolean isSomeDelegation = StringUtils.isNotEmpty(request.getDelegatedByGroupId());
        boolean isPerformingGroupIdSet = StringUtils.isNotEmpty(request.getGroupId());
        boolean shouldRewriteCandidates = isPerformingGroupIdSet && (isOrParallelTask || isSomeDelegation);

        // Performing task

        try {
            taskService.claim(taskId, request.getUserId());

            if (shouldRewriteCandidates) {

                // Evaluating the current desk, if necessary.
                // We actually can clean the candidates, through taskService.deleteCandidateGroup,
                // But those modifications are not applied to the historyTask.

                if (isSomeDelegation) {
                    taskVars.put(META_CANDIDATE_DELEGATED_BY_GROUP_ID, request.getDelegatedByGroupId());
                }

                existingCandidateGroups.forEach(groupId -> taskService.deleteCandidateGroup(taskId, groupId));
                taskService.addCandidateGroup(taskId, request.getGroupId());
            }
        } catch (FlowableObjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "The given taskId or definitionKey was not found when claimed", e);
        }

        try {

            taskService.complete(taskId, taskVars);

        } catch (FlowableException e) {

            // Restoring initial state.
            // Without this, instances get stuck in an "un-actionnable" state.

            taskService.unclaim(taskId);

            if (shouldRewriteCandidates) {
                taskService.deleteCandidateGroup(taskId, request.getGroupId());
                existingCandidateGroups.forEach(groupId -> taskService.addCandidateGroup(taskId, groupId));
            }

            throw (e);
        }

        // On a recycle action, we'll just delete everything from the history.
        // That'll ease every upcoming builds of events.
        if (request.getAction() == RECYCLE) {
            Optional.ofNullable(flowableService.getHistoryService().createHistoricTaskInstanceQuery().taskId(taskId).singleResult())
                    .map(HistoricTaskInstance::getProcessInstanceId)
                    // Kinda lame, but the businessKey is not accessible from the tasks. We have to retrieve the ProcessInstance.
                    .map(i -> flowableService.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(i).list())
                    .orElse(emptyList())
                    .stream()
                    .map(HistoricProcessInstance::getBusinessKey)
                    .flatMap(b -> flowableService.getHistoryService().createHistoricTaskInstanceQuery().processInstanceBusinessKey(b).list().stream())
                    // The START action is actually re-used. We cannot delete it from history.
                    .filter(t -> !StringUtils.equals("workflow_internal_draft", t.getName()))
                    .filter(t -> !StringUtils.equals("main_start", t.getName()))
                    .forEach(t -> flowableService.getHistoryService().deleteHistoricTaskInstance(t.getId()));
        }
    }


    /**
     * Some metadata shall be stored at root-level, not in the Task's sub-process.
     * Typically, for a chain action.
     * <p>
     * Note that this method does not check the validity of the root-level access.
     * It shall be checked before. If there is any root-level variable given, it will store it.
     *
     * @param taskId    the current task to root
     * @param variables the overridden variables to store
     */
    private void setInnerRootVariables(String taskId, Map<String, String> variables) {

        log.debug("setFolderVariables taskId:{} variables:{}", taskId, variables);
        Set<String> innerVariablesKeys = Set.of(META_VALIDATION_WORKFLOW_ID, META_ORIGIN_GROUP_ID, META_FINAL_GROUP_ID, META_FINAL_NOTIFIED_GROUPS);

        Map<String, String> innerVariablesMap = new HashMap<>();
        innerVariablesKeys.forEach(key -> innerVariablesMap.put(key, popValue(variables, key)));
        innerVariablesMap.values().removeIf(StringUtils::isEmpty);

        if (CollectionUtils.isEmpty(innerVariablesMap)) {
            return;
        }

        // We have to find the root process id

        org.flowable.task.api.Task task = flowableService
                .getTaskService()
                .createTaskQuery()
                .taskId(taskId)
                .singleResult();

        Execution execution = runtimeService
                .createExecutionQuery()
                .executionId(task.getExecutionId())
                .singleResult();

        // Actual save

        log.debug("setFolderVariables processInstanceId:{}", execution.getRootProcessInstanceId());

        runtimeService.removeVariables(
                execution.getRootProcessInstanceId(),
                innerVariablesKeys.stream()
                        .filter(key -> !innerVariablesMap.containsKey(key))
                        .toList()
        );

        runtimeService.setVariables(
                execution.getRootProcessInstanceId(),
                innerVariablesMap
        );
    }


    /**
     * In some very specific cases, overriding internal workflow keys is allowed.
     * Yet, for some actions, that can be allowed.
     *
     * @param request the request to test
     */
    static void checkForbiddenInnerVariables(PerformTaskRequest request) {
        if (request.getVariables().keySet().stream()
                .filter(metadataKey -> {
                    boolean isStartingAction = List.of(START, CHAIN).contains(request.getAction());
                    boolean isWorkflowDefinitionMetadata = StringUtils.equals(metadataKey, META_VALIDATION_WORKFLOW_ID);
                    boolean isExceptionallyAllowedValidationMetadata = isWorkflowDefinitionMetadata && isStartingAction;
                    return !isExceptionallyAllowedValidationMetadata;
                })
                .filter(metadataKey -> {
                    boolean isChainAction = request.getAction() == CHAIN;
                    boolean isOriginGroupIdMetadata = StringUtils.equals(metadataKey, META_ORIGIN_GROUP_ID);
                    boolean isFinalGroupIdMetadata = StringUtils.equals(metadataKey, META_FINAL_GROUP_ID);
                    boolean isFinalNotifiedGroupIdMetadata = StringUtils.equals(metadataKey, META_FINAL_NOTIFIED_GROUPS);
                    boolean isExceptionallyAllowedStartFinalDeskMetadata = ((isFinalGroupIdMetadata || isFinalNotifiedGroupIdMetadata || isOriginGroupIdMetadata)) && isChainAction;
                    return !isExceptionallyAllowedStartFinalDeskMetadata;
                })
                .filter(metadataKey -> {
                    boolean isTransferAction = List.of(TRANSFER, ASK_SECOND_OPINION).contains(request.getAction());
                    boolean isTargetDeskMetadata = StringUtils.equals(metadataKey, META_CANDIDATE_DELEGATE_GROUPS);
                    boolean isExceptionallyAllowedTargetDeskMetadata = isTargetDeskMetadata && isTransferAction;
                    return !isExceptionallyAllowedTargetDeskMetadata;
                })
                .anyMatch(metadataKey -> metadataKey.startsWith(WORKFLOW_INTERNAL))) {
            log.warn("A metadata has  'workflow_internal' prefix in an inappropriate context, abort");
            throw new ResponseStatusException(BAD_REQUEST, "Instance variables starting with `" + WORKFLOW_INTERNAL + "` are reserved");
        }
    }


}
