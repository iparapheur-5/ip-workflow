/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.controller;

import coop.libriciel.workflow.models.SortBy;
import coop.libriciel.workflow.models.State;
import coop.libriciel.workflow.models.Task;
import coop.libriciel.workflow.models.requests.FilteredRequest;
import coop.libriciel.workflow.models.requests.FilteringParameter;
import coop.libriciel.workflow.models.requests.FilteringParameterCountResult;
import coop.libriciel.workflow.models.requests.IntResponse;
import coop.libriciel.workflow.services.DatabaseService;
import coop.libriciel.workflow.utils.ApiUtils.Group;
import coop.libriciel.workflow.utils.EnumLowercaseConverter;
import coop.libriciel.workflow.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.LongSupplier;

import static coop.libriciel.workflow.models.State.PENDING;
import static coop.libriciel.workflow.utils.ApiUtils.CODE_200;
import static java.util.Collections.singletonList;
import static java.util.Comparator.*;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@Log4j2
@RestController
@RequestMapping("group")
@Tag(name = "group", description = "Group-level data access")
public class GroupController {


    // <editor-fold desc="Beans">


    private final DatabaseService dbService;


    @Autowired
    public GroupController(DatabaseService dbService) {
        this.dbService = dbService;
    }


    @PostConstruct
    public void init() {
        dbService.initVariableNames();
    }


    @InitBinder public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(State.class, new EnumLowercaseConverter<>(State.class));
    }


    // </editor-fold desc="Beans">


    @GetMapping("{" + Group.API_PATH + "}/count/{" + State.API_PATH + "}")
    @Operation(description = "Simple count on group's instances")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull IntResponse countWorkflows(@Parameter(description = Group.API_DOC_ID_VALUE)
                                               @PathVariable(name = Group.API_PATH) String groupId,
                                               @Parameter(description = State.API_DOC_ID_VALUE)
                                               @PathVariable(name = State.API_PATH) State state) {
        return new IntResponse(
                dbService.getCount(state, singletonList(new FilteringParameter(groupId, null, null, null)))
                        .stream()
                        .map(FilteringParameterCountResult::getCount)
                        .findFirst()
                        .orElse(0)
        );
    }


    @PostMapping("{" + Group.API_PATH + "}/{" + State.API_PATH + "}")
    @Operation(description = "List tasks")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull PaginatedList<Task> requestTasks(@Parameter(description = Group.API_DOC_ID_VALUE)
                                                     @PathVariable(name = Group.API_PATH) String groupId,
                                                     @Parameter(description = State.API_DOC_ID_VALUE)
                                                     @PathVariable(name = State.API_PATH) State state,
                                                     @Parameter(description = FilteredRequest.API_DOC)
                                                     @RequestBody FilteredRequest requestTasksParams,
                                                     @Parameter(description = "Sort by column")
                                                     @RequestParam(required = false) SortBy sortBy,
                                                     @Parameter(description = "Sort ascending")
                                                     @RequestParam(defaultValue = "true") boolean asc,
                                                     @Parameter(description = PaginatedList.API_DOC_PAGE)
                                                     @RequestParam(defaultValue = "0") int page,
                                                     @Parameter(description = PaginatedList.API_DOC_PAGE_SIZE)
                                                     @RequestParam(defaultValue = "50") int pageSize) {

        log.debug("requestTasks group:{} state:{} filteringParameters:{}", groupId, state, requestTasksParams.getFilteringParameters());
        List<FilteringParameter> filteringParameters = new ArrayList<>(requestTasksParams.getFilteringParameters());

        if (requestTasksParams.getFolderFilter().getMetadataFilters().isEmpty()) {
            Optional.ofNullable(groupId)
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> new FilteringParameter(i, null, null, null))
                    .ifPresent(filteringParameters::add);
        }

        requestTasksParams
                .getFolderFilter()
                .getMetadataFilters()
                .forEach((key, value) -> filteringParameters.add(new FilteringParameter(groupId, null, key, value)));

        List<Task> result = dbService
                .getTasks(
                        null,
                        filteringParameters,
                        state,
                        sortBy.toString(),
                        asc,
                        page,
                        pageSize,
                        Optional.ofNullable(requestTasksParams.getFolderFilter().getTypeId())
                                .filter(StringUtils::isNotEmpty)
                                .orElse(null),
                        Optional.ofNullable(requestTasksParams.getFolderFilter().getSubtypeId())
                                .filter(StringUtils::isNotEmpty)
                                .orElse(null),
                        Optional.ofNullable(requestTasksParams.getFolderFilter().getSearchData())
                                .filter(StringUtils::isNotEmpty)
                                .orElse(null),
                        requestTasksParams.getFolderFilter().getCreatedAfter(),
                        requestTasksParams.getFolderFilter().getCreatedBefore(),
                        null
                );

        // The count will return a group:state:count map.
        // If we have a delegation request, we may have multiple groups in return.
        // To get a proper pagination, we just want every PENDING count, in a proper summed count.
        LongSupplier totalSupplier = () -> dbService.getCount(state, filteringParameters)
                .stream()
                .filter(c -> c.getState() == state)
                .mapToInt(FilteringParameterCountResult::getCount)
                .sum();

        // Sending back result

        log.debug("getPendingTasks result:{}", result.stream().map(Task::getInstanceName).toList());
        return new PaginatedList<>(result, page, pageSize, totalSupplier);
    }


    @PostMapping
    @Operation(description = "Request instance counts for given parameters. Just the total count.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
    })
    public @NotNull List<FilteringParameterCountResult> requestCounts(@Parameter(description = FilteredRequest.API_DOC)
                                                                      @RequestBody FilteredRequest requestTasksParams) {

        log.debug("requestCountInstances list:{}", requestTasksParams);

        List<FilteringParameter> groupCountList = requestTasksParams.getFilteringParameters();
        ArrayList<FilteringParameterCountResult> result = new ArrayList<>();

        // Prepare requests

        List<String> groups = groupCountList.stream()
                .filter(g -> StringUtils.isNotEmpty(g.getGroupId()))
                .filter(g -> StringUtils.isEmpty(g.getFilterMetadataKey()))
                .filter(g -> StringUtils.isEmpty(g.getFilterMetadataValue()))
                .map(FilteringParameter::getGroupId)
                .toList();

        List<FilteringParameter> metadataSpecificCountList = groupCountList.stream()
                .filter(g -> StringUtils.isNotEmpty(g.getGroupId()))
                .filter(g -> StringUtils.isNotEmpty(g.getFilterMetadataKey()))
                .filter(g -> StringUtils.isNotEmpty(g.getFilterMetadataValue()))
                .toList();

        if (!metadataSpecificCountList.stream().allMatch(filteringParameter -> filteringParameter.getState() == PENDING)) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Partial counts are only supported on PENDING state");
        }

        log.debug("requestCountInstances groups:{}", groups);
        log.debug("requestCountInstances metadataSpecificCountList:{}", metadataSpecificCountList);

        // Doing it in a single SQL request is kind of tricky.
        // If a group request collapse with a metadata-specific request, counts will not be relevant.
        // We'll have to split those in two requests (or improve the request).

        if (CollectionUtils.isNotEmpty(groups)) {
            List<FilteringParameterCountResult> groupCountResult = dbService.getCount(groups);
            log.debug("requestCountInstances {} groupCountList for {} results", groups.size(), groupCountResult.size());
            result.addAll(groupCountResult);
        }

        if (CollectionUtils.isNotEmpty(metadataSpecificCountList)) {
            List<FilteringParameterCountResult> metadataCountResult = dbService.getCount(PENDING, metadataSpecificCountList);
            log.debug("requestCountInstances {} metadataSpecificCountList for {} results...", metadataSpecificCountList.size(), metadataCountResult.size());
            result.addAll(metadataCountResult);
        }

        // Sorting results back to ease unit-tests

        result.sort(comparing(FilteringParameter::getGroupId, nullsLast(naturalOrder()))
                .thenComparing(FilteringParameter::getState, nullsLast(naturalOrder()))
                .thenComparing(FilteringParameter::getFilterMetadataKey, nullsFirst(naturalOrder()))
                .thenComparing(FilteringParameter::getFilterMetadataValue, nullsFirst(naturalOrder())));

        log.debug("requestCountInstances result:{}", result);
        return result;
    }


}
