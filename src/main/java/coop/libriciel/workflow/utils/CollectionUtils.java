/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import coop.libriciel.workflow.models.Task;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.flowable.identitylink.api.IdentityLinkInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.workflow.models.Task.Action.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Comparator.*;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.flowable.identitylink.api.IdentityLinkType.CANDIDATE;


@Log4j2
public class CollectionUtils {


    public static @NotNull Map<String, String> parseMetadata(@Nullable String[][] data) {

        Map<String, String> metadata = new HashMap<>();

        if (data == null) {
            return metadata;
        }

        Arrays.stream(data)
                .filter(entry -> entry.length == 2)
                .forEach(entry -> metadata.put(entry[0], entry[1]));

        return metadata;
    }


    public static @Nullable <T> T popValue(@Nullable Map<String, T> map, @NotNull String key) {

        if (map == null) {
            return null;
        }

        T result = map.get(key);
        map.remove(key);
        return result;
    }


    public static void safeAdd(@NotNull Map<Pair<Long, Long>, List<Task>> map, @NotNull Pair<Long, Long> key, @NotNull Task value) {
        map.computeIfAbsent(key, k -> new ArrayList<>());
        map.get(key).add(value);
    }


    public static List<String> toCandidateGroupIds(@NotNull List<? extends IdentityLinkInfo> identityLinkInfoList) {
        return identityLinkInfoList.stream()
                .filter(link -> StringUtils.equals(link.getType(), CANDIDATE))
                .map(IdentityLinkInfo::getGroupId)
                .toList();
    }


    // <editor-fold desc="Merging tasks">


    /**
     * Unfortunately, UNDOs are registered in history, as undo-ed tasks.
     *
     * @param index           The current step to clean
     * @param historyTasksMap The already performed UserTasks, we need the full list for a forward-seeking
     */
    private static void removeUndoTasks(Pair<Long, Long> index, TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap) {

        List<Task> historyList = historyTasksMap.getOrDefault(index, emptyList());

        // TODO : Weird cases, should investigate
        // It seems to be the revoked/unused UNDO actions,
        // That lays there, with a weird performed actions.
        historyList.removeIf(t -> (t.getExpectedAction() == UNDO) && (t.getPerformedAction() != UNDO));

        // The last "N+1-step" UNDO task is a threshold.
        // If the UNDO was triggered at a d-date, everything before that should be ignored.
        Pair<Long, Long> nextStepIndex = Pair.of(index.getLeft(), index.getRight() + 1);
        Optional.ofNullable(historyTasksMap.get(nextStepIndex))
                .orElse(emptyList())
                .stream()
                .filter(t -> t.getPerformedAction() == UNDO)
                .min(comparing(Task::getDate, nullsLast(reverseOrder())))
                .map(Task::getDate)
                .ifPresent(threshold -> historyList.removeIf(t -> (t.getDate() != null) && (t.getDate().before(threshold))));

        // In-step UNDOs should cancel the previous task
        ListIterator<Task> iterator = historyList.listIterator();
        while (iterator.hasNext()) {

            Task task = iterator.next();
            if ((task != null) && (task.getPerformedAction() == UNDO)) {
                iterator.remove();
                continue;
            }

            Task upcomingTask = iterator.hasNext() ? iterator.next() : null;
            if ((upcomingTask != null) && (upcomingTask.getPerformedAction() == UNDO)) {
                iterator.previous();
                iterator.remove();
                iterator.next();
                iterator.remove();
            }
        }
    }


    /**
     * So... Here's the big one.
     * <p>
     * Flowable have 3 completely different data storages for the past, current and future tasks.<br/>
     * Those 3 maps need to be merged to have a proper list to display in the UI, without duplicates.
     * <p/>
     * A special attention is taken for the UNDO tasks :<br/>
     * Any UNDO and the UNDOed action are already into the history,<br/>
     * Yet we don't want to return any hint of those.
     * <p/>
     * The {@link Pair}<{@link Long}, {@link Long}> are a sort of coordinates that ease the merge of definitions, pending, and history tasks.<br/>
     * <ul>
     *     <li>{@link Pair#getLeft()}, the current folder's workflow :<br/>
     *         <ul>
     *             <li>`0` : The draft level.</li>
     *             <li>`1` : The creation workflow (may be null, and not containing anything)</li>
     *             <li>`2` : The validation workflow</li>
     *         </ul>
     *     </li>
     *     <li>{@link Pair#getLeft()}, the current step index, in its own workflow.</li>
     * </ul>
     *
     * @param definitionTasksMap Tasks parsed from the WorkflowDefinition.
     * @param historyTasksMap    The already performed UserTasks
     * @param pendingTasksMap    The currently waiting-for-an-action UserTasks
     * @return a proper, sorted, single Task list
     */
    public static @NotNull List<Task> mergeTaskLists(TreeMap<Pair<Long, Long>, List<Task>> definitionTasksMap,
                                                     TreeMap<Pair<Long, Long>, List<Task>> historyTasksMap,
                                                     TreeMap<Pair<Long, Long>, List<Task>> pendingTasksMap) {

        Set<Pair<Long, Long>> indexes = new HashSet<>();
        indexes.addAll(definitionTasksMap.keySet());
        indexes.addAll(historyTasksMap.keySet());
        indexes.addAll(pendingTasksMap.keySet());

        log.debug("mergeTaskLists definitionTasksMap:{}", definitionTasksMap);
        log.debug("mergeTaskLists historyTasksMap:{}", historyTasksMap);
        log.debug("mergeTaskLists pendingTasksMap:{}", pendingTasksMap);

        // Force mutable lists

        definitionTasksMap.forEach((key, valuesList) -> definitionTasksMap.put(key, new ArrayList<>(valuesList)));
        historyTasksMap.forEach((key, valuesList) -> historyTasksMap.put(key, new ArrayList<>(valuesList)));
        pendingTasksMap.forEach((key, valuesList) -> pendingTasksMap.put(key, new ArrayList<>(valuesList)));

        // Cleanup canceled/revoked steps

        historyTasksMap.forEach((key, value) -> value.removeIf(t -> (t.getExpectedAction() == READ) && (t.getAssignee() == null)));

        // Merging pending steps

        return indexes.stream()
                .sorted(comparing(Pair<Long, Long>::getLeft, naturalOrder()).thenComparing(Pair::getRight, naturalOrder()))
                .map(index -> {
                    List<Task> pendingList = pendingTasksMap.getOrDefault(index, emptyList());
                    List<Task> historyList = historyTasksMap.getOrDefault(index, emptyList());

                    pendingList.sort(comparing(t -> t.getExpectedAction().ordinal(), naturalOrder()));
                    historyList.sort(comparing(Task::getDate, nullsLast(naturalOrder())));

                    removeUndoTasks(index, historyTasksMap);

                    if (isEmpty(historyList) && isEmpty(pendingList)) {
                        // If everything else is empty, we simply go for the definition
                        return definitionTasksMap.get(index);
                    }

                    // Special case on multiple tasks :
                    // * start -> After a recycle, the internal start task is reused. We have to remove the old ones
                    // * external_action -> We want to merge the 2 consecutive tasks, and only keep the last one too
                    if (Optional.ofNullable(definitionTasksMap.get(index)).orElse(historyList)
                            .stream().anyMatch(t -> asList(START, EXTERNAL_SIGNATURE, SECURE_MAIL, IPNG).contains(t.getExpectedAction()))) {

                        // Getting the last one in date (or the one that is not yet performed)
                        Task multipleTasksReduced = Stream
                                .concat(historyList.stream(), pendingList.stream())
                                .min(comparing(Task::getDate, nullsFirst(reverseOrder())))
                                .orElse(null);

                        log.debug("mergeTaskLists result 2 {}:{}", index, multipleTasksReduced);
                        return singletonList(multipleTasksReduced);
                    }

                    // Now that every list is cleaned up,
                    // we send back historical+pending Task we got.
                    List<Task> finalList = new ArrayList<>();
                    finalList.addAll(historyList);
                    finalList.addAll(pendingList);

                    // Some special cases,
                    // where we still want the definition back in the UI.
                    boolean isPendingUndo = (!pendingList.isEmpty()) && pendingList.stream().allMatch(t -> t.getExpectedAction() == UNDO);
                    boolean isPendingSecondOpinion = pendingList.stream()
                            .min(comparing(Task::getBeginDate, nullsLast(reverseOrder())))
                            .map(t -> t.getExpectedAction() == SECOND_OPINION)
                            .orElse(false);

                    if (isPendingUndo || isPendingSecondOpinion) {
                        finalList.addAll(definitionTasksMap.get(index));
                    }

                    // Sending back result
                    log.debug("mergeTaskLists result 3 {}:{}", index, finalList);
                    return finalList;
                })
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
    }


    // </editor-fold desc="Merging tasks">


}
