/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.utils;

import org.apache.xml.security.Init;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.jetbrains.annotations.NotNull;

import javax.xml.transform.TransformerException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.xml.security.c14n.Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS;

public class XmlUtils {

    public static final String OUTPUTKEYS_INDENT_AMOUNT = "{http://xml.apache.org/xslt}indent-amount";


    static {
        Init.init();
    }


    XmlUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static @NotNull String canonicalize(byte[] xmlBytes) throws TransformerException {
        String result;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            Canonicalizer canonicalizer = Canonicalizer.getInstance(ALGO_ID_C14N_WITH_COMMENTS);
            canonicalizer.canonicalize(xmlBytes, baos, false);
            result = baos.toString(UTF_8);
        } catch (XMLSecurityException | IOException e) {
            throw new TransformerException(e.getLocalizedMessage());
        }

        return result;
    }

}
