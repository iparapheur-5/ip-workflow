/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package coop.libriciel.workflow.utils;

import org.apache.xml.security.Init;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.conf.Settings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.jooq.conf.RenderKeywordCase.UPPER;
import static org.jooq.conf.RenderNameCase.AS_IS;
import static org.jooq.conf.RenderQuotedNames.EXPLICIT_DEFAULT_UNQUOTED;


public class TextUtils {

    public static final String WORKFLOW_INTERNAL = "workflow_internal_";
    public static final String I_PARAPHEUR_INTERNAL = "i_Parapheur_internal_";

    private static final @SuppressWarnings("SpellCheckingInspection") String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static Settings PSQL_RENDER_SETTINGS = new Settings()
            .withRenderQuotedNames(EXPLICIT_DEFAULT_UNQUOTED)
            .withRenderKeywordCase(UPPER)
            .withRenderFormatted(true)
            .withRenderNameCase(AS_IS);


    static {
        Init.init();
    }


    TextUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static @NotNull String toIso8601String(@NotNull Date date) {
        return new SimpleDateFormat(ISO8601_FORMAT, Locale.getDefault()).format(date);
    }


    public static @Nullable Date fromIso8601String(@Nullable String dateString) {

        if (StringUtils.isEmpty(dateString)) {
            return null;
        }

        try {
            return new SimpleDateFormat(ISO8601_FORMAT, Locale.getDefault()).parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

}
