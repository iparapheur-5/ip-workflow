/*
 * Workflow
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package coop.libriciel.workflow.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.LongSupplier;

import static java.util.Collections.EMPTY_LIST;

@Data
@AllArgsConstructor
public class PaginatedList<T> {

    public static final String API_DOC_PAGE = "Result page, starting at 0";
    public static final String API_DOC_PAGE_SIZE = "Result chunk size\n(Note that every retrieved chunk's data may be roughly 0.5ko)";


    private List<T> data;
    private long page;
    private long pageSize;
    private long total;


    @SuppressWarnings("unchecked")
    public static <T> PaginatedList<T> emptyPaginatedList() {
        return new PaginatedList<>((List<T>) EMPTY_LIST, 0, 0, 0);
    }


    public PaginatedList(@NotNull List<T> data, long page, long pageSize, @NotNull LongSupplier totalSupplier) {

        this.data = data;
        this.page = page;
        this.pageSize = pageSize;

        // Managing computable cases, avoiding un-necessary requests

        if ((data.size() == 0) && (page == 0)) {
            total = 0;
        } else if ((data.size() != 0) && (data.size() != pageSize)) {
            total = page * pageSize + data.size();
        } else {
            total = totalSupplier.getAsLong();
        }
    }

}
