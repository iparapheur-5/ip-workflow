Workflow
========

## Swagger

Once the application started, a Swagger page is available here :  
`http://iparapheur.dom.local/workflow/swagger-ui.html`

## Packaging

*Requirements:*

* Java 17
* Maven 3
* Gradle 7

Launch this command at the project root folder :

```bash
gradle bootJar
```

The packaged jar will be available at `target/workflow-VERSION.jar`.

## BPMN patch

### Flowable-modeler

Create a simple workflow.  
Predefined tasks should be `callActivity` tasks, by convention, we name them `visa xxx`, or `signature xxx`.

![GitHub Logo](markdown_resources/workflow_example.png)

... And download the `bpmn20.xml` file.

### Defining the sub-activity

Every sub-activities should call as `visa` or a `signature` activity.

Some `xslt` patches will define those steps automatically, if their names are set properly (like `visa Some group`) :

* Every `callActivity` step will call the appropriate `activity`.
* The `current/previous_candidate_groups` will be set.
* Some `undo`, where available.

![GitHub Logo](markdown_resources/workflow_undo_catches.png)

*Note : This patch removes the existing BPMN diagram : Since it adds elements, it will most likely break it anyway.*

